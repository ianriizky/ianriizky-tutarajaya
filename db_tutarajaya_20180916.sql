-- MySQL dump 10.13  Distrib 5.7.19, for Win64 (x86_64)
--
-- Host: localhost    Database: db_tutarajaya
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `z_detail_transaksi`
--

DROP TABLE IF EXISTS `z_detail_transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_detail_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_transaksi` (`id_transaksi`),
  KEY `id_transaksi_2` (`id_transaksi`),
  CONSTRAINT `z_detail_transaksi_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `z_transaksi` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_detail_transaksi`
--

LOCK TABLES `z_detail_transaksi` WRITE;
/*!40000 ALTER TABLE `z_detail_transaksi` DISABLE KEYS */;
INSERT INTO `z_detail_transaksi` VALUES (1,1,2,4750,1,4750);
/*!40000 ALTER TABLE `z_detail_transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_kategori`
--

DROP TABLE IF EXISTS `z_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_kategori`
--

LOCK TABLES `z_kategori` WRITE;
/*!40000 ALTER TABLE `z_kategori` DISABLE KEYS */;
INSERT INTO `z_kategori` VALUES (1,'Makanan'),(2,'Minuman'),(3,'Mainan'),(4,'Kenaapa ya?');
/*!40000 ALTER TABLE `z_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_member`
--

DROP TABLE IF EXISTS `z_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `thumbnail` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `status` enum('0','1') NOT NULL,
  `token` varchar(50) NOT NULL,
  `province` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telp` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_member`
--

LOCK TABLES `z_member` WRITE;
/*!40000 ALTER TABLE `z_member` DISABLE KEYS */;
INSERT INTO `z_member` VALUES (1,'ianrizky','Septianata Rizky Pratama','ian.rizkypratama02@gmail.com','$2a$08$EnpHRdetFL3EuvVSNVb1FO0oYuj7lKmliZuPp8S1X46QM.Er0MjnS','','2018-08-17 00:44:16','1','','Bali','Denpasar','Pesanggaran','085738626264'),(2,'ianrizky2','Septianata Rizky Pratama2','ian.rizkypratama@gmail.com','$2a$08$SimOlLTZ2GZSFmkgROnvueN1TnJG.bbIK3oojhBoraOuouuAX2ZJG','','2018-08-17 01:18:06','0','','','','Pesangga','');
/*!40000 ALTER TABLE `z_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_produk`
--

DROP TABLE IF EXISTS `z_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(6) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `kategori` int(11) NOT NULL,
  `harga` float NOT NULL,
  `diskon` float NOT NULL,
  `berat` int(11) NOT NULL,
  `thumbnail` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `popular` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_produk`
--

LOCK TABLES `z_produk` WRITE;
/*!40000 ALTER TABLE `z_produk` DISABLE KEYS */;
INSERT INTO `z_produk` VALUES (1,'PR0001','Snack Ringan','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1,500,10,0,'','1','0'),(2,'PR0002','Minuman enak','Lorem',2,5000,5,0,'','1','0');
/*!40000 ALTER TABLE `z_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_produk_gambar`
--

DROP TABLE IF EXISTS `z_produk_gambar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_produk_gambar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produk` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_produk` (`id_produk`),
  CONSTRAINT `z_produk_gambar_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `z_produk` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_produk_gambar`
--

LOCK TABLES `z_produk_gambar` WRITE;
/*!40000 ALTER TABLE `z_produk_gambar` DISABLE KEYS */;
INSERT INTO `z_produk_gambar` VALUES (1,1,'uploads/product/product-20180827221241-1.jpg'),(2,1,'uploads/product/product-20180827221252-1.jpg');
/*!40000 ALTER TABLE `z_produk_gambar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_produk_stok`
--

DROP TABLE IF EXISTS `z_produk_stok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_produk_stok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produk` int(11) NOT NULL,
  `stok` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_produk` (`id_produk`),
  CONSTRAINT `z_produk_stok_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `z_produk` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_produk_stok`
--

LOCK TABLES `z_produk_stok` WRITE;
/*!40000 ALTER TABLE `z_produk_stok` DISABLE KEYS */;
INSERT INTO `z_produk_stok` VALUES (1,1,10),(2,2,11);
/*!40000 ALTER TABLE `z_produk_stok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_slider`
--

DROP TABLE IF EXISTS `z_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_slider`
--

LOCK TABLES `z_slider` WRITE;
/*!40000 ALTER TABLE `z_slider` DISABLE KEYS */;
INSERT INTO `z_slider` VALUES (1,'uploads/slider/slider-20180814200120-1.jpg','0'),(2,'uploads/slider/slider-20180814200717-2.jpg','1'),(3,'uploads/slider/slider-20180819180549-3.jpg','0');
/*!40000 ALTER TABLE `z_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_transaksi`
--

DROP TABLE IF EXISTS `z_transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(8) NOT NULL,
  `province` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `nama_penerima` varchar(100) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `total` float NOT NULL,
  `created_at` datetime NOT NULL,
  `member_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  `bukti_transfer` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `z_transaksi_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `z_member` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_transaksi`
--

LOCK TABLES `z_transaksi` WRITE;
/*!40000 ALTER TABLE `z_transaksi` DISABLE KEYS */;
INSERT INTO `z_transaksi` VALUES (1,'TR000001','Bali','Denpasar','Pesanggaran','Septianata Rizky Pratama','085738626264','',4750,'2018-09-09 22:15:14',1,'2','uploads/transaction/transaction-20180915162929-1-TR000001.png|03b122b09901c03d4a696672a67ee2e4');
/*!40000 ALTER TABLE `z_transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_user`
--

DROP TABLE IF EXISTS `z_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `z_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `icon` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_user`
--

LOCK TABLES `z_user` WRITE;
/*!40000 ALTER TABLE `z_user` DISABLE KEYS */;
INSERT INTO `z_user` VALUES (1,'admin','admin@tutarajaya.lol','$2a$08$2vbNH9wWLdAc.EQ/iICve.iU50kIruARlQGpThGN6Wg5alYQpsM3m','assets/img/user.jpg','2018-07-29 20:56:34');
/*!40000 ALTER TABLE `z_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-16 23:43:39
