function imageHandler(fileId, textId, imageId) {
    // We can attach the `fileselect` event to all file inputs on the page
    $(fileId).change(function () {
        var input = $(this);
        var numFiles = input.get(0).files ? input.get(0).files.length : 1;
        var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger("fileselect", [numFiles, label]);
    });
    // We can watch for our custom `fileselect` event like this
    $(fileId).on("fileselect", function (event, numFiles, label) {
        var input = $(textId);
        var log = numFiles > 1 ? numFiles + " files selected" : label;

        input.text(log); // modified
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(imageId).attr("src", e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
}
