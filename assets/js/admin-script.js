function activateDatatables(tableId, options) {
    var settings = $.extend({
        ajax: {},
        oLanguage: {
            sSearch: '',
            sLengthMenu: '_MENU_'
        },
        initComplete: function (settings, json) {
            $('.dataTables_filter').children('input').attr('placeholder', 'Search...');
            // $('.dataTables_wrapper select').select2({
            //     minimumResultsForSearch: Infinity
            // });
        },
        dom: '<"top"fl<"clear">>rt<"bottom"ip<"clear">>',
        paging: true,
        lengthChange: true,
        ordering: true,
        info: true,
        autoWidth: true,
        processing: true, // Feature control the processing indicator.
        serverSide: true, // Feature control DataTables' server-side processing mode.
        order: [], // Initial no order.
        bDdestroy : true,
        columnDefs: [
            {
                'targets': [0, 1], // first column / numbering column
                'orderable': true, // set not orderable
            },
        ],
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, 'Semua']
        ],
        initComplete: null,
        fnDrawCallback: null
    }, options);
    return $(tableId).DataTable(settings);
}

function activateAjax(options) {
    var settings = $.extend({
        // response setting
        responsePlace: null,
        notifyDelay: 3000,
        // ajax setting
        url: null,
        type: null,
        data: null,
        dataType: 'json',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        processData: true,
        cache:false,
        async:false,
        // function after ajax setting
        functionDone: null,
        functionFail: null
    }, options);

    $.ajax({
        type: settings.type,
        data: settings.data,
        url: settings.url,
        dataType: settings.dataType,
        contentType: settings.contentType,
        processData: settings.processData,
    }).done(function (data) {
        switch (settings.dataType) {
            case 'html':
            default:
                $(settings.responsePlace).html(data);
                break;
            case 'json':
                $(settings.responsePlace).html(data.html);
                break;
        }

        if (settings.showNotification != 'none') {
            showNotification(settings.dataType, data, settings.notifyDelay);
        }

        if ($.isFunction(settings.functionDone)) {
            settings.functionDone(data);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 0) {
            alert("Not connect.n Verify Network.");
        } else if (jqXHR.status == 404) {
            alert("Requested page not found. [404]");
        } else if (jqXHR.status == 500) {
            alert("Internal Server Error [500].");
        } else if (textStatus === "parsererror") {
            alert("Requested JSON parse failed.");
        } else if (textStatus === "timeout") {
            alert("Time out error.");
        } else if (textStatus === "abort") {
            alert("Ajax request aborted.");
        } else {
            alert("Uncaught Error.n" + jqXHR.responseText);
        }
        if ($.isFunction(settings.functionFail)) {
            settings.functionFail(errorThrown);
        }
    });
}

function showNotification(dataType, data, notifyDelay) {
    // notification using bootstrap-notify.js
    var context = {};
    var defaultOptions = {
        delay: notifyDelay,
        type: 'info',
        placement: {
            from: 'bottom',
            align: 'right'
        }
    };
    if (dataType === 'json') {
        if (data.hasOwnProperty('notification')) {
            context = {
                message: data.notification.message
            };
            options = $.extend(defaultOptions, {
                // settings
                type: data.notification.type
            });
            $.notify(context, options);
        }
    } else if (dataType === 'html') {
        context = {
            message: data
        };
        options = defaultOptions;
        $.notify(context, options);
    }
}

function imageHandler(fileId, textId, imageId) {
    // We can attach the `fileselect` event to all file inputs on the page
    $(fileId).change(function () {
        var input = $(this);
        var numFiles = input.get(0).files ? input.get(0).files.length : 1;
        var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger("fileselect", [numFiles, label]);
    });
    // We can watch for our custom `fileselect` event like this
    $(fileId).on("fileselect", function (event, numFiles, label) {
        var input = $(textId);
        var log = numFiles > 1 ? numFiles + " files selected" : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log)
                alert(log);
        }
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(imageId).attr("src", e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
}