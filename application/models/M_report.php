<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_report extends MY_datatables
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public $status    = -1;
    public $startDate = null;
    public $endDate   = null;
    public function __construct()
    {
        $this->table         = 'z_transaksi';
        $this->column_order  = ['id', 'kode', 'nama_penerima', 'created_at', 'total', 'status']; // set column field database for datatable orderable
        $this->column_search = [null, 'id', 'kode', 'nama_penerima', 'created_at', 'total', 'status'];
        // set column field database for datatable searchable
        $this->order = [ // default order
            'id' => 'asc'
        ];
        parent::__construct();
    }

    public function mysql_custom_query()
    {
        $this->db->select('*')
        ->from($this->table);
        if (!empty($this->status) and $this->status != -1) {
            $this->db->where('status =', $this->status);
        }
        if (!empty($this->startDate) and !empty($this->endDate)) {
            $this->db
            ->where('created_at >=', $this->startDate)
            ->where('created_at <=', $this->endDate);
        }
    }
}
/* End of file M_report.php */
/* Location: ./application/models/M_report.php */
