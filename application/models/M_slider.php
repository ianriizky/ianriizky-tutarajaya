<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_slider extends MY_datatables
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        $this->table         = 'z_slider';
        $this->column_order  = ['id', null, 'foto', 'status']; // set column field database for datatable orderable
        $this->column_search = [null, 'id', 'foto', 'status'];
        // set column field database for datatable searchable
        $this->order = [ // default order
            'id' => 'asc'
        ];
        parent::__construct();
    }

}
/* End of file M_slider.php */
/* Location: ./application/models/M_slider.php */
