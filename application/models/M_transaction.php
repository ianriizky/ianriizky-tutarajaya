<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaction extends MY_datatables
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public $statusOnProcess = 0;
    public $statusConfirmed = 1;
    public $statusProcessed = 2;
    public $statusCode = [
        0,
        1,
        2
    ];
    public $statusText = [
        'Dalam proses',
        'Terkonfirmasi',
        'Sudah diproses'
    ];
    public $statusColor = [
        'btn-warning',
        'btn-info',
        'btn-success'
    ];
    public $statusBadge = [
        'badge-warning',
        'badge-info',
        'badge-success'
    ];
    public $statusIcon = [
        'fa-clock-o',
        'fa-credit-card',
        'fa-check'
    ];

    public function __construct()
    {
        $this->table         = 'z_transaksi';
        $this->column_order  = ['id', 'kode', 'nama_penerima', 'created_at', 'total', 'status']; // set column field database for datatable orderable
        $this->column_search = [null, 'id', 'kode', 'nama_penerima', 'created_at', 'total', 'status'];
        // set column field database for datatable searchable
        $this->order = [ // default order
            'id' => 'asc'
        ];
        parent::__construct();
    }

    // get detail transaction list data with product (from "z_detail_transaksi")
    public function getDetailWithProduct($condition = null)
    {
        $this->db->select(''
            . 'z_detail_transaksi.id AS dt_id, '
            . 'z_detail_transaksi.id_transaksi AS dt_idtransaksi, '
            . 'z_detail_transaksi.id_barang AS dt_idbarang, '
            . 'z_detail_transaksi.harga AS dt_harga, '
            . 'z_detail_transaksi.jumlah AS dt_jumlah, '
            . 'z_detail_transaksi.subtotal AS dt_subtotal, '
            . 'z_produk.nama AS p_nama')
        ->from('z_detail_transaksi')
        ->join('z_produk', 'z_detail_transaksi.id_barang = z_produk.id', 'left');
        if ($condition != null) {
            $this->db->where($condition);
        }
        return $this->db->group_by('z_detail_transaksi.id')->get();
    }
}
/* End of file M_transaction.php */
/* Location: ./application/models/M_transaction.php */
