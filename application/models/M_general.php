<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_general extends CI_Model
{

    /**
     * @author [Ari Sudarma] <[arisudarma@gmail.com]>
     */

    public function __construct()
    {
        parent::__construct();
    }

    // auth function
    public function verify($table, $condition)
    {
        return $this->edit($table, $condition);
    }

    // general function CRUD
    public function add($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function edit($table, $condition)
    {
        return $this->db->get_where($table, $condition);
    }

    public function update($table, $condition, $data)
    {
        return $this->db->update($table, $data, $condition);
    }

    public function delete($table, $condition)
    {
        return $this->db->delete($table, $condition);
    }

    public function getData($table)
    {
        return $this->db->get($table);
    }

    public function insertId($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function insBatch($table, $data)
    {
        return $this->db->insert_batch($table, $data);
    }

    public function updateBatch($table, $data)
    {
        return $this->db->update_batch($table, $data, 'id');
    }

    public function lastRecord($table)
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get($table, 1);
    }

    public function lastId($table)
    {
        $this->db->order_by('id', 'desc');
        $result = $this->db->get($table, 1, 0)->row_array();
        return (isset($result['id']) ? $result['id'] : 0);
    }

    public function countData($table, $data = null)
    {
        if (!empty($data)) {
            return $this->db->select()->from($table)->where($data)->count_all_results();
        } else {
            return $this->db->select()->from($table)->count_all_results();
        }
    }

    public function belanja($limit, $offset, $id)
    {
        $this->db->select('*');
        $this->db->from('z_transaksi');
        $this->db->limit($limit, $offset);
        $this->db->where('member_id', $id);
        $this->db->order_by('id', 'desc');
        return $this->db->get();
    }

    public function detailbelanja($id)
    {
        $this->db->select('z_transaksi.*, z_transaksi.kode as kode_transaksi, z_transaksi.status as status_transaksi, z_detail_transaksi.*, z_detail_transaksi.harga as harga_barang, z_produk.*');
        $this->db->from('z_transaksi');
        $this->db->join('z_detail_transaksi', 'z_transaksi.id = z_detail_transaksi.id_transaksi');
        $this->db->join('z_produk', 'z_detail_transaksi.id_barang = z_produk.id');
        $this->db->where('z_transaksi.id', $id);
        return $this->db->get()->result_array();
    }

    public function detailTransaksi($id)
    {
        $this->db->select('z_transaksi.*, z_transaksi.id as id_trans, z_transaksi.status as status_transaksi, z_detail_transaksi.*, z_member.*, z_member.alamat as alamat_member, z_produk.thumbnail, z_produk.kode as kode_barang, z_produk.nama as nama_barang');
        $this->db->from('z_transaksi');
        $this->db->join('z_detail_transaksi', 'z_transaksi.id = z_detail_transaksi.id_transaksi');
        $this->db->join('z_member', 'z_transaksi.member_id = z_member.id');
        $this->db->join('z_produk', 'z_produk.id = z_detail_transaksi.id_barang');
        $this->db->where('z_transaksi.id', $id);
        return $this->db->get();
    }

    public function updateStok($table, $con, $stok)
    {
        $this->db->set('stok', 'stok-' . $stok, false);
        $this->db->where($con);
        return $this->db->update($table);
    }

    public function datauser($con)
    {
        $this->db->select('z_member.*');
        $this->db->from('z_transaksi');
        $this->db->join('z_member', 'z_member.id = z_transaksi.member_id');
        $this->db->where('z_transaksi.id', $con['id']);
        return $this->db->get()->row_array();
    }
}
/* End of file M_general.php */
/* Location: ./application/models/M_general.php */
