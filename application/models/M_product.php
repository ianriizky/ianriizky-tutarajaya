<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends MY_datatables
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        $this->table         = 'z_produk';
        $this->column_order  = ['z_produk.id', 'z_produk.kode', 'z_produk.nama', 'z_kategori.kategori']; // set column field database for datatable orderable
        $this->column_search = [null, 'z_produk.id', 'z_produk.kode', 'z_produk.nama', 'z_kategori.kategori'];
        // set column field database for datatable searchable
        $this->order = [ // default order
            'z_produk.id' => 'asc'
        ];
        // var_dump($this->query);
        // die();
        parent::__construct();
    }

    public function mysql_custom_query()
    {
        $this->db->select(''
            . 'z_produk.id AS p_id, '
            . 'z_produk.kode AS p_kode, '
            . 'z_produk.nama AS p_nama, '
            . 'z_kategori.kategori AS k_kategori')
        ->from($this->table)
        ->join('z_kategori', $this->table . '.kategori = z_kategori.id', 'left')
        ->group_by('z_produk.id');
    }

    // get product list data with category (from "z_kategori"), stock (from "z_produk_stok"), and picture (from "z_produk_gambar")
    public function get($condition = [], $withPicture = false)
    {
        $this->db->select(''
            . 'z_produk.id AS p_id, '
            . 'z_produk.kode AS p_kode, '
            . 'z_produk.nama AS p_nama, '
            . 'z_produk.deskripsi AS p_deskripsi, '
            . 'z_produk.kategori AS p_kategori, '
            . 'z_produk.harga AS p_harga, '
            . 'z_produk.diskon AS p_diskon, '
            . 'z_produk.berat AS p_berat, '
            . 'z_produk.status AS p_status, '
            . 'z_produk.popular AS p_popular, '
            . 'z_kategori.kategori AS k_kategori, ');
        if ($withPicture) {
            $this->db->select(''
                . 'z_produk_stok.stok AS ps_stok, '
                . 'GROUP_CONCAT(z_produk_gambar.gambar SEPARATOR \'|\') AS pg_gambar');
        } else {
            $this->db->select(''
                . 'z_produk_stok.stok AS ps_stok');
        }
        $this->db->from($this->table)
        ->join('z_kategori', $this->table . '.kategori = z_kategori.id', 'left')
        ->join('z_produk_stok', $this->table . '.id = z_produk_stok.id_produk', 'inner');
        if ($withPicture) {
            $this->db->join('z_produk_gambar', $this->table . '.id = z_produk_gambar.id_produk', 'left');
        }
        if (!empty($condition)) {
            $this->db->where($condition);
        }
        return $this->db->group_by($this->table . '.id')->get();
    }

    // get product picture list data with name (from "z_produk")
    public function getWithPicture($condition = null)
    {
        $this->db->select(''
            . 'z_produk.id AS p_id, '
            . 'z_produk.nama AS p_nama, '
            . 'z_produk_gambar.id AS pg_id, '
            . 'z_produk_gambar.gambar AS pg_gambar')
        ->from($this->table)
        ->join('z_produk_gambar', $this->table . '.id = z_produk_gambar.id_produk', 'left');
        if ($condition != null) {
            $this->db->where($condition);
        }
        return $this->db->group_by('z_produk.id')->get();
    }

    public function getLastStock($productId)
    {
        $result = $this->general->edit('z_produk_stok', [
            'id_produk' => $productId
        ]);
        if ($result->num_rows() === 1) {
            $produkStock = $result->row_array();
            return $produkStock['stok'];
        } else {
            return 0;
        }
    }
}
/* End of file M_product.php */
/* Location: ./application/models/M_product.php */
