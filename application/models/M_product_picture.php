<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product_picture extends MY_datatables
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public $productId = 0;
    public function __construct()
    {
        $this->table         = 'z_produk_gambar';
        $this->column_order  = ['z_produk_gambar.id', null, 'z_produk_gambar.gambar']; // set column field database for datatable orderable
        $this->column_search = [null, 'z_produk_gambar.id', 'z_produk_gambar.gambar'];
        // set column field database for datatable searchable
        $this->order = [ // default order
            'z_produk_gambar.id' => 'asc'
        ];
        // var_dump($this->query);
        // die();
        parent::__construct();
    }

    public function mysql_custom_query()
    {
        $this->db->select($this->table . '.*')
        ->from('z_produk')
        ->join($this->table, 'z_produk.id = ' . $this->table . '.id_produk', 'left')
        ->where(['id_produk' => $this->productId])
        ->group_by('z_produk_gambar.id');
    }

}
/* End of file M_product_picture.php */
/* Location: ./application/models/M_product_picture.php */
