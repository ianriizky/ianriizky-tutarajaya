<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_home extends CI_Model
{

    /**
     * @author [Ari Sudarma] <[arisudarma@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function verify($table, $condition)
    {
        return $this->db->get_where($table, $condition);
    }

    public function getData($table)
    {
        return $this->db->get($table);
    }

    public function update($table, $condition, $data)
    {
        return $this->db->update($table, $data, $condition);
    }

    public function getWhere($table, $con)
    {
        return $this->db->get_where($table, $con);
    }

    public function insert($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function arrival()
    {
        $this->db->select('z_produk.*, z_kategori.kategori as nama_kategori');
        $this->db->from('z_produk');
        $this->db->join('z_kategori', 'z_produk.kategori = z_kategori.id');
        $this->db->limit(6);
        $this->db->order_by('z_produk.id', 'desc');
        return $this->db->get()->result_array();
    }

    public function popular()
    {
        $this->db->select('z_produk.*, z_kategori.kategori as nama_kategori');
        $this->db->from('z_produk');
        $this->db->join('z_kategori', 'z_produk.kategori = z_kategori.id');
        $this->db->where('z_produk.popular', '1');
        $this->db->limit(6);
        $this->db->order_by('z_produk.id', 'desc');
        return $this->db->get()->result_array();
    }

    public function dataTips($limit, $offset)
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('z_tips', $limit, $offset);
    }

    public function getDataSlider($table)
    {
        $this->db->where('status', '1');
        $this->db->order_by('id', 'desc');
        return $this->db->get($table);
    }

    public function edit($table, $con)
    {
        return $this->db->get_where($table, $con);
    }

    public function cekmail($email)
    {
        $this->db->select('id');
        $this->db->where('email', $email);
        $this->db->from('z_member');
        return $this->db->get();
    }

    public function detailProduk($id)
    {
        $this->db->select('z_produk.*, z_kategori.kategori as nama_kategori, GROUP_CONCAT(z_produk_size.size SEPARATOR ',
            ') as size, GROUP_CONCAT(z_produk_size.stok SEPARATOR ', ') as stok');
        $this->db->from('z_produk');
        $this->db->join('z_kategori', 'z_produk.kategori = z_kategori.id');
        $this->db->join('z_produk_size', 'z_produk.id = z_produk_size.id_produk');
        $this->db->where('z_produk.id', $id);
        $this->db->group_by('z_produk.id');
        return $this->db->get();
    }

    public function getGambar($id)
    {
        return $this->db->get_where('z_produk_gambar', array('id_produk' => $id));
    }

    public function another($id)
    {
        $this->db->select('z_produk.*, z_kategori.kategori as nama_kategori');
        $this->db->from('z_produk');
        $this->db->join('z_kategori', 'z_produk.kategori = z_kategori.id');
        $this->db->where_not_in('z_produk.id', $id);
        $this->db->limit(6);
        $this->db->order_by('z_produk.id', 'desc');
        return $this->db->get()->result_array();
    }

    public function dataProduk($limit, $offset, $search, $price, $kategori, $query)
    {
        $this->db->select('z_produk.*, z_kategori.kategori as nama_kategori');
        $this->db->from('z_produk');
        $this->db->join('z_kategori', 'z_produk.kategori = z_kategori.id');
        $this->db->limit($limit, $offset);
        if ($search == 3) {
            $this->db->order_by('z_produk.popular', 'desc');
        } elseif ($search == 4) {
            $this->db->order_by('z_produk.harga', 'asc');
        } elseif ($search == 5) {
            $this->db->order_by('z_produk.harga', 'desc');
        } elseif ($search == 2 || $search == '') {
            $this->db->order_by('z_produk.id', 'desc');
        }
        if (!empty($query)) {
            $this->db->like('z_produk.nama', $query, 'both');
        }
        if ($kategori != '' || $kategori != null) {
            $this->db->where('z_produk.kategori', $kategori);
        }
        if ($price != null) {
            $this->db->where('z_produk.harga>=', $price[0]);
            $this->db->where('z_produk.harga<=', $price[1]);
        }
        return $this->db->get()->result_array();
    }

    public function getDataSearch($search, $price, $kategori, $query)
    {
        $this->db->select('z_produk.*, z_kategori.kategori as nama_kategori');
        $this->db->from('z_produk');
        $this->db->join('z_kategori', 'z_produk.kategori = z_kategori.id');
        if ($search == 3) {
            $this->db->order_by('z_produk.popular', 'desc');
        } elseif ($search == 4) {
            $this->db->order_by('z_produk.harga', 'asc');
        } elseif ($search == 5) {
            $this->db->order_by('z_produk.harga', 'desc');
        } elseif ($search == 2 || $search == '') {
            $this->db->order_by('z_produk.id', 'desc');
        }
        if (!empty($query)) {
            $this->db->like('z_produk.nama', $query, 'both');
        }
        if ($kategori != '' || $kategori != null) {
            $this->db->where('z_produk.kategori', $kategori);
        }
        if ($price != null) {
            $this->db->where('z_produk.harga>=', $price[0]);
            $this->db->where('z_produk.harga<=', $price[1]);
        }
        return $this->db->get();
    }

    public function lastRecord($table)
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get($table, 1);
    }

    public function insertId($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function insBatch($table, $data)
    {
        return $this->db->insert_batch($table, $data);
    }

    public function delete($table, $condition)
    {
        return $this->db->delete($table, $condition);
    }

    public function maxprice()
    {
        $this->db->select('MAX(harga) as maxprice');
        $this->db->from('z_produk');
        return $this->db->get()->row_array();
    }

    public function cekStatusProduk($id)
    {
        $this->db->select('status');
        $this->db->from('z_produk');
        $this->db->where('id', $id);
        return $this->db->get()->row_array();
    }
}
/* End of file M_home.php */
/* Location: ./application/models/M_home.php */
