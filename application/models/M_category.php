<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_category extends MY_datatables
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */

    public function __construct()
    {
        $this->table         = 'z_kategori';
        $this->column_order  = ['id', 'kategori']; // set column field database for datatable orderable
        $this->column_search = [null, 'id', 'kategori'];
        // set column field database for datatable searchable
        $this->order = [ // default order
            'id' => 'asc'
        ];
        parent::__construct();
    }

}
/* End of file M_category.php */
/* Location: ./application/models/M_category.php */
