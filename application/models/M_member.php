<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends MY_datatables
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */

    public function __construct()
    {
        parent::__construct();
        $this->table         = 'z_member';
        $this->column_order  = ['id', 'username', 'fullname', 'email', 'status']; // set column field database for datatable orderable
        $this->column_search = [null, 'id', 'username', 'fullname', 'email', 'status'];
        // set column field database for datatable searchable
        $this->order = [ // default order
            'id' => 'asc'
        ];
    }

}
/* End of file M_member.php */
/* Location: ./application/models/M_member.php */
