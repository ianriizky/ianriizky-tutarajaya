<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminProfile extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect admin if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(ADMIN . '/login?redirect=' . urlencode(current_url()));
        }
    }

    public function show()
    {
        $page['title']      = 'Profil';
        $page['nav-active'] = 'dashboard';

        // redirect user if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->sess_destroy();
            show_error('Forbidden access', 403);
        }

        $id = $this->session->userdata('user')['id'];
        $result = $this->general->edit('z_user', ['id' => $id]);
        if ($result->num_rows() === 1) {
            $user   = $result->row_array();
            $data = [
                'id'              => [
                    'value' => $user['id']
                ],
                'username'        => [
                    'value' => $user['username']
                ],
                'email'           => [
                    'value' => $user['email']
                ],
                'mode-edit'    => false,
                'selected-tab' => 'update-profile'
            ];
            $this->load->view('admin/profile/index', compact('page', 'data'));
        } else {
            show_error('Forbidden access', 403);
        }
    }

    public function store()
    {
        // redirect user if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->sess_destroy();
            show_error('Forbidden access', 403);
        }

        $id = $this->session->userdata('user')['id'];
        $result = $this->general->edit('z_user', ['id' => $id]);
        $this->load->library('form_validation');
        if ($result->num_rows() === 1) {
            $selectedTab = strlen($this->input->post('selected-tab')) > 0 ? $this->input->post('selected-tab') : '';
            switch ($selectedTab) {
                case 'update-profile': {
                    // see codeigniter reference rules (required, alpha_dash, etc)
                    // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
                    $this->form_validation->set_rules('username', 'username', 'trim|required|alpha_dash|min_length[4]|max_length[15]|edit_unique[z_member.username.id.' .  $id. ']', [
                        'edit_unique' => 'The username has been registered already'
                    ]);
                    $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|edit_unique[z_member.email.id.' .  $id. ']', [
                        'edit_unique' => 'The email has been registered already'
                    ]);
                    // if form validation is fail
                    if ($this->form_validation->run() == false) {
                        $this->session->set_flashdata([ // show error form validation is session/flashdata
                            'form_data' => [
                                'id' => [
                                    'value' => $id
                                ],
                                'username'        => [
                                    'value' => set_value('username'),
                                    'error' => form_error('username')
                                ],
                                'email'           => [
                                    'value' => set_value('email'),
                                    'error' => form_error('email')
                                ],
                                'mode-edit' => true,
                                'selected-tab' => $action
                            ]
                        ]);
                        redirect(site_url(ADMIN . '/profile'));
                    // else if form validation is success
                    } else {
                        $result = $this->general->edit('z_user', [
                            'id' => $id
                        ]);
                        if ($result->num_rows() === 1) {
                            $condition = [
                                'id' => $id
                            ];
                            $collection = [ // collect data from register-form (true for avoid xss injection)
                                'username'   => $this->input->post('username', true),
                                'email'      => $this->input->post('email', true)
                            ];
                            // udpate data into table and check if update process is success or not
                            // if update process is success
                            if ($this->general->update('z_user', $condition, $collection)) {
                                $message = 'Profil anda berhasil diubah.';
                                $this->session->set_flashdata([
                                    'type'    => 'alert-success',
                                    'message' => $message
                                ]);
                                redirect(site_url(ADMIN . '/profile'));
                            // else if update process is error
                            } else {
                                $message = 'Maaf, profil anda gagal diubah. Silahkan coba lagi.';
                                $this->session->set_flashdata([
                                    'type'    => 'alert-danger',
                                    'message' => $message,
                                    'form_data' => [
                                        'username'  => [
                                            'value' => $collection['username']
                                        ],
                                        'email'     => [
                                            'value' => $collection['email']
                                        ],
                                        'mode-edit' => true,
                                        'selected-tab' => $action
                                    ]
                                ]);
                                redirect(site_url(ADMIN . '/profile'));
                            }
                        } else {
                            show_error('Forbidden access', 403);
                        }
                    }
                }
                break;
                
                case 'reset-password': {
                    $user = $result->row_array();
                    // see codeigniter reference rules (required, alpha_dash, etc)
                    // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
                    $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]');
                    $this->form_validation->set_rules('retype-password', 'retype password', 'trim|required|matches[password]');
                    // if form validation is fail
                    if ($this->form_validation->run() == false) {
                        $this->session->set_flashdata([ // show error form validation is session/flashdata
                            'form_data' => [
                                'id' => [
                                    'value' => $id
                                ],
                                'password'        => [
                                    // empty value
                                    'error' => form_error('password')
                                ],
                                'retype-password' => [
                                    // empty value
                                    'error' => form_error('retype-password')
                                ],
                                'username'        => [
                                    'value' => $user['username']
                                ],
                                'email'           => [
                                    'value' => $user['email']
                                ],
                                'mode-edit' => true,
                                'selected-tab' => $selectedTab
                            ]
                        ]);
                        redirect(site_url(ADMIN . '/profile'));
                    // else if form validation is success
                    } else {
                        $result = $this->general->edit('z_user', [
                            'id' => $id
                        ]);
                        if ($result->num_rows() === 1) {
                            $this->load->library('password');
                            $condition = [
                                'id' => $id
                            ];
                            $collection = [ // collect data (true for avoid xss injection)
                                'password' => $this->password->hash($this->input->post('password', true))
                            ];
                            // udpate data into table and check if update process is success or not
                            // if update process is success
                            if ($this->general->update('z_user', $condition, $collection)) {
                                $message = 'Proses ganti password anda berhasil dilakukan.';
                                $this->session->set_flashdata([
                                    'type'    => 'alert-success',
                                    'message' => $message
                                ]);
                                redirect(site_url(ADMIN . '/profile'));
                            // else if update process is error
                            } else {
                                $message = 'Maaf, proses ganti password anda gagal dilakukan. Silahkan coba lagi.';
                                $this->session->set_flashdata([
                                    'type'    => 'alert-danger',
                                    'message' => $message,
                                    'form_data' => [
                                        'id' => [
                                            'value' => $id
                                        ],
                                        'password'        => [
                                            // empty value
                                            'error' => form_error('password')
                                        ],
                                        'retype-password' => [
                                            // empty value
                                            'error' => form_error('retype-password')
                                        ],
                                        'username'        => [
                                            'value' => $user['username']
                                        ],
                                        'email'           => [
                                            'value' => $user['email']
                                        ],
                                        'mode-edit' => true,
                                        'selected-tab' => $selectedTab
                                    ]
                                ]);
                                redirect(site_url(ADMIN . '/profile'));
                            }
                        } else {
                            show_error('Forbidden access', 403);
                        }
                    }
                }
                break;
            }
        } else {
            show_error('Forbidden access', 403);
        }
    }

}
/* End of file AdminProfile.php */
/* Location: ./application/controllers/AdminProfile.php */
