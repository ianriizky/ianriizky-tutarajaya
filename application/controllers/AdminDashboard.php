<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminDashboard extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect admin if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(ADMIN . '/login');
        }
    }

    public function index()
    {
        $page['title']      = 'Dashboard';
        $page['nav-active'] = 'dashboard';

        // collect transaction-list
        $transactions = $this->db->get_where('z_transaksi', 
            [
                'DATE_FORMAT(created_at, \'%Y-%m-%d\') >=' => date('Y-m-d', strtotime('-7 days')),
                'DATE_FORMAT(created_at, \'%Y-%m-%d\') <=' => date('Y-m-d')
            ] , 5, 0)->result_array();
        $this->load->model('M_transaction', 'transaction');
        foreach ($transactions as $index => $transaction) {
            $transactions[$index]['status_text']  = $this->transaction->statusText[(int) $transaction['status']];
            $transactions[$index]['status_badge'] = $this->transaction->statusBadge[(int) $transaction['status']];
            $transactions[$index]['status_icon'] = $this->transaction->statusIcon[(int) $transaction['status']];
        }
        // collect data for dashboard
        $data = [
            'confirmation-total' => $this->general->countData('z_transaksi', [
                'status' => '1',
            ]),
            'transaction-week' => $this->general->countData('z_transaksi', [
                'created_at >=' => date('Y-m-d', strtotime('-7 days')),
                'created_at <=' => date('Y-m-d')
            ]),
            'transaction-total' => $this->general->countData('z_transaksi'),
            'member-total'      => $this->general->countData('z_member'),
            'transaction-list'  => $transactions
        ];
        $this->load->view('admin/dashboard/index', compact('page', 'data'));
    }

}
/* End of file AdminDashboard.php */
/* Location: ./application/controllers/AdminDashboard.php */
