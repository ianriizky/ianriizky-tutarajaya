<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserAuthentication extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
    }

    public function loginGet()
    {
        // if user has been login, no need to login anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        $page['title']      = 'Login';
        $page['nav-active'] = 'login';
        $this->load->view('user/authentication/login', compact('page'));
    }

    public function loginPost()
    {
        // if user has been login, can't login anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        // if form_validation is fail (username/email are not filled)
        if ($this->form_validation->run() == false) {
            // show error form validation is session/flashdata
            $this->session->set_flashdata([
                'form_data' => [
                    'email'    => [
                        'value' => set_value('email'),
                        'error' => form_error('email')
                    ],
                    'password' => [
                        'value' => set_value('password'),
                        'error' => form_error('password')
                    ]
                ]
            ]);
            redirect(site_url('login' . (!empty($this->input->post('redirect')) ? '?redirect=' . urlencode($this->input->post('redirect')) : '')));
        // else if form validation is success
        } else {
            $usernameEmail = $this->input->post('email', true); // checking user data from email input
            $result        = $this->general->verify('z_member', '' // check if user data is exist on database or not
                // where query
                . 'username = \'' . $usernameEmail . '\''
                . ' OR email = \'' . $usernameEmail . '\''
            );
            // if user data is exist on database
            if ($result->num_rows() === 1) {
                $this->load->library('password');
                $member   = $result->row_array();
                $password = $this->input->post('password', true);
                // if member's password is correct
                if ($this->password->check($password, $member['password'])) {
                    if ($member['status'] == 1) { // only active account who can login
                        $this->session->set_userdata('member', $member); // set session of member
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => 'Selamat datang <strong>' . $member['fullname'] . '</strong>! Anda telah berhasil login.'
                        ]);
                        if (!empty($this->input->post('redirect'))) {
                            redirect(urldecode($this->input->post('redirect')));
                        } else {
                            redirect(site_url());
                        }
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-danger',
                            'message' => 'Maaf, akun anda belum terkonfirmasi.',
                            'form_data' => [
                                'email'    => [
                                    'value' => $this->input->post('email', true)
                                ]
                            ]
                        ]);
                        redirect(site_url('login' . (!empty($this->input->post('redirect')) ? '?redirect=' . urlencode($this->input->post('redirect')) : '')));
                    }
                // else if member's password is wrong
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Maaf, password yang anda masukkan salah.',
                        'form_data' => [
                            'email'    => [
                                'value' => $this->input->post('email', true)
                            ]
                        ]
                    ]);
                    redirect(site_url('login' . (!empty($this->input->post('redirect')) ? '?redirect=' . urlencode($this->input->post('redirect')) : '')));
                }
            // else if user data is not exist on database
            } else {
                $this->session->set_flashdata([
                    'type'    => 'alert-danger',
                    'message' => 'Maaf, email dan password yang anda masukkan tidak ditemukan.',
                    'form_data' => [
                        'email'    => [
                            'value' => $this->input->post('email', true)
                        ]
                    ]
                ]);
                redirect(site_url('login' . (!empty($this->input->post('redirect')) ? '?redirect=' . urlencode($this->input->post('redirect')) : '')));
            }
        }
    }

    public function passwordResetGet()
    {
        // if user has been login, can't request forgot password anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        $page['title']      = 'Lupa Password';
        $page['nav-active'] = 'login';
        $this->load->view('user/authentication/password-forgot', compact('page'));
    }

    public function passwordResetPost()
    {
        // if user has been login, can't request forgot password anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        // if form_validation is fail (username are not filled)
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata([ // show error form validation is session/flashdata
                'form_data' => [
                    'email' => [
                        'value' => set_value('email'),
                        'error' => form_error('email')
                    ]
                ]
            ]);
            redirect(site_url('password/reset'));
        // else if form_validation is success
        } else {
            $result = $this->general->verify('z_member', [
                'email' => $this->input->post('email', true)
            ]);
            if ($result->num_rows() === 1) {
                $member = $result->row_array();
                $condition = [
                    'id' => $member['id']
                ];
                $collection = [
                    'token' => md5($this->input->post('email', true))
                ];
                if ($this->general->update('z_member', $condition, $collection)) {
                    $page = [
                        'title'     => 'Konfirmasi Lupa Password',
                        'name'      => $this->config->item('site_name'),
                        'email'     => $member['email'],
                        'link'      => site_url('password/reset/verify/' . md5($member['email']))
                    ];
                    // load email template
                    $template = $this->load->view('email/password-forgot-confirmation', compact('page'), true);
                    // send reset password confirmation email
                    $this->load->library('email');
                    $this->email->to($page['email']);
                    $this->email->from('no-reply@tutarajaya.lol', $page['name']);
                    $this->email->subject($page['title']);
                    $this->email->message($template);
                    if ($this->email->send()) {
                        $this->load->view('user/authentication/success', [
                            'title'   =>  $page['title'],
                            'message' => 'Proses reset password anda berhasil dilakukan. Silahkan cek email anda di <strong>' . $page['email'] . '</strong> untuk proses selanjutnya.'
                        ]);
                    } else {
                        $this->load->view('user/authentication/success', [
                            'title'   =>  $page['title'],
                            'message' => 'Proses reset password anda berhasil dilakukan. Kami akan segera mengirimkan konfirmasi ke email anda untuk langkah reset password selanjutnya.'
                                        // . $this->email->print_debugger()
                        ]);
                    }
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Maaf, proses reset password akun anda tidak berhasil dilakukan. Silahkan coba lagi'
                    ]);
                    redirect(site_url());
                }
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'email' => [
                            'value' => set_value('email'),
                            'error' => 'Maaf, akun anda tidak ditemukan'
                        ]
                    ]
                ]);
                redirect(site_url('password/reset'));
            }
        }
    }

    public function passwordResetVerifyGet($encryptUsername = null)
    {
        // if user has been login, can't password reset verify anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        if ($encryptUsername != null) {
            // get member data from "z_member" table with encryptUsername (md5(email))
            $result = $this->general->verify('z_member', [
                'token' => $encryptUsername
            ]);
            if ($result->num_rows() === 1) {
                $member = $result->row_array();
                $page = [
                    'title'     => 'Reset Password',
                    'id'        => $member['id'],
                    'link'      => $encryptUsername
                ];
                $this->load->view('user/authentication/password-reset', compact('page'));
            } else {
                $this->session->set_flashdata([
                    'type'    => 'alert-danger',
                    'message' => 'Maaf, akun anda tidak ditemukan.'
                ]);
                redirect(site_url());
            }
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Maaf, kode reset password anda salah.'
            ]);
            redirect(site_url());
        }
    }

    public function passwordResetVerifyPost($encryptUsername = null)
    {
        // if user has been login, can't password reset verify anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        if ($encryptUsername != null) {
            // get member data from "z_member" table with encryptUsername (md5(email))
            $result = $this->general->verify('z_member', [
                'token' => $encryptUsername 
            ]);
            if ($result->num_rows() === 1) {
                $this->load->library('form_validation');
                // see codeigniter reference rules (required, alpha_dash, etc)
                // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
                $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]');
                $this->form_validation->set_rules('retype-password', 'retype password', 'trim|required|matches[password]');
                // if form validation is fail
                if ($this->form_validation->run() == false) {
                    $this->session->set_flashdata([ // show error form validation is session/flashdata
                        'form_data' => [
                            'password'        => [
                                // empty value
                                'error' => form_error('password')
                            ],
                            'retype-password' => [
                                // empty value
                                'error' => form_error('retype-password')
                            ]
                        ]
                    ]);
                    redirect(site_url('password/reset/verify/' . $encryptUsername));
                } else {
                    $this->load->library('password');
                    $condition = [
                        'id' => $this->input->post('id', true)
                    ];
                    $collection = [
                        'token'    => null, // reset token
                        'password' => $this->password->hash($this->input->post('password', true))
                    ];
                    if ($this->general->update('z_member', $condition, $collection)) {
                        // lakukan proses login dan set userdata 
                        $result = $this->general->verify('z_member', [
                            'id' => $this->input->post('id', true)
                        ]);
                        if ($result->num_rows() === 1) {
                            $member = $result->row_array();
                            $this->session->set_userdata('member', $member); // set session of member
                            $this->session->set_flashdata([
                                'type'    => 'alert-success',
                                'message' => 'Selamat datang <strong>' . $member['fullname'] . '</strong>! Proses reset password akun anda telah berhasil dilakukan.'
                            ]);
                            redirect(site_url());
                        } else {
                            $this->session->set_flashdata([
                                'type'    => 'alert-danger',
                                'message' => 'Proses reset password akun anda telah berhasil dilakukan. Silahkan login kembali menggunakan password baru anda.'
                            ]);
                            redirect(site_url());
                        }
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-danger',
                            'message' => 'Maaf, proses reset password akun anda tidak berhasil dilakukan. Silahkan coba lagi'
                        ]);
                        redirect(site_url());
                    }
                }
            }
        }
    }

    public function registerGet()
    {
        // if user has been login, can't request forgot password anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        $page['title']      = 'Daftar';
        $page['nav-active'] = 'register';
        $this->load->view('user/authentication/register', compact('page'));
    }

    public function registerPost()
    {
        // if user has been login, can't request forgot password anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('username', 'username', 'trim|required|alpha_dash|min_length[4]|max_length[15]|is_unique[z_member.username]', [
            // custom error message
            'is_unique' => 'The username has been registered already',
        ]);
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[z_member.email]', [
            // custom error message
            'is_unique' => 'The email has been registered already'
        ]);
        $this->form_validation->set_rules('fullname', 'full name', 'required|max_length[100]');
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('retype-password', 'retype password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('alamat', 'address', 'max_length[200]');
        $this->form_validation->set_rules('province', 'province', 'max_length[100]');
        $this->form_validation->set_rules('city', 'city', 'max_length[100]');
        $this->form_validation->set_rules('telp', 'telephone number', 'numeric|max_length[15]');
        $this->form_validation->set_rules('term-agreement', 'term and agreement', 'required', [
            'required' => 'You must agree our term and agreement before submitting.'
        ]);
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata([ // show error form validation is session/flashdata
                'form_data' => [
                    'username'        => [
                        'value' => set_value('username'),
                        'error' => form_error('username')
                    ],
                    'email'           => [
                        'value' => set_value('email'),
                        'error' => form_error('email')
                    ],
                    'fullname'        => [
                        'value' => set_value('fullname'),
                        'error' => form_error('fullname')
                    ],
                    'password'        => [
                        // empty value
                        'error' => form_error('password')
                    ],
                    'retype-password' => [
                        // empty value
                        'error' => form_error('retype-password')
                    ],
                    'alamat'         => [
                        'value' => set_value('alamat'),
                        'error' => form_error('alamat')
                    ],
                    'province'        => [
                        'value' => set_value('province'),
                        'error' => form_error('province')
                    ],
                    'city'            => [
                        'value' => set_value('city'),
                        'error' => form_error('city')
                    ],
                    'telp'            => [
                        'value' => set_value('telp'),
                        'error' => form_error('telp')
                    ],
                    'term-agreement'            => [
                        'value' => set_value('term-agreement'),
                        'error' => form_error('term-agreement')
                    ]
                ]
            ]);
            redirect(site_url('register'));
        // else if form validation is success
        } else {
            $this->load->library('password');
            $collection = [ // collect data from register-form (true for avoid xss injection)
                'id'         => (int) $this->general->lastId('z_member') + 1,
                'username'   => $this->input->post('username', true),
                'fullname'   => $this->input->post('fullname', true),
                'email'      => $this->input->post('email', true),
                'password'   => $this->password->hash($this->input->post('password', true)),
                'created_at' => date('Y-m-d H:i:s'),
                'status'     => '0', // set 0 to inactive account, need email confirm to change it to 1
                'province'   => $this->input->post('province', true),
                'city'       => $this->input->post('city', true),
                'alamat'     => $this->input->post('alamat', true),
                'telp'       => $this->input->post('telp', true)
            ];
            // insert collection into "z_member" table and check if insert process is success or not
            // if insert process is success
            if ($memberId = $this->general->insertId('z_member', $collection)) {
                $this->load->library('email');
                $page = [
                    'title'     => 'Konfirmasi Akun',
                    'name'      => $this->config->item('site_name'),
                    'email'     => $collection['email'],
                    'link'      => site_url('register/verify/' . md5($collection['username'] . '-' . $collection['email']))
                ];
                // load email template
                $template = $this->load->view('email/register-confirmation', compact('page'), true);
                $this->email->to($page['email']);
                $this->email->from('no-reply@tutarajaya.lol', $page['name']);
                $this->email->subject($page['title']);
                $this->email->message($template);
                if ($this->email->send()) {
                    $this->load->view('user/authentication/success', [
                        'title'   =>  $page['title'],
                        'message' => 'Registrasi anda berhasil dilakukan. Silahkan cek email anda di <strong>' . $page['email'] . '</strong> untuk proses konfirmasi selanjutnya.'
                    ]);
                } else {
                    $this->load->view('user/authentication/success', [
                        'title'   =>  $page['title'],
                        'message' => 'Registrasi anda berhasil dilakukan. Kami akan segera mengirimkan konfirmasi ke email anda untuk langkah pendaftaran selanjutnya.'
                                    // . $this->email->print_debugger()
                    ]);
                }
            // else if insert process is error
            } else {
                $this->session->set_flashdata([
                    'type'    => 'alert-danger',
                    'message' => 'Maaf, proses registrasi anda gagal. Silahkan coba lagi.',
                    'form_data' => [
                        'username'  => [
                            'value' => $collection['username']
                        ],
                        'email'     => [
                            'value' => $collection['email']
                        ],
                        'fullname'  => [
                            'value' => $collection['fullname']
                        ],
                        'alamat'   => [
                            'value' => $collection['alamat']
                        ],
                        'province'  => [
                            'value' => $collection['province']
                        ],
                        'city'      => [
                            'value' => $collection['city']
                        ],
                        'telp'      => [
                            'value' => $collection['telp']
                        ]
                    ]
                ]);
                redirect(site_url('register'));
            }
        }
    }

    public function registerVerifyGet($encryptUsername = null)
    {
        // if user has been login, can't register verify anymore (redirect to site_url())
        if ($this->session->has_userdata('member')) {
            redirect(site_url());
        }

        if ($encryptUsername != null) {
            // get member data from "z_member" table with encryptUsername (md5(username-email))
            $result = $this->general->verify('z_member', ''
                . 'MD5(CONCAT(z_member.username, \'-\', z_member.email)) = \'' . $encryptUsername . '\''
            );
            if ($result->num_rows() === 1) {
                $member = $result->row_array();
                // jika sudah teraktivasi, langsung redirect ke site_url
                if ($member['status'] == '1') {
                    $this->session->set_flashdata([
                        'type'    => 'alert-warning',
                        'message' => 'Akun anda sudah berhasil diverifikasi sebelumnya.'
                    ]);
                    redirect(site_url());
                } else {
                    $condition = [
                        'id' => $member['id']
                    ];
                    $collection = [
                        'status' => '1'
                    ];
                    if ($this->general->update('z_member', $condition, $collection)) {
                        $this->session->set_userdata('member', $member);
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => 'Selamat datang <strong>' . $member['fullname'] . '</strong>! Akun anda telah berhasil diverifikasi.'
                        ]);
                        redirect(site_url());
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-danger',
                            'message' => 'Maaf, akun anda tidak berhasil diverifikasi. Silahkan coba lagi'
                        ]);
                        redirect(site_url());
                    }
                }
            } else {
                $this->session->set_flashdata([
                    'type'    => 'alert-danger',
                    'message' => 'Maaf, akun anda tidak ditemukan.'
                ]);
                redirect(site_url());
            }
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Maaf, kode verifikasi anda salah.'
            ]);
            redirect(site_url());
        }
    }

    public function logoutPost()
    {
        $this->session->sess_destroy();
        redirect(site_url('logout/success'));
    }

    public function logoutSuccessGet()
    {
        // if user has been login, can't see logout success (redirect to site_url())
        if (!$this->session->has_userdata('member')) {
            $this->session->set_flashdata([
                'type'    => 'alert-success',
                'message' => 'Anda telah berhasil logout.',
            ]);
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Proses logout gagal dilakukan.',
            ]);
        }
        redirect(site_url());
    }
}
/* End of file UserAuthentication.php */
/* Location: ./application/controllers/UserAuthentication.php */