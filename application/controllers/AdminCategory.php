<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminCategory extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    private $readable = 'kategori';
    private $navActive = 'master';

    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect admin if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(ADMIN . '/login?redirect=' . urlencode(current_url()));
        }
    }

    public function index()
    {
        $page['title']      = 'Master ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;
        $this->load->view('admin/category/index', compact('page'));
    }

    public function datatables()
    {
        if (!$this->input->is_ajax_request()) {
            show_error('Forbidden access', 403);
        }
        // initiate datatable input
        $data   = [];
        $draw   = $this->input->post('draw', true);
        $start  = $this->input->post('start', true);
        $length = $this->input->post('length', true);
        $order  = $this->input->post('order', true);
        $search = $this->input->post('search', true);

        // get data from table with datatables
        $this->load->model('M_category', 'datatables');
        $list = $this->datatables->get_datatables($start, $length, $order, $search);
        foreach ($list as $key => $value) {
            $row = [];
            array_push($row, $start + $key + 1);
            array_push($row, $value['kategori']);
            $actions = [
                [
                    'href'  => site_url(ADMIN . '/category/' . $value['id'] . '/edit'),
                    'class' => 'btn btn-info' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-pencil"></i> Edit'
                ],
                [
                    'href'  => site_url(ADMIN . '/category/' . $value['id'] . '/delete'),
                    'class' => 'btn btn-danger' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-trash"></i> Hapus'
                ]
            ];
            array_push($row, $this->load->view('layouts/admin/action', compact('actions'), true));
            array_push($data, $row);
        }
        $response = [
            'draw'            => $draw,
            'recordsTotal'    => $this->datatables->count_all(),
            'recordsFiltered' => $this->datatables->count_filtered($order, $search),
            'data'            => $data
        ];
        // output to json format
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create()
    {
        $page['title']      = 'Tambah ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        if (USEAJAX) {
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/category/form-ajax', compact('page'), true)
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->load->view('admin/category/form-default', compact('page'));
        }
    }

    public function store()
    {
        $page['title'] = 'Tambah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('kategori', 'category', 'required|min_length[4]|is_unique[z_kategori.kategori]', [
            // custom error message
            'is_unique' => 'Please find another name'
        ]);
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'kategori' => [
                        'value' => set_value('kategori'),
                        'error' => form_error('kategori')
                    ]
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/category/form-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'kategori'        => [
                            'value' => set_value('kategori'),
                            'error' => form_error('kategori')
                        ]
                    ]
                ]);
                redirect(site_url(ADMIN . '/category/create'));
            }
        // else if form validation is success
        } else {
            $collection = [ // collect data (true for avoid xss injection)
                'id'       => (int) $this->general->lastId('z_kategori') + 1,
                'kategori' => $this->input->post('kategori', true)
            ];
            // insert data into table and check if insert process is success or not
            // if insert process is success
            if ($this->general->insertId('z_kategori', $collection)) {
                $message = ucwords($this->readable) . ' <strong>' . $collection['kategori'] . '</strong> berhasil ditambahkan.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status' => true,
                        'notification' => [
                            'type'    => 'success',
                            'message' => $message
                        ]
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => $message
                    ]);
                    redirect(site_url(ADMIN . '/category'));
                }
            // else if insert process is error
            } else {
                $message = 'Maaf, ' . $this->readable . ' gagal ditambahkan. Silahkan coba lagi.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status' => false,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message
                        ]
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message,
                        'form_data' => [
                            'kategori'  => [
                                'value' => $collection['kategori']
                            ]
                        ]
                    ]);
                    redirect(site_url(ADMIN . '/category/create'));
                }
            }
        }
    }

    public function show($id)
    {
        $this->edit($id);
    }

    public function edit($id)
    {
        $page['title']      = 'Ubah ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_kategori', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $category = $result->row_array();
            $data = [
                'id' => [
                    'value' => $category['id']
                ],
                'kategori' => [
                    'value' => $category['kategori']
                ]
            ];
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/category/form-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/category/form-default', compact('page', 'data'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/category'));
        }
    }

    public function update($id)
    {
        $page['title'] = 'Ubah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('kategori', 'category', 'required|min_length[4]');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'id' => [
                        'value' => $id
                    ],
                    'kategori' => [
                        'value' => set_value('kategori'),
                        'error' => form_error('kategori')
                    ]
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/category/form-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'id' => [
                            'value' => $id
                        ],
                        'kategori' => [
                            'value' => set_value('kategori'),
                            'error' => form_error('kategori')
                        ]
                    ]
                ]);
                redirect(site_url(ADMIN . '/category/' . $id . '/edit'));
            }
        // else if form validation is success
        } else {
            $result = $this->general->edit('z_kategori', [
                'id' => $id
            ]);
            if ($result->num_rows() === 1) {
                $condition = [
                    'id' => $id
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'kategori' => $this->input->post('kategori', true)
                ];
                // udpate data into table and check if update process is success or not
                // if update process is success
                if ($this->general->update('z_kategori', $condition, $collection)) {
                    $message = ucwords($this->readable) . ' <strong>' . $result->row_array()['kategori'] . ' </strong> berhasil diubah menjadi <strong>' . $collection['kategori'] . '</strong>.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message
                        ]);
                        redirect(site_url(ADMIN . '/category'));
                    }
                // else if update process is error
                } else {
                    $message = 'Maaf, ' . $this->readable . ' <strong>' . $result->row_array()['kategori'] . '</strong> gagal diubah. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-danger',
                            'message' => $message,
                            'form_data' => [
                                'kategori'  => [
                                    'value' => $collection['kategori']
                                ]
                            ]
                        ]);
                        redirect(site_url(ADMIN . '/category/' . $id . '/edit'));
                    }
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/category'));
            }
        }
    }

    public function delete($id)
    {
        $page['title']      = 'Hapus ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_kategori', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $category = $result->row_array();
            $message = 'Apakah anda yakin ingin menghapus ' . $this->readable . ' ini : <strong>' . $category['kategori'] . '</strong> ?';
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/category/delete-ajax', compact('page', 'category', 'message'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/category/delete-default', compact('page', 'category', 'message'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/category'));
        }
    }

    public function destroy($id)
    {
        $result = $this->general->edit('z_kategori', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            // delete data into table and check if delete process is success or not
            // if delete process is success
            if ($this->general->delete('z_kategori', ['id' => $id])) {
                $message = ucwords($this->readable) . ' <strong>' . $result->row_array()['kategori'] . '</strong> berhasil dihapus.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status' => true,
                        'notification' => [
                            'type'    => 'success',
                            'message' => $message
                        ]
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => $message
                    ]);
                    redirect(site_url(ADMIN . '/category'));
                }
            // else if insert process is error
            } else {
                $message = 'Maaf, ' . $this->readable . ' <strong>' . $result->row_array()['kategori'] . '</strong> gagal dihapus. Silahkan coba lagi.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status' => false,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message
                        ]
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message
                    ]);
                    redirect(site_url(ADMIN . '/category'));
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/category'));
        }
    }

    private function notFoundData($page, $redirect)
    {
        if (USEAJAX) {
            $message = ucwords($this->readable) . ' tidak ditemukan.';
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/category/error-ajax', compact('page', 'message'), true)
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-warning',
                'message' => ucwords($this->readable) . ' tidak ditemukan.'
            ]);
            redirect($redirect);
        }
    }

    public function dummy()
    {
        $this->load->view('admin/add-product');
    }
}
/* End of file AdminCategory.php */
/* Location: ./application/controllers/AdminCategory.php */
