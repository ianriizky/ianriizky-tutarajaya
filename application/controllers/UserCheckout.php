<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserCheckout extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    private $readable = 'transaksi';

    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // load library cart
        $this->load->library('cart');
        // redirect member if not login
        if (!$this->session->has_userdata('member')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(site_url('login?redirect=' . urlencode(current_url())));
        }
    }

    public function index()
    {
        $page['title']      = 'Pembayaran';
        $page['nav-active'] = 'cart';

        if (!empty($this->cart->contents())) {
            $id = $this->session->userdata('member')['id'];
            $result = $this->general->edit('z_member', ['id' => $id]);
            if ($result->num_rows() === 1) {
                $member = $result->row_array();
                $data = [
                    'member_id' => [
                        'value' => $member['id']
                    ],
                    'nama_penerima' => [
                        'value' => $member['fullname']
                    ],
                    'alamat' => [
                        'value' => $member['alamat']
                    ],
                    'province' => [
                        'value' => $member['province']
                    ],
                    'city' => [
                        'value' => $member['city']
                    ],
                    'no_telp' => [
                        'value' => $member['telp']
                    ]
                ];
                $this->load->view('user/checkout/index', compact('page', 'data'));
            } else {
                $this->session->set_flashdata([
                    'type'    => 'alert-danger',
                    'message' => 'Anda harus login untuk mengakses halaman ini'
                ]);
                redirect(site_url('login?redirect=' . urlencode(current_url())));
            }
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-warning',
                'message' => 'Keranjang belanja anda masih kosong. Silahkan berbelanja terlebih dahulu sebelum menuju ke proses pembayaran.'
            ]);
            redirect(site_url('cart'));
        }
    }

    public function store()
    {
        if (!empty($this->input->post('detail-transaksi', true))) {
            $this->load->library('form_validation');
            // see codeigniter reference rules (required, alpha_dash, etc)
            // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
            $this->form_validation->set_rules('member_id', 'member id', 'required');
            $this->form_validation->set_rules('nama_penerima', 'full name', 'required|max_length[100]');
            $this->form_validation->set_rules('city', 'city', 'max_length[100]');
            $this->form_validation->set_rules('province', 'province', 'max_length[100]');
            $this->form_validation->set_rules('alamat', 'address', 'required|max_length[200]');
            $this->form_validation->set_rules('no_telp', 'telephone number', 'required|numeric|max_length[15]');
            $this->form_validation->set_rules('catatan', 'note', 'max_length[255]');
            $this->form_validation->set_rules('total', 'total', 'required');
            foreach ($this->input->post('detail-transaksi', true) as $key => $value) {
                $this->form_validation->set_rules('detail-transaksi[' . $key . '][id_barang]', 'product id in detail transaction number ' . $key, 'required');
                $this->form_validation->set_rules('detail-transaksi[' . $key . '][harga]', 'price in detail transaction number ' . $key, 'required');
                $this->form_validation->set_rules('detail-transaksi[' . $key . '][jumlah]', 'quantity in detail transaction number ' . $key, 'required');
                $this->form_validation->set_rules('detail-transaksi[' . $key . '][subtotal]', 'subtotal in detail transaction number ' . $key, 'required');
            }
            // if form validation is fail
            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'member_id' => [
                            'value' => set_value('member_id')
                        ],
                        'nama_penerima' => [
                            'value' => set_value('nama_penerima'),
                            'error' => form_error('nama_penerima')
                        ],
                        'city' => [
                            'value' => set_value('city'),
                            'error' => form_error('city')
                        ],
                        'province' => [
                            'value' => set_value('province'),
                            'error' => form_error('province')
                        ],
                        'alamat' => [
                            'value' => set_value('alamat'),
                            'error' => form_error('alamat')
                        ],
                        'no_telp' => [
                            'value' => set_value('no_telp'),
                            'error' => form_error('no_telp')
                        ],
                        'catatan' => [
                            'value' => set_value('catatan'),
                            'error' => form_error('catatan')
                        ],
                        'total' => [
                            'value' => set_value('total')
                        ]
                    ]
                ]);
                redirect(site_url('checkout'));
            // else if form validation is success
            } else {
                // check if member is exist or not
                $result = $this->general->edit('z_member', [
                    'id' => $this->input->post('member_id', true)
                ]);
                if ($result->num_rows() === 1) {
                    $member = $result->row_array();
                    $transactionId  = (int) $this->general->lastId('z_transaksi') + 1;
                    $transactionCode = 'TR' . sprintf('%06s', $transactionId);
                    $transactionEncryptCode = md5($transactionId . '-' . $transactionCode);
                    $this->load->model('M_transaction', 'transaction');
                    $collection = [ // collect data (true for avoid xss injection)
                        'id'                => $transactionId,
                        'kode'              => $transactionCode,
                        'province'          => $this->input->post('province', true),
                        'city'              => $this->input->post('city', true),
                        'alamat'            => $this->input->post('alamat', true),
                        'nama_penerima'     => $this->input->post('nama_penerima', true),
                        'no_telp'           => $this->input->post('no_telp', true),
                        'catatan'           => $this->input->post('catatan', true),
                        'total'             => $this->input->post('total', true),
                        'created_at'        => date('Y-m-d H:i:s'),
                        'member_id'         => $member['id'],
                        'status'            => strval($this->transaction->statusOnProcess), // on process
                        'bukti_transfer'    => $transactionEncryptCode
                    ];
                    // insert data into table and check if insert process is success or not
                    // if insert process is success
                    if ($this->general->insertId('z_transaksi', $collection)) {
                        $insertDetailTransactionStatus = [];
                        $this->load->model('M_product', 'product');
                        foreach ($this->input->post('detail-transaksi', true) as $transactionDetail) {
                            if ($this->general->insertId('z_detail_transaksi', [
                                'id'            => (int) $this->general->lastId('z_detail_transaksi') + 1,
                                'id_transaksi'  => $transactionId,
                                'id_barang'     => $transactionDetail['id_barang'],
                                'harga'         => $transactionDetail['harga'],
                                'jumlah'        => $transactionDetail['jumlah'],
                                'subtotal'      => $transactionDetail['subtotal'],
                            ])) {
                                // get last stock
                                $condition = [
                                    'id_produk' => $transactionDetail['id_barang']
                                ];
                                $collection = [ // collect data (true for avoid xss injection)
                                    'stok' => $this->product->getLastStock($transactionDetail['id_barang']) - $transactionDetail['jumlah']
                                ];
                                // update stock in "z_produk_stok" table
                                // if update process is success
                                if ($this->general->update('z_produk_stok', $condition, $collection)) {
                                    array_push($insertDetailTransactionStatus, true);
                                }
                            } else {
                                array_push($insertDetailTransactionStatus, false);
                            }
                        }
                        // if insert detail transaction is success
                        if (!in_array(false, $insertDetailTransactionStatus)) {
                            $page = [
                                'title'     => 'Konfirmasi Transaksi',
                                'name'      => $this->config->item('site_name'),
                                'link'      => site_url('transaction/' . $transactionEncryptCode . '/upload')
                            ];
                            // load email template
                            $template = $this->load->view('email/transaction-confirmation', compact('page'), true);
                            // send email to member to confirm transaction
                            $this->load->library('email');
                            $this->email->to($member['email']);
                            $this->email->from('no-reply@tutarajaya.lol', $this->config->item('site_name'));
                            $this->email->subject('Konfirmasi Transaksi di ' . $this->config->item('site_name'));
                            $this->email->message($template);
                            if ($this->email->send()) {
                                $this->session->set_flashdata([
                                    'type'    => 'alert-success',
                                    'message' => $message = ucwords($this->readable) . ' dengan kode <strong>' . $transactionCode . '</strong> berhasil diproses. Silahkan cek email anda di <strong>' . $member['email'] . '</strong> untuk proses transaksi selanjutnya.'
                                ]);
                            } else {
                                $this->session->set_flashdata([
                                    'type'    => 'alert-success',
                                    'message' => $message = ucwords($this->readable) . ' dengan kode <strong>' . $transactionCode . '</strong> berhasil diproses. Kami akan segera mengirimkan konfirmasi ke email anda untuk proses transaksi selanjutnya.'
                                        // . $this->email->print_debugger()
                                ]);
                            }
                            // load library cart to destroy cart session
                            $this->load->library('cart');
                            $this->cart->destroy();
                            // redirect to home
                            redirect(site_url());
                        // if insert detail transaction is fail
                        } else {
                            if ($this->general->delete('z_detail_transaksi', ['id_transaksi' => $transactionId])) {
                                $this->general->delete('z_transaksi', ['id' => $transactionId]);
                            }
                            $this->session->set_flashdata([
                                'type'    => 'alert-danger',
                                'message' => 'Maaf, ' . $this->readable . ' dengan kode <strong>' . $transactionCode . '</strong> gagal diproses.'
                            ]);
                            redirect(site_url());
                        }
                    }
                } else {
                    $this->session->sess_destroy();
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Maaf, akun anda tidak ditemukan.'
                    ]);
                    redirect(site_url());
                }
            }
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-warning',
                'message' => 'Keranjang belanja anda masih kosong. Silahkan berbelanja terlebih dahulu sebelum menuju ke proses pembayaran.'
            ]);
            redirect(site_url('checkout'));
        }
    }
}
/* End of file UserCheckout.php */
/* Location: ./application/controllers/UserCheckout.php */
