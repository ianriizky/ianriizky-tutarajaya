<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminTransaction extends CI_Controller
{

	/**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    private $readable = 'transaksi';
    private $navActive = 'transaction';

    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect admin if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(ADMIN . '/login?redirect=' . urlencode(current_url()));
        }
        // load "M_transaction" model
        $this->load->model('M_transaction', 'transaction');
    }

    public function index()
    {
        $page['title']      = 'Daftar ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;
        $this->load->view('admin/transaction/index', compact('page'));
    }

    public function datatables()
    {
        if (!$this->input->is_ajax_request()) {
            show_error('Forbidden access', 403);
        }
        // initiate datatable input
        $data   = [];
        $draw   = $this->input->post('draw', true);
        $start  = $this->input->post('start', true);
        $length = $this->input->post('length', true);
        $order  = $this->input->post('order', true);
        $search = $this->input->post('search', true);

        // get data from table with datatables
        $list = $this->transaction->get_datatables($start, $length, $order, $search);
        foreach ($list as $key => $value) {
            $row = [];
            array_push($row, $start + $key + 1);
            array_push($row, $value['kode']);
            array_push($row, $value['nama_penerima']);
            array_push($row, date('j F Y H.i.s', strtotime($value['created_at'])));
            array_push($row, format_rupiah($value['total']));
            array_push($row, '<span class="btn ' . $this->transaction->statusColor[(int) $value['status']] . ' btn-block" style="cursor: default;">' . $this->transaction->statusText[(int) $value['status']] . '</span>');
            $actions = [
                [
                    'href'  => site_url(ADMIN . '/transaction/' . $value['id']),
                    'class' => 'btn btn-success'/* . (USEAJAX ? ' action-ajax' : '')*/,
                    'title' => '<i class="fa fa-bookmark"></i> Lihat'
                ],
                [
                    'href'  => site_url(ADMIN . '/transaction/' . $value['id'] . '/delete'),
                    'class' => 'btn btn-danger'/* . (USEAJAX ? ' action-ajax' : '')*/,
                    'title' => '<i class="fa fa-trash"></i> Hapus'
                ]
            ];
            array_push($row, $this->load->view('layouts/admin/action', compact('actions'), true));
            array_push($data, $row);
        }
        $response = [
            'draw'            => $draw,
            'recordsTotal'    => $this->transaction->count_all(),
            'recordsFiltered' => $this->transaction->count_filtered($order, $search),
            'data'            => $data
        ];
        // output to json format
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function show($id)
    {
        $page['title']      = 'Lihat ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_transaksi', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $transaction = $result->row_array();
            $page['title'] = 'Lihat ' . ucwords($this->readable) . ' - ' . $transaction['kode'];
            // get member data
            $resultMember = $this->general->edit('z_member', [
                'id' => $transaction['member_id']
            ]);
            if ($resultMember->num_rows() === 1) {
                $member = $resultMember->row_array();
                // get detail transaction data
                $resultsTransactionDetail = $this->transaction->getDetailWithProduct([
                    'z_detail_transaksi.id_transaksi' => $id
                ]);
                if ($resultsTransactionDetail->num_rows() > 0) {
                    $transactionDetails = $resultsTransactionDetail->result_array();
                    $listStatus = [];
                    foreach ($this->transaction->statusCode as $code) {
                        array_push($listStatus, [
                            'code' => $code,
                            'text' => $this->transaction->statusText[$code]
                        ]);
                    }
                    if (false) { // if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'html' => $this->load->view('admin/transaction/show-ajax', compact('page', 'transaction', 'member', 'transactionDetails', 'listStatus'), true),
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->load->view('admin/transaction/show-default', compact('page', 'transaction', 'member', 'transactionDetails', 'listStatus'));
                    }
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/transaction'));
        }
    }

    public function edit($id)
    {
        
    }

    public function update($id)
    {
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('status', 'status', 'required');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (false) { // if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/transaction/show-ajax', compact('page'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([
                    'type'    => 'alert-danger',
                    'message' => validation_errors()
                ]);
                redirect(site_url(ADMIN . '/transaction/' . $id));
            }
            // else if form validation is success
        } else {
            $result = $this->general->edit('z_transaksi', [
                'id' => $id,
            ]);
            if ($result->num_rows() === 1) {
                $condition = [
                    'id' => $id
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'status' => $this->input->post('status', true)
                ];
                // udpate data into table and check if update process is success or not
                // if update process is success
                if ($this->general->update('z_transaksi', $condition, $collection)) {
                    $transaction = $result->row_array();
                    $message = ucwords($this->readable) . ' ' . $transaction['kode'] . '</strong> berhasil diubah menjadi <strong>' . $this->transaction->statusText[$collection['status']] . '</strong>.';
                    $resultMember = $this->general->edit('z_member', [
                        'id' => $transaction['member_id']
                    ]);
                    if ($resultMember->num_rows() === 1) {
                        $member = $resultMember->row_array();
                        // send email progress to member
                        $page = [
                            'title'     => 'Perubahan Status Transaksi',
                            'name'      => $this->config->item('site_name'),
                            'link'      => site_url('transaction/' . $condition['id']),
                            'before'    => $this->transaction->statusText[$result->row_array()['status']],
                            'after'     => $this->transaction->statusText[$collection['status']]
                        ];
                        // load email template
                        $template = $this->load->view('email/transaction-status', compact('page'), true);
                        // send email to member to confirm transaction
                        $this->load->library('email');
                        $this->email->to($member['email']);
                        $this->email->from('no-reply@tutarajaya.lol', $this->config->item('site_name'));
                        $this->email->subject('Update Status Transaksi di ' . $this->config->item('site_name'));
                        $this->email->message($template);
                        $this->email->send();
                    }
                    // check if request are come from ajax or default
                    if (false) { // if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message,
                        ]);
                        redirect(site_url(ADMIN . '/transaction/' . $condition['id']));
                    }
                    // else if update process is error
                } else {
                    $message = 'Maaf, ' . $this->readable . ' gagal diubah. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (false) { // if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'      => 'alert-danger',
                            'message'   => $message,
                            'form_data' => [
                                'foto' => [
                                    'value' => set_value('foto')
                                ],
                                'status' => [
                                    'value' => set_value('status')
                                ]
                            ],
                        ]);
                        redirect(site_url(ADMIN . '/transaction/' . $id . '/edit'));
                    }
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/transaction'));
            }
        }
    }

    public function delete($id)
    {
        $page['title']      = 'Hapus ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_transaksi', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $transaction = $result->row_array();
            $message = 'Apakah anda yakin ingin menghapus ' . $this->readable . ' ini : <strong>' . $transaction['kode'] . '</strong> ?';
            if (false) { // if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/transaction/delete-ajax', compact('page', 'transaction', 'message'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/transaction/delete-default', compact('page', 'transaction', 'message'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/transaction'));
        }
    }

    public function destroy($id)
    {
        $result = $this->general->edit('z_transaksi', [
            'id' => $id,
        ]);
        if ($result->num_rows() === 1) {
            // return product stock from detail transaction data
            $resultsTransactionDetail = $this->transaction->getDetailWithProduct([
                'z_detail_transaksi.id_transaksi' => $id
            ]);
            if ($resultsTransactionDetail->num_rows() > 0) {
                $transactionDetails = $resultsTransactionDetail->result_array();
                $updateProductStockStatus = [];
                $this->load->model('M_product', 'product');
                foreach ($transactionDetails as $key => $transactionDetail) {
                    // get last stock
                    $condition = [
                        'id_produk' => $transactionDetail['dt_idbarang']
                    ];
                    $collection = [ // collect data (true for avoid xss injection)
                        'stok' => $this->product->getLastStock($transactionDetail['dt_idbarang']) + $transactionDetail['dt_jumlah']
                    ];
                    // update stock in "z_produk_stok" table
                    // if update process is success
                    if ($this->general->update('z_produk_stok', $condition, $collection)) {
                        array_push($updateProductStockStatus, true);
                    } else {
                        array_push($updateProductStockStatus, false);
                    }
                }
                if (!in_array(false, $updateProductStockStatus)) {
                    // delete data into table and check if delete process is success or not
                    // if delete process is success
                    if ($this->general->delete('z_detail_transaksi', ['id_transaksi' => $id])) {
                        if ($this->general->delete('z_transaksi', ['id' => $id])) {
                            @unlink('./' . $result->row_array()['bukti_transfer']);
                            $this->session->set_flashdata([
                                'type'    => 'alert-success',
                                'message' => ucwords($this->readable) . ' berhasil dihapus.',
                            ]);
                            redirect(site_url(ADMIN . '/transaction'));
                        // else if insert process is error
                        } else {
                            $this->session->set_flashdata([
                                'type'    => 'alert-danger',
                                'message' => 'Maaf, ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.',
                            ]);
                            redirect(site_url(ADMIN . '/transaction'));
                        }
                    }
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Maaf, ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.',
                    ]);
                    redirect(site_url(ADMIN . '/transaction'));
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/transaction'));
        }
    }

    // destroy bukti_transfer
    public function uploadDestroy($id)
    {
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('transaction-encrypt-code', '', 'required');
        // if form_validation is fail (username are not filled)
        if ($this->form_validation->run() == false) {
            $this->notFoundData();
        } else {
            $result = $this->general->edit('z_transaksi', [
                'id' => $id,
            ]);
            if ($result->num_rows() === 1) {
                $transaction = $result->row_array();
                $condition = [
                    'id' => $transaction['id']
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'bukti_transfer' => $this->input->post('transaction-encrypt-code')
                ];
                // update stock in "z_produk_stok" table
                // if update process is success
                if ($this->general->update('z_transaksi', $condition, $collection)) {
                    @unlink('./' . explode('|', $transaction['bukti_transfer'])[0]);
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => 'Bukti ' . $this->readable . ' berhasil dihapus.',
                    ]);
                    redirect(site_url(ADMIN . '/transaction/' . $transaction['id']));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Maaf, bukti ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.',
                    ]);
                    redirect(site_url(ADMIN . '/transaction/' . $transaction['id']));
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/transaction'));
            }
        }
    }

    private function notFoundData($page, $redirect)
    {
        if (false) { // if (USEAJAX) {
            $message = ucwords($this->readable) . ' tidak ditemukan.';
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/transaction/error-ajax', compact('page', 'message'), true),
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-warning',
                'message' => ucwords($this->readable) . ' tidak ditemukan.',
            ]);
            redirect($redirect != null ? $redirect : site_url(ADMIN . '/transaction'));
        }
    }
}
/* End of file AdminTransaction.php */
/* Location: ./application/controllers/AdminTransaction.php */
