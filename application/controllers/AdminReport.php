<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminReport extends CI_Controller
{

	/**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    private $readable = 'laporan';
    private $navActive = 'report';

    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect admin if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(ADMIN . '/login?redirect=' . urlencode(current_url()));
        }
        // load "M_transaction" model
        $this->load->model('M_transaction', 'transaction');
    }

    public function index()
    {
        $page['title']      = ucwords($this->readable) . ' transaksi';
        $page['nav-active'] = $this->navActive;

        $listStatus = [];
        foreach ($this->transaction->statusCode as $code) {
            array_push($listStatus, [
                'code' => $code,
                'text' => $this->transaction->statusText[$code]
            ]);
        }
        $this->load->view('admin/report/index', compact('page', 'listStatus'));
    }

    public function datatables()
    {
        if (!$this->input->is_ajax_request()) {
            show_error('Forbidden access', 403);
        }
        // initiate datatable input
        $data   = [];
        $draw   = $this->input->post('draw', true);
        $start  = $this->input->post('start', true);
        $length = $this->input->post('length', true);
        $order  = $this->input->post('order', true);
        $search = $this->input->post('search', true);
        // addition input
        $this->load->model('M_report', 'report');
        $this->report->status     = $this->input->post('status', true);
        $this->report->startDate = $this->input->post('start_date', true);
        $this->report->endDate   = $this->input->post('end_date', true);

        // get data from table with datatables
        $list = $this->report->get_datatables($start, $length, $order, $search);
        foreach ($list as $key => $value) {
            $row = [];
            array_push($row, $start + $key + 1);
            array_push($row, $value['kode']);
            array_push($row, $value['nama_penerima']);
            array_push($row, date('j F Y H.i.s', strtotime($value['created_at'])));
            array_push($row, format_rupiah($value['total']));
            array_push($row, '<span class="btn ' . $this->transaction->statusColor[(int) $value['status']] . ' btn-block" style="cursor: default;">' . $this->transaction->statusText[(int) $value['status']] . '</span>');
            $actions = [
                [
                    'href'  => site_url(ADMIN . '/report/' . $value['id']),
                    'class' => 'btn btn-success' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-bookmark"></i> Lihat'
                ]
            ];
            array_push($row, $this->load->view('layouts/admin/action', compact('actions'), true));
            array_push($data, $row);
        }
        $response = [
            'draw'            => $draw,
            'recordsTotal'    => $this->report->count_all(),
            'recordsFiltered' => $this->report->count_filtered($order, $search),
            'data'            => $data
        ];
        // output to json format
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function show($id)
    {
        $page['title']      = 'Lihat ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_transaksi', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $transaction = $result->row_array();
            $page['title'] = 'Lihat ' . ucwords($this->readable) . ' - ' . $transaction['kode'];
            // get member data
            $resultMember = $this->general->edit('z_member', [
                'id' => $transaction['member_id']
            ]);
            if ($resultMember->num_rows() === 1) {
                $member = $resultMember->row_array();
                // get detail transaction data
                $resultsTransactionDetail = $this->transaction->getDetailWithProduct([
                    'z_detail_transaksi.id_transaksi' => $id
                ]);
                if ($resultsTransactionDetail->num_rows() > 0) {
                    $transactionDetails = $resultsTransactionDetail->result_array();
                    if (false) { // if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'html' => $this->load->view('admin/report/show-ajax', compact('page', 'transaction', 'member', 'transactionDetails'), true),
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->load->view('admin/report/show-default', compact('page', 'transaction', 'member', 'transactionDetails'));
                    }
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/transaction'));
        }
    }

    private function notFoundData($page, $redirect)
    {
        if (false) { // if (USEAJAX) {
            $message = ucwords($this->readable) . ' tidak ditemukan.';
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/report/error-ajax', compact('page', 'message'), true),
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-warning',
                'message' => ucwords($this->readable) . ' tidak ditemukan.',
            ]);
            redirect($redirect != null ? $redirect : site_url(ADMIN . '/report'));
        }
    }
}
/* End of file AdminReport.php */
/* Location: ./application/controllers/AdminReport.php */
