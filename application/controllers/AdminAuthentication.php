<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminAuthentication extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
    }

    public function loginGet()
    {
        if ($this->session->has_userdata('user')) {
            redirect(site_url(ADMIN));
        }

        $page['title'] = 'Admin';
        $this->load->view('admin/authentication/login', compact('page'));
    }

    public function loginPost()
    {
        // if user has been login, can't login anymore (redirect to site_url())
        if ($this->session->has_userdata('user')) {
            redirect(site_url(ADMIN));
        }

        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        // if form_validation is fail (username/email are not filled)
        if ($this->form_validation->run() == false) {
            // show error form validation is session/flashdata
            $this->session->set_flashdata([
                'form_data' => [
                    'email'    => [
                        'value' => set_value('email'),
                        'error' => form_error('email')
                    ],
                    'password' => [
                        'value' => set_value('password'),
                        'error' => form_error('password')
                    ]
                ]
            ]);
            redirect(site_url(ADMIN . '/login' . (!empty($this->input->post('redirect')) ? '?redirect=' . urlencode($this->input->post('redirect')) : '')));
        // else if form validation is success
        } else {
            $usernameEmail = $this->input->post('email', true); // checking user data from email input
            $result        = $this->general->verify('z_user', '' // check if user data is exist on database or not
                // where query
                . 'username = \'' . $usernameEmail . '\''
                . ' OR email = \'' . $usernameEmail . '\''
            );
            // if user data is exist on database
            if ($result->num_rows() === 1) {
                $this->load->library('password');
                $user     = $result->row_array();
                $password = $this->input->post('password', true);
                // if user's password is correct
                if ($this->password->check($password, $user['password'])) {
                    $this->session->set_userdata('user', $user); // set session of member
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => 'Selamat datang <strong>' . $user['username'] . '</strong>! Anda telah berhasil login.'
                    ]);
                    if (!empty($this->input->post('redirect'))) {
                        redirect(urldecode($this->input->post('redirect')));
                    } else {
                        redirect(site_url(ADMIN));
                    }
                // else if user's password is wrong
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Maaf, password yang anda masukkan salah.',
                        'form_data' => [
                            'email'    => [
                                'value' => $this->input->post('email', true)
                            ]
                        ]
                    ]);
                    redirect(site_url(ADMIN . '/login' . (!empty($this->input->post('redirect')) ? '?redirect=' . urlencode($this->input->post('redirect')) : '')));
                }
            // else if user data is not exist on database
            } else {
                $this->session->set_flashdata([
                    'type'    => 'alert-danger',
                    'message' => 'Maaf, email dan password yang anda masukkan tidak ditemukan.'
                ]);
                redirect(site_url(ADMIN . '/login' . (!empty($this->input->post('redirect')) ? '?redirect=' . urlencode($this->input->post('redirect')) : '')));
            }
        }
    }

    public function logoutPost()
    {
        $this->session->sess_destroy();
        redirect(site_url(ADMIN . '/logout/success'));
    }

    public function logoutSuccessGet()
    {
        // if user has been login, can't see logout success (redirect to site_url())
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-success',
                'message' => 'Anda telah berhasil logout.',
            ]);
        }
        redirect(site_url(ADMIN . '/login'));
    }

}
/* End of file AdminAuthentication.php */
/* Location: ./application/controllers/AdminAuthentication.php */