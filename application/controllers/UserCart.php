<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserCart extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // load library cart
        $this->load->library('cart');
    }

    public function index()
    {
        // redirect member if not login
        if (!$this->session->has_userdata('member')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(site_url('login?redirect=' . urlencode(current_url())));
        }

        $page['title']      = 'Keranjang';
        $page['nav-active'] = 'cart';

        $this->load->view('user/cart/index', compact('page'));
    }

    public function store()
    {
        // redirect member if not login
        if (!$this->session->has_userdata('member')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(site_url('login?redirect=' . urlencode($this->input->post('redirect'))));
        }

        // load library cart
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('id', 'id', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('discount', 'discount', 'numeric');
        $this->form_validation->set_rules('stock', 'stock', 'required');
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('qty', 'quantity', 'required');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Maaf, proses penambahan produk ke keranjang gagal dilakukan. Silahkan coba lagi.',
            ]);
            redirect(current_url());
        } else {
            $this->cart->insert([
                'id'       => $this->input->post('id'),
                'qty'      => $this->input->post('qty'),
                'price'    => $this->input->post('price'),
                'discount' => !empty($this->input->post('discount')) ? $this->input->post('discount') : 0,
                'stock'    => $this->input->post('stock'),
                'name'     => $this->input->post('name'),
                'picture'  => !empty($this->input->post('picture')) ? $this->input->post('picture') : null
            ]);
            $this->session->set_flashdata([
                'type'    => 'alert-success',
                'message' => 'Proses penambahan produk ke keranjang berhasil dilakukan.',
            ]);
            redirect(site_url('cart'));
        }
    }

    public function update($rowId)
    {
        // redirect member if not login
        if (!$this->session->has_userdata('member')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(site_url('login?redirect=' . urlencode(current_url())));
        }

        // load library cart
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('id', 'product id', 'required');
        $this->form_validation->set_rules('qty', 'quantity', 'required');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Maaf, proses perubahan keranjang gagal dilakukan. Silahkan coba lagi.',
            ]);
            redirect(current_url());
        } else {
            $this->cart->update([
                'rowid'   => $rowId,
                'qty'     => $this->input->post('qty')
            ]);
            $this->session->set_flashdata([
                'type'    => 'alert-success',
                'message' => 'Proses perubahan keranjang berhasil dilakukan.',
            ]);
            redirect(site_url('cart'));
        }
    }

}
/* End of file UserCart.php */
/* Location: ./application/controllers/UserCart.php */
