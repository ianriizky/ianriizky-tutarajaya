<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Error_custom extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
    }

    public function show_404() {
        
        // Setting response header as 404
        $this->output->set_status_header('404');
        
        // Load the error view page form views folder (applications/views/errors_custom/error_404.php)
        $heading = '404 Page Not Found';
        $message = 'The page you requested was not found.';
        $this->load->view('errors/html/error_404_custom', compact('heading', 'message'));
    }

}
/* End of file Error_custom.php */
/* Location: ./application/controllers/Error_custom.php */