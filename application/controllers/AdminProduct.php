<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminProduct extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    private $readable = 'produk';
    private $navActive = 'master';

    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect admin if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(ADMIN . '/login?redirect=' . urlencode(current_url()));
        }
        // load "z_produk" model
        $this->load->model('M_product', 'product');
    }

    public function index()
    {
        $page['title']      = 'Master ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;
        $this->load->view('admin/product/index', compact('page'));
    }

    public function datatables()
    {
        if (!$this->input->is_ajax_request()) {
            show_error('Forbidden access', 403);
        }
        // initiate datatable input
        $data   = [];
        $draw   = $this->input->post('draw', true);
        $start  = $this->input->post('start', true);
        $length = $this->input->post('length', true);
        $order  = $this->input->post('order', true);
        $search = $this->input->post('search', true);

        // get data from table with datatables
        $list = $this->product->get_datatables($start, $length, $order, $search);
        foreach ($list as $key => $value) {
            $row = [];
            array_push($row, $start + $key + 1);
            array_push($row, $value['p_kode']);
            array_push($row, $value['p_nama']);
            array_push($row, $value['k_kategori']);
            $actions = [
                [
                    'href'  => site_url(ADMIN . '/product/' . $value['p_id'] . '/edit'),
                    'class' => 'btn btn-info' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-pencil"></i> Edit',
                ],
                [
                    'href'  => site_url(ADMIN . '/product/' . $value['p_id'] . '/picture'),
                    'class' => 'btn btn-primary',
                    'title' => '<i class="fa fa-file-image-o"></i> Gambar',
                ],
                [
                    'href'  => site_url(ADMIN . '/product/' . $value['p_id'] . '/delete'),
                    'class' => 'btn btn-danger' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-trash"></i> Hapus',
                ],
            ];
            array_push($row, $this->load->view('layouts/admin/action', compact('actions'), true));
            array_push($data, $row);
        }
        $response = [
            'draw'            => $draw,
            'recordsTotal'    => $this->product->count_all(),
            'recordsFiltered' => $this->product->count_filtered($order, $search),
            'data'            => $data,
        ];
        // output to json format
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create()
    {
        $page['title']      = 'Tambah ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        // generate product code
        $productId = (int) $this->general->lastId('z_produk') + 1;
        $preparation = [
             'productCode' => 'PR' . sprintf('%04s', $productId),
             'kategori'    => $this->general->getData('z_kategori')->result_array()
        ];
        if (USEAJAX) {
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/product/form-ajax', compact('page', 'preparation'), true)
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->load->view('admin/product/form-default', compact('page', 'preparation'));
        }
    }

    public function store()
    {
        $page['title'] = 'Tambah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('kode', 'code', 'required|is_unique[z_produk.kode]', [
            // custom error message
            'is_unique' => 'The code is already exist.',
        ]);
        $this->form_validation->set_rules('nama', 'name', 'required|min_length[3]|max_length[100]');
        $this->form_validation->set_rules('deskripsi', 'description', 'min_length[3]|max_length[500]');
        $this->form_validation->set_rules('kategori', 'category', 'required');
        $this->form_validation->set_rules('harga', 'price', 'required');
        $this->form_validation->set_rules('diskon', 'discount', 'required|numeric');
        $this->form_validation->set_rules('status', 'status', 'required');
        $this->form_validation->set_rules('popular', 'popular', 'required');
        $this->form_validation->set_rules('stok', 'stock', 'required|numeric'); // table "z_produk_stok"
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'kode' => [
                        'value' => set_value('kode'),
                        'error' => form_error('kode')
                    ],
                    'nama' => [
                        'value' => set_value('nama'),
                        'error' => form_error('nama')
                    ],
                    'deskripsi' => [
                        'value' => set_value('deskripsi'),
                        'error' => form_error('deskripsi')
                    ],
                    'kategori' => [
                        'value' => set_value('kategori'),
                        'error' => form_error('kategori')
                    ],
                    'harga' => [
                        'value' => set_value('harga'),
                        'error' => form_error('harga')
                    ],
                    'diskon' => [
                        'value' => set_value('diskon'),
                        'error' => form_error('diskon')
                    ],
                    'status' => [
                        'value' => set_value('status'),
                        'error' => form_error('status')
                    ],
                    'popular' => [
                        'value' => set_value('popular'),
                        'error' => form_error('popular')
                    ],
                    'stok' => [
                        'value' => set_value('stok'),
                        'error' => form_error('stok')
                    ]
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/product/form-ajax', compact('page', 'data'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'kode' => [
                            'value' => set_value('kode'),
                            'error' => form_error('kode')
                        ],
                        'nama' => [
                            'value' => set_value('nama'),
                            'error' => form_error('nama')
                        ],
                        'deskripsi' => [
                            'value' => set_value('deskripsi'),
                            'error' => form_error('deskripsi')
                        ],
                        'kategori' => [
                            'value' => set_value('kategori'),
                            'error' => form_error('kategori')
                        ],
                        'harga' => [
                            'value' => set_value('harga'),
                            'error' => form_error('harga')
                        ],
                        'diskon' => [
                            'value' => set_value('diskon'),
                            'error' => form_error('diskon')
                        ],
                        'status' => [
                            'value' => set_value('status'),
                            'error' => form_error('status')
                        ],
                        'popular' => [
                            'value' => set_value('popular'),
                            'error' => form_error('popular')
                        ],
                        'stok' => [
                            'value' => set_value('stok'),
                            'error' => form_error('stok')
                        ]
                    ],
                ]);
                redirect(site_url(ADMIN . '/product/create'));
            }
        // else if form validation is success
        } else {
            $productId = (int) $this->general->lastId('z_produk') + 1;
            $collection = [ // collect data (true for avoid xss injection)
                'id'            => $productId,
                'kode'          => $this->input->post('kode', true),
                'nama'          => $this->input->post('nama', true),
                'deskripsi'     => $this->input->post('deskripsi', true),
                'kategori'      => $this->input->post('kategori', true),
                'harga'         => $this->input->post('harga', true),
                'diskon'        => $this->input->post('diskon', true),
                'status'        => $this->input->post('status', true),
                'popular'       => $this->input->post('popular', true)
            ];
            // insert data into table and check if insert process is success or not
            // if insert process is success
            if ($this->general->insertId('z_produk', $collection)) {
                if ($this->general->insertId('z_produk_stok', [
                    'id'        => (int) $this->general->lastId('z_produk_stok') + 1,
                    'id_produk' => $productId,
                    'stok'      => $this->input->post('stok', true)
                ])) {
                    $message = ucwords($this->readable) . ' berhasil ditambahkan.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message,
                        ]);
                        redirect(site_url(ADMIN . '/product'));
                    }
                }
            // else if insert process is error
            } else {
                // delete fail picture
                @unlink('./' . $collection['gambar']);
                $message = 'Maaf, ' . $this->readable . ' gagal ditambahkan. Silahkan coba lagi.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status'       => false,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message,
                        ],
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'      => 'alert-danger',
                        'message'   => $message,
                        'form_data' => [
                            'kode' => [
                                'value' => $collection['kode']
                            ],
                            'nama' => [
                                'value' => $collection['nama']
                            ],
                            'deskripsi' => [
                                'value' => $collection['deskripsi']
                            ],
                            'kategori' => [
                                'value' => $collection['kategori']
                            ],
                            'harga' => [
                                'value' => $collection['harga']
                            ],
                            'diskon' => [
                                'value' => $collection['diskon']
                            ],
                            'status' => [
                                'value' => $collection['status']
                            ],
                            'popular' => [
                                'value' => $collection['popular']
                            ],
                            'stok' => [
                                'value' => $collection['stok']
                            ]
                        ],
                    ]);
                    redirect(site_url(ADMIN . '/product/create'));
                }
            }
        }
    }

    public function show($id)
    {
        $this->edit($id);
    }

    public function edit($id)
    {
        $page['title']      = 'Ubah ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->product->get([
            'z_produk.id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $product = $result->row_array();
            $preparation = [
                 'kategori'    => $this->general->getData('z_kategori')->result_array()
            ];
            $data   = [
                'id'       => [
                    'value' => $product['p_id']
                ],
                'kode' => [
                    'value' => $product['p_kode']
                ],
                'nama' => [
                    'value' => $product['p_nama']
                ],
                'deskripsi' => [
                    'value' => $product['p_deskripsi']
                ],
                'kategori' => [
                    'value' => $product['p_kategori']
                ],
                'harga' => [
                    'value' => $product['p_harga']
                ],
                'diskon' => [
                    'value' => $product['p_diskon']
                ],
                'status' => [
                    'value' => $product['p_status']
                ],
                'popular' => [
                    'value' => $product['p_popular']
                ],
                'stok' => [
                    'value' => $product['ps_stok']
                ]
            ];
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/product/form-ajax', compact('page', 'data', 'preparation'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/product/form-default', compact('page', 'data', 'preparation'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/product'));
        }
    }

    public function update($id)
    {
        $page['title'] = 'Ubah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('kode', 'code', 'required|edit_unique[z_produk.kode.id.' .  $id. ']', [
            // custom error message
            'edit_unique' => 'The code is already exist.',
        ]);
        $this->form_validation->set_rules('nama', 'name', 'required|min_length[3]|max_length[100]');
        $this->form_validation->set_rules('deskripsi', 'description', 'min_length[3]|max_length[500]');
        $this->form_validation->set_rules('kategori', 'category', 'required');
        $this->form_validation->set_rules('harga', 'price', 'required');
        $this->form_validation->set_rules('diskon', 'discount', 'required|numeric');
        $this->form_validation->set_rules('status', 'status', 'required');
        $this->form_validation->set_rules('popular', 'popular', 'required');
        $this->form_validation->set_rules('stok', 'stock', 'required|numeric'); // table "z_produk_stok"
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'id'       => [
                        'value' => $id,
                    ],
                    'kode' => [
                        'value' => set_value('kode'),
                        'error' => form_error('kode')
                    ],
                    'nama' => [
                        'value' => set_value('nama'),
                        'error' => form_error('nama')
                    ],
                    'deskripsi' => [
                        'value' => set_value('deskripsi'),
                        'error' => form_error('deskripsi')
                    ],
                    'kategori' => [
                        'value' => set_value('kategori'),
                        'error' => form_error('kategori')
                    ],
                    'harga' => [
                        'value' => set_value('harga'),
                        'error' => form_error('harga')
                    ],
                    'diskon' => [
                        'value' => set_value('diskon'),
                        'error' => form_error('diskon')
                    ],
                    'status' => [
                        'value' => set_value('status'),
                        'error' => form_error('status')
                    ],
                    'popular' => [
                        'value' => set_value('popular'),
                        'error' => form_error('popular')
                    ],
                    'stok' => [
                        'value' => set_value('stok'),
                        'error' => form_error('stok')
                    ]
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/product/form-ajax', compact('page', 'data'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'id'       => [
                            'value' => $id,
                        ],
                        'kode' => [
                            'value' => set_value('kode'),
                            'error' => form_error('kode')
                        ],
                        'nama' => [
                            'value' => set_value('nama'),
                            'error' => form_error('nama')
                        ],
                        'deskripsi' => [
                            'value' => set_value('deskripsi'),
                            'error' => form_error('deskripsi')
                        ],
                        'kategori' => [
                            'value' => set_value('kategori'),
                            'error' => form_error('kategori')
                        ],
                        'harga' => [
                            'value' => set_value('harga'),
                            'error' => form_error('harga')
                        ],
                        'diskon' => [
                            'value' => set_value('diskon'),
                            'error' => form_error('diskon')
                        ],
                        'status' => [
                            'value' => set_value('status'),
                            'error' => form_error('status')
                        ],
                        'popular' => [
                            'value' => set_value('popular'),
                            'error' => form_error('popular')
                        ],
                        'stok' => [
                            'value' => set_value('stok'),
                            'error' => form_error('stok')
                        ]
                    ],
                ]);
                redirect(site_url(ADMIN . '/product/' . $id . '/edit'));
            }
            // else if form validation is success
        } else {
            $result = $this->general->edit('z_produk', [
                'id' => $id,
            ]);
            if ($result->num_rows() === 1) {
                $condition = [
                    'id' => $id
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'kode'          => $this->input->post('kode', true),
                    'nama'          => $this->input->post('nama', true),
                    'deskripsi'     => $this->input->post('deskripsi', true),
                    'kategori'      => $this->input->post('kategori', true),
                    'harga'         => $this->input->post('harga', true),
                    'diskon'        => $this->input->post('diskon', true),
                    'status'        => $this->input->post('status', true),
                    'popular'       => $this->input->post('popular', true)
                ];
                // udpate data into table and check if update process is success or not
                // if update process is success
                if ($this->general->update('z_produk', $condition, $collection)) {
                    if ($this->general->update('z_produk_stok', ['id_produk' => $id], [
                        'stok'      => $this->input->post('stok', true)
                    ])) {
                        $message = ucwords($this->readable) . ' <strong>' . $collection['nama'] . '</strong> berhasil diubah.';
                        // check if request are come from ajax or default
                        if (USEAJAX) {
                            if (!$this->input->is_ajax_request()) {
                                show_error('Forbidden access', 403);
                            }
                            $response = [
                                'status'       => true,
                                'notification' => [
                                    'type'    => 'success',
                                    'message' => $message,
                                ],
                            ];
                            $this->output->set_content_type('application/json')->set_output(json_encode($response));
                        } else {
                            $this->session->set_flashdata([
                                'type'    => 'alert-success',
                                'message' => $message,
                            ]);
                            redirect(site_url(ADMIN . '/product'));
                        }
                    }
                // else if update process is error
                } else {
                    $message = 'Maaf, ' . $this->readable . ' ' . $collection['nama'] . ' gagal diubah. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'      => 'alert-danger',
                            'message'   => $message,
                            'form_data' => [
                                'id'       => [
                                    'value' => $id,
                                ],
                                'kode' => [
                                    'value' => set_value('kode')
                                ],
                                'nama' => [
                                    'value' => set_value('nama')
                                ],
                                'deskripsi' => [
                                    'value' => set_value('deskripsi')
                                ],
                                'kategori' => [
                                    'value' => set_value('kategori')
                                ],
                                'harga' => [
                                    'value' => set_value('harga')
                                ],
                                'diskon' => [
                                    'value' => set_value('diskon')
                                ],
                                'status' => [
                                    'value' => set_value('status')
                                ],
                                'popular' => [
                                    'value' => set_value('popular')
                                ],
                                'stok' => [
                                    'value' => set_value('stok')
                                ],
                                'gambar' => [
                                    'value' => set_value('gambar')
                                ]
                            ]
                        ]);
                        redirect(site_url(ADMIN . '/product/' . $id . '/edit'));
                    }
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/product'));
            }
        }
    }

    public function delete($id)
    {

        $page['title']      = 'Hapus ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_produk', [
            'id' => $id,
        ]);
        if ($result->num_rows() === 1) {
            $product  = $result->row_array();
            $message = 'Apakah anda yakin ingin menghapus ' . $this->readable . ' ini : <strong>' . $product['nama'] . '</strong> ?';
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/product/delete-ajax', compact('page', 'product', 'message'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/product/delete-default', compact('page', 'product', 'message'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/product'));
        }
    }

    public function destroy($id)
    {
        $result = $this->general->edit('z_produk', [
            'id' => $id,
        ]);
        if ($result->num_rows() === 1) {
            // delete data into table and check if delete process is success or not
            // if delete process is success
            if ($this->general->delete('z_produk_gambar', ['id_produk' => $id])) {
                if ($this->general->delete('z_produk_stok', ['id_produk' => $id])) {
                    if ($this->general->delete('z_produk', ['id' => $id])) {
                        @unlink('./' . $result->row_array()['foto']);
                        $message = ucwords($this->readable) . ' berhasil dihapus.';
                        // check if request are come from ajax or default
                        if (USEAJAX) {
                            if (!$this->input->is_ajax_request()) {
                                show_error('Forbidden access', 403);
                            }
                            $response = [
                                'status'       => true,
                                'notification' => [
                                    'type'    => 'success',
                                    'message' => $message,
                                ],
                            ];
                            $this->output->set_content_type('application/json')->set_output(json_encode($response));
                        } else {
                            $this->session->set_flashdata([
                                'type'    => 'alert-success',
                                'message' => $message,
                            ]);
                            redirect(site_url(ADMIN . '/product'));
                        }
                    }
                }
            // else if insert process is error
            } else {
                $message = 'Maaf, ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status'       => false,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message,
                        ],
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message,
                    ]);
                    redirect(site_url(ADMIN . '/product'));
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/product'));
        }
    }

    private function notFoundData($page, $redirect)
    {
        if (USEAJAX) {
            $message = ucwords($this->readable) . ' tidak ditemukan.';
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/product/error-ajax', compact('page', 'message'), true),
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-warning',
                'message' => ucwords($this->readable) . ' tidak ditemukan. (' . $this->db->last_query() . ')',
            ]);
            redirect($redirect);
        }
    }

    public function pictureIndex($productId)
    {
        $page['nav-active'] = $this->navActive;

        $result = $this->product->getWithPicture([
            'z_produk.id' => $productId
        ]);
        if ($result->num_rows() === 1) {
            $product = $result->row_array();
            $page['title'] = 'Daftar Gambar ' . ucwords($this->readable) . ' : ' . $product['p_nama'];
            $data   = [
                'product_id'       => [
                    'value' => $product['p_id']
                ]
            ];
            $this->load->view('admin/product/list-picture', compact('page', 'data'));
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/product'));
        }
    }

    public function pictureDatatables()
    {
        if (!$this->input->is_ajax_request()) {
            show_error('Forbidden access', 403);
        }
        // initiate datatable input
        $data   = [];
        $draw   = $this->input->post('draw', true);
        $start  = $this->input->post('start', true);
        $length = $this->input->post('length', true);
        $order  = $this->input->post('order', true);
        $search = $this->input->post('search', true);
        $productId = $this->input->post('product_id', true);

        // get data from table with datatables
        $this->load->model('M_product_picture', 'product_picture');
        $this->product_picture->productId = $productId;
        $list = $this->product_picture->get_datatables($start, $length, $order, $search);
        foreach ($list as $key => $value) {
            $row = [];
            array_push($row, $start + $key + 1);
            array_push($row, '<a href="' . base_url($value['gambar']) . '" target="_blank"><img src="' . base_url($value['gambar']) . '" alt="' . $value['gambar'] . '" style="width: 20em;"></a>');
            $actions = [
                [
                    'href'  => site_url(ADMIN . '/product/' . $value['id'] . '/picture/delete'),
                    'class' => 'btn btn-danger' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-trash"></i> Hapus',
                ]
            ];
            array_push($row, $this->load->view('layouts/admin/action', compact('actions'), true));
            array_push($data, $row);
        }
        $response = [
            'draw'            => $draw,
            'recordsTotal'    => $this->product_picture->count_all(),
            'recordsFiltered' => $this->product_picture->count_filtered($order, $search),
            'data'            => $data,
        ];
        // output to json format
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function pictureCreate($productId)
    {
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_produk', [
            'id' => $productId
        ]);
        if ($result->num_rows() === 1) {
            $product = $result->row_array();
            $page['title'] = 'Tambah Gambar ' . ucwords($this->readable) . ' : ' . $product['nama'];
            $data   = [
                'product_id'       => [
                    'value' => $product['id']
                ]
            ];
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/product/form-picture-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/product/form-picture-default', compact('page', 'data'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/product'));
        }
    }

    public function pictureStore($productId)
    {
        $result = $this->general->edit('z_produk', [
            'id' => $productId
        ]);
        if ($result->num_rows() === 1) {
            $product = $result->row_array();
            // upload product picture
            $uploadPath = 'uploads/product';
            $productId   = $product['id'];
            $fileName   = 'product-' . date('YmdHis') . '-' . $productId;
            $this->load->library('upload');
            $this->upload->initialize([
                'upload_path'      => './' . $uploadPath,
                'allowed_types'    => 'gif|jpg|png|jpeg',
                'file_ext_tolower' => true,
                'max_size'         => 1024,
                'file_name'        => $fileName
            ]);
            if (!$this->upload->do_upload('gambar')) {
                $message = ucwords($this->readable) . ' gagal ditambahkan.<br>' . $this->upload->display_errors();
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status'       => true,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message,
                        ],
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message
                    ]);
                    redirect(site_url(ADMIN . '/product/' . $productId . '/picture'));
                }
            } else {
                $collection = [ // collect data (true for avoid xss injection)
                    'id'        => (int) $this->general->lastId('z_produk_gambar') + 1,
                    'id_produk' => $productId,
                    'gambar'    => $uploadPath . '/' . $this->upload->data()['file_name']
                ];
                // insert data into table and check if insert process is success or not
                // if insert process is success
                if ($this->general->insertId('z_produk_gambar', $collection)) {
                    $message = 'Gambar ' . $this->readable . ' berhasil ditambahkan.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message,
                        ]);
                        redirect(site_url(ADMIN . '/product/' . $productId . '/picture'));
                    }
                    // else if insert process is error
                } else {
                    // delete fail picture
                    @unlink('./' . $collection['gambar']);
                    $message = 'Maaf, gambar ' . $this->readable . ' gagal ditambahkan. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'      => 'alert-danger',
                            'message'   => $message
                        ]);
                        redirect(site_url(ADMIN . '/product/' . $productId . '/picture'));
                    }
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/product'));
        }
    }

    public function pictureDelete($productPictureId)
    {
        $page['nav-active'] = $this->navActive;

        $result = $this->product->getWithPicture([
            'z_produk_gambar.id' => $productPictureId
        ]);
        if ($result->num_rows() === 1) {
            $product  = $result->row_array();
            $page['title'] = 'Hapus Gambar ' . ucwords($this->readable) . ' : ' . $product['p_nama'];
            $message = 'Apakah anda yakin ingin menghapus gambar dari ' . $this->readable . ' ' . $product['p_nama'] . ' ini : <strong><a href="' . base_url($product['pg_gambar']) . '" target="_blank">' . base_url($product['pg_gambar']) . '</a></strong> ?';
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/product/delete-picture-ajax', compact('page', 'product', 'message'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/product/delete-picture-default', compact('page', 'product', 'message'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/product'));
        }
    }

    public function pictureDestroy($productPictureId)
    {
        $result = $this->product->getWithPicture([
            'z_produk_gambar.id' => $productPictureId
        ]);
        if ($result->num_rows() === 1) {
            $product = $result->row_array();
            // delete data into table and check if delete process is success or not
            // if delete process is success
            if ($this->general->delete('z_produk_gambar', ['id' => $productPictureId])) {
                @unlink('./' . $product['pg_gambar']);
                $message = 'Gambar ' . $this->readable . ' berhasil dihapus.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status'       => true,
                        'notification' => [
                            'type'    => 'success',
                            'message' => $message,
                        ],
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => $message,
                    ]);
                    redirect(site_url(ADMIN . '/product/' . $product['p_id'] . '/picture'));
                }
            // else if insert process is error
            } else {
                $message = 'Maaf, gambar ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status'       => false,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message,
                        ],
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message,
                    ]);
                    redirect(site_url(ADMIN . '/product/' . $product['p_id'] . '/picture'));
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/product'));
        }
    }

}
/* End of file AdminProduct.php */
/* Location: ./application/controllers/AdminProduct.php */
