<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserTransaction extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    private $readable = 'transaksi';
    private $navActive = 'transaction';

    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect member if not login
        if (!$this->session->has_userdata('member')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(site_url('login?redirect=' . urlencode(current_url())));
        }
        // load "M_transaction" model
        $this->load->model('M_transaction', 'transaction');
    }

    public function index()
    {
        // redirect member if not login
        if (!$this->session->has_userdata('member')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(site_url('login?redirect=' . urlencode(current_url())));
        }

        $page['title']      = 'Transaksi';
        $page['nav-active'] = $this->navActive;

        // get transaction list
        $results = $this->general->edit('z_transaksi', [
            'member_id' => $this->session->userdata('member')['id']
        ]);
        $transactions = $results->result_array();
        foreach ($transactions as $key => $transaction) {
            $transactions[$key]['status_color'] = $this->transaction->statusColor[(int) $transaction['status']];
            $transactions[$key]['status_text']  = $this->transaction->statusText[(int) $transaction['status']];
        }
        $this->load->view('user/transaction/index', compact('page', 'transactions'));
    }

    public function show($id)
    {
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_transaksi', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $transaction = $result->row_array();
            $transaction['status_color'] = $this->transaction->statusColor[(int) $transaction['status']];
            $transaction['status_text']  = $this->transaction->statusText[(int) $transaction['status']];
            $page['title'] = $transaction['kode'];
            // get member data
            $resultMember = $this->general->edit('z_member', [
                'id' => $transaction['member_id']
            ]);
            if ($resultMember->num_rows() === 1) {
                $member = $resultMember->row_array();
                // get detail transaction data
                $resultsTransactionDetail = $this->transaction->getDetailWithProduct([
                    'z_detail_transaksi.id_transaksi' => $id
                ]);
                if ($resultsTransactionDetail->num_rows() > 0) {
                    $transactionDetails = $resultsTransactionDetail->result_array();
                    $this->load->view('user/transaction/show', compact('page', 'transaction', 'member', 'transactionDetails'));
                }
            }
        } else {
            $this->notFoundData();
        }
    }

    // $transactionEncryptCode = md5($transaction['id'] . '-' . $transaction['kode']
    public function upload($transactionEncryptCode)
    {
        $page['nav-active'] = $this->navActive;
        $page['title']      = 'Upload Bukti Transaksi';

        // mathcing transactionEncryptCode with "z_transaksi" table
        $result = $this->general->edit('z_transaksi', [
            'bukti_transfer' => $transactionEncryptCode
        ]);
        if ($result->num_rows() === 1) {
            $transaction = $result->row_array();
            $transaction['status_color'] = $this->transaction->statusColor[(int) $transaction['status']];
            $transaction['status_text']  = $this->transaction->statusText[(int) $transaction['status']];
            // get member data
            $resultMember = $this->general->edit('z_member', [
                'id' => $transaction['member_id']
            ]);
            if ($resultMember->num_rows() === 1) {
                $member = $resultMember->row_array();
                // get detail transaction data
                $resultsTransactionDetail = $this->transaction->getDetailWithProduct([
                    'z_detail_transaksi.id_transaksi' => $transaction['id']
                ]);
                if ($resultsTransactionDetail->num_rows() > 0) {
                    $transactionDetails = $resultsTransactionDetail->result_array();
                    $this->load->view('user/transaction/upload', compact('page', 'transaction', 'member', 'transactionDetails'));
                }
            }
        } else {
            $result = $this->general->edit('z_transaksi', [
                'SUBSTRING_INDEX(bukti_transfer, \'|\', -1) = ' => $transactionEncryptCode
            ]);
            if ($result->num_rows() === 1) {
                $transaction = $result->row_array();
                $this->notFoundData('Maaf, anda sudah melampirkan bukti transaksi sebelumnya. Silahkan cek kembali detail transaksi anda.', 'transaction/' . $transaction['id']);
            } else {
                $this->notFoundData();
            }
        }
    }

    public function update($id)
    {
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('transaction-encrypt-code', '', 'required');
        // if form_validation is fail (username are not filled)
        if ($this->form_validation->run() == false) {
            $this->notFoundData();
        } else {
            $result = $this->general->edit('z_transaksi', [
                'id' => $id
            ]);
            if ($result->num_rows() === 1) {
                $transaction     = $result->row_array();
                $uploadPath      = 'uploads/transaction';
                $fileName        = 'transaction-' . date('YmdHis') . '-' . $transaction['id'] . '-' . $transaction['kode'];
                $this->load->library('upload');
                $this->upload->initialize([
                    'upload_path'      => './' . $uploadPath,
                    'allowed_types'    => 'jpg|png|jpeg',
                    'file_ext_tolower' => true,
                    'max_size'         => 1024,
                    'file_name'        => $fileName
                ]);
                if (!$this->upload->do_upload('bukti_transfer')) {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Bukti ' . $this->readable . ' ' . $transaction['kode'] . ' gagal ditambahkan.<br>' . $this->upload->display_errors()
                    ]);
                    redirect(site_url('transaction/' . $this->input->post('transaction-encrypt-code') . '/upload'));
                } else {
                    $condition = [
                        'id' => $transaction['id']
                    ];
                    $collection = [ // collect data (true for avoid xss injection)
                        'bukti_transfer' => $uploadPath . '/' . $this->upload->data()['file_name'] . '|' . $this->input->post('transaction-encrypt-code')
                    ];
                    // update stock in "z_produk_stok" table
                    // if update process is success
                    if ($this->general->update('z_transaksi', $condition, $collection)) {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => 'Bukti ' . $this->readable . ' ' . $transaction['kode'] . ' berhasil ditambahkan.',
                        ]);
                        redirect(site_url('transaction/' . $transaction['id']));
                    // else if insert process is error
                    } else {
                        // delete fail picture
                        @unlink('./' . $collection['bukti_transfer']);
                        $this->session->set_flashdata([
                            'type'      => 'alert-danger',
                            'message'   => 'Bukti ' . $this->readable . ' ' . $transaction['kode'] . ' gagal ditambahkan'
                        ]);
                        redirect(site_url('transaction/' . $this->input->post('transaction-encrypt-code') . '/upload'));
                    }
                }
            } else {
                $this->notFoundData();
            }
        }
    }

    public function delete($id)
    {
        $page['title']      = 'Hapus transaksi';
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_transaksi', [
            'id' => $id,
        ]);
        if ($result->num_rows() === 1) {
            $transaction = $result->row_array();
            $resultsTransactionDetail = $this->transaction->getDetailWithProduct([
                'z_detail_transaksi.id_transaksi' => $id
            ]);
            if ($resultsTransactionDetail->num_rows() > 0) {
                $transactionDetails = $resultsTransactionDetail->result_array();
                $this->load->view('user/transaction/delete', compact('page', 'transaction', 'transactionDetails'));
            }
        } else {
            $this->notFoundData();
        }
    }

    public function destroy($id)
    {
        $result = $this->general->edit('z_transaksi', [
            'id' => $id,
        ]);
        if ($result->num_rows() === 1) {
            // return product stock from detail transaction data
            $resultsTransactionDetail = $this->transaction->getDetailWithProduct([
                'z_detail_transaksi.id_transaksi' => $id
            ]);
            if ($resultsTransactionDetail->num_rows() > 0) {
                $transactionDetails = $resultsTransactionDetail->result_array();
                $updateProductStockStatus = [];
                $this->load->model('M_product', 'product');
                foreach ($transactionDetails as $key => $transactionDetail) {
                    // get last stock
                    $condition = [
                        'id_produk' => $transactionDetail['dt_idbarang']
                    ];
                    $collection = [ // collect data (true for avoid xss injection)
                        'stok' => $this->product->getLastStock($transactionDetail['dt_idbarang']) + $transactionDetail['dt_jumlah']
                    ];
                    // update stock in "z_produk_stok" table
                    // if update process is success
                    if ($this->general->update('z_produk_stok', $condition, $collection)) {
                        array_push($updateProductStockStatus, true);
                    } else {
                        array_push($updateProductStockStatus, false);
                    }
                }
                if (!in_array(false, $updateProductStockStatus)) {
                    // delete data into table and check if delete process is success or not
                    // if delete process is success
                    if ($this->general->delete('z_detail_transaksi', ['id_transaksi' => $id])) {
                        if ($this->general->delete('z_transaksi', ['id' => $id])) {
                            @unlink('./' . $result->row_array()['bukti_transfer']);
                            $this->session->set_flashdata([
                                'type'    => 'alert-success',
                                'message' => ucwords($this->readable) . ' berhasil dihapus.',
                            ]);
                            redirect(site_url('transaction'));
                        // else if insert process is error
                        } else {
                            $this->session->set_flashdata([
                                'type'    => 'alert-danger',
                                'message' => 'Maaf, ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.',
                            ]);
                            redirect(site_url('transaction'));
                        }
                    }
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Maaf, ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.',
                    ]);
                    redirect(site_url('transaction'));
                }
            }
        } else {
            $this->notFoundData();
        }
    }

    // destroy bukti_transfer
    public function uploadDestroy($id)
    {
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('transaction-encrypt-code', '', 'required');
        // if form_validation is fail (username are not filled)
        if ($this->form_validation->run() == false) {
            $this->notFoundData();
        } else {
            $result = $this->general->edit('z_transaksi', [
                'id' => $id,
            ]);
            if ($result->num_rows() === 1) {
                $transaction = $result->row_array();
                $condition = [
                    'id' => $transaction['id']
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'bukti_transfer' => $this->input->post('transaction-encrypt-code')
                ];
                // update stock in "z_produk_stok" table
                // if update process is success
                if ($this->general->update('z_transaksi', $condition, $collection)) {
                    @unlink('./' . explode('|', $transaction['bukti_transfer'])[0]);
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => 'Bukti ' . $this->readable . ' berhasil dihapus.',
                    ]);
                    redirect(site_url('transaction/' . $transaction['id']));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => 'Maaf, bukti ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.',
                    ]);
                    redirect(site_url('transaction/' . $transaction['id']));
                }
            } else {
                $this->notFoundData();
            }
        }
    }

    private function notFoundData($message = null, $redirect = null)
    {
        $this->session->set_flashdata([
            'type'    => 'alert-danger',
            'message' => $message != null ? $message : 'Maaf, ' . $this->readable . ' anda tidak ditemukan.'
        ]);
        redirect(site_url($redirect != null ? $redirect : 'transaction'));
    }
}
/* End of file UserTransaction.php */
/* Location: ./application/controllers/UserTransaction.php */
