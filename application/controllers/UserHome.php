<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserHome extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
    }

    public function index($categorySelect = null)
    {
        // if user goes to 'http://tutarajaya.lol/category', $page['title'] change into 'Kategori'
        if ($this->uri->segment(1) == 'category') {
            $page['title'] = 'Kategori';
        } else {
            $page['title'] = 'Beranda';
        }
        $page['nav-active'] = 'home';

        // get list of category from 'z_kategori' table
        $categories = $this->general->getData('z_kategori')->result_array();

        // get list of slider from 'z_slider' table
        $sliders = $this->general->edit('z_slider', [
            'status' => '1'
        ])->result_array();

        // get list of product from 'z_produk' table edit
        $this->load->model('M_product', 'product');
        if (strlen($categorySelect) > 0) {
            // get data based on choosed category
            $products = $this->product->get([
                'z_produk.status'   => '1', // active status
                'z_produk.kategori' => $categorySelect
            ], true)->result_array();
        } else {
            // get all data
            $products = $this->product->get([
                'z_produk.status'   => '1' // active status
            ], true)->result_array();
        }
        $this->load->view('user/home/index', compact('page', 'categories', 'products', 'sliders', 'categorySelect'));
    }

    public function show($id)
    {
        $page['nav-active'] = 'home';
        $this->load->model('M_product', 'product');
        $product = $this->product->get([
            'z_produk.id' => $id
        ], true)->row_array();
        $page['title'] = $product['p_nama'];

        $this->load->view('user/home/show', compact('page', 'product'));
    }

}
/* End of file UserHome.php */
/* Location: ./application/controllers/UserHome.php */
