<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminSlider extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    private $readable = 'slider';
    private $navActive = 'master';

    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect admin if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(ADMIN . '/login?redirect=' . urlencode(current_url()));
        }
    }

    public function index()
    {
        $page['title']      = 'Master ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;
        $this->load->view('admin/slider/index', compact('page'));
    }

    public function datatables()
    {
        if (!$this->input->is_ajax_request()) {
            show_error('Forbidden access', 403);
        }
        // initiate datatable input
        $data   = [];
        $draw   = $this->input->post('draw', true);
        $start  = $this->input->post('start', true);
        $length = $this->input->post('length', true);
        $order  = $this->input->post('order', true);
        $search = $this->input->post('search', true);

        // get data from table with datatables
        $this->load->model('M_slider', 'datatables');
        $list = $this->datatables->get_datatables($start, $length, $order, $search);
        foreach ($list as $key => $value) {
            $row = [];
            array_push($row, $start + $key + 1);
            array_push($row, '<a href="' . base_url($value['foto']) . '" target="_blank"><img src="' . base_url($value['foto']) . '" alt="' . $value['foto'] . '" style="width: 20em;"></a>');
            if ($value['status'] == '1') {
                array_push($row, '<span class="btn btn-success btn-block" style="cursor: default;">Aktif</span>');
            } else {
                array_push($row, '<span class="btn btn-warning btn-block" style="cursor: default;">Tidak aktif</span>');
            }
            $actions = [
                [
                    'href'  => site_url(ADMIN . '/slider/' . $value['id'] . '/status'),
                    'class' => 'btn' . (USEAJAX ? ' action-ajax' : '') . ($value['status'] == '0' ? ' btn-success' : ' btn-warning'),
                    'title' => ($value['status'] == '0' ? '<i class="fa fa-star-o"></i> Aktifkan' : '<i class="fa fa-star"></i> Non-aktifkan')
                ],
                [
                    'href'  => site_url(ADMIN . '/slider/' . $value['id'] . '/delete'),
                    'class' => 'btn btn-danger' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-trash"></i> Hapus',
                ],
                // [
                //     'href'  => site_url(ADMIN . '/slider/' . $value['id'] . '/edit'),
                //     'class' => 'btn btn-info' . (USEAJAX ? ' action-ajax' : ''),
                //     'title' => '<i class="fa fa-pencil"></i> Edit',
                // ]
            ];
            array_push($row, $this->load->view('layouts/admin/action', compact('actions'), true));
            array_push($data, $row);
        }
        $response = [
            'draw'            => $draw,
            'recordsTotal'    => $this->datatables->count_all(),
            'recordsFiltered' => $this->datatables->count_filtered($order, $search),
            'data'            => $data
        ];
        // output to json format
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create()
    {
        $page['title']      = 'Tambah ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        if (USEAJAX) {
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/slider/form-ajax', compact('page'), true)
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->load->view('admin/slider/form-default', compact('page'));
        }
    }

    public function store()
    {
        $page['title'] = 'Tambah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('status', 'status', 'required');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'foto' => [
                        'error' => form_error('foto')
                    ],
                    'status' => [
                        'value' => set_value('status'),
                        'error' => form_error('status')
                    ]
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/slider/form-ajax', compact('page', 'data'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'foto' => [
                            'error' => form_error('foto')
                        ],
                        'status' => [
                            'value' => set_value('status'),
                            'error' => form_error('status')
                        ]
                    ],
                ]);
                redirect(site_url(ADMIN . '/slider/create'));
            }
        // else if form validation is success
        } else {
            // upload slider picture
            $uploadPath = 'uploads/slider';
            $sliderId   = (int) $this->general->lastId('z_slider') + 1;
            $fileName   = 'slider-' . date('YmdHis') . '-' . $sliderId;
            $this->load->library('upload');
            $this->upload->initialize([
                'upload_path'      => './' . $uploadPath,
                'allowed_types'    => 'gif|jpg|png|jpeg',
                'file_ext_tolower' => true,
                'max_size'         => 1024,
                'file_name'        => $fileName
            ]);
            if (!$this->upload->do_upload('foto')) {
                $message = ucwords($this->readable) . ' gagal ditambahkan.<br>' . $this->upload->display_errors();
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status'       => true,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message,
                        ],
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message
                    ]);
                    redirect(site_url(ADMIN . '/slider'));
                }
            } else {
                $collection = [ // collect data (true for avoid xss injection)
                    'id'     => $sliderId,
                    'foto'   => $uploadPath . '/' . $this->upload->data()['file_name'],
                    'status' => $this->input->post('status', true)
                ];
                // insert data into table and check if insert process is success or not
                // if insert process is success
                if ($this->general->insertId('z_slider', $collection)) {
                    $message = ucwords($this->readable) . ' berhasil ditambahkan.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message,
                        ]);
                        redirect(site_url(ADMIN . '/slider'));
                    }
                    // else if insert process is error
                } else {
                    // delete fail picture
                    @unlink('./' . $collection['foto']);
                    $message = 'Maaf, ' . $this->readable . ' gagal ditambahkan. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'      => 'alert-danger',
                            'message'   => $message,
                            'form_data' => [
                                'status' => [
                                    'value' => $collection['status'],
                                ],
                            ],
                        ]);
                        redirect(site_url(ADMIN . '/slider/create'));
                    }
                }
            }
        }
    }

    public function show($id)
    {
        $this->edit($id);
    }

    public function edit($id)
    {
        $page['title']      = 'Ubah ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_slider', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $slider = $result->row_array();
            $data   = [
                'id'       => [
                    'value' => $slider['id']
                ],
                'foto' => [
                    'value' => $slider['foto']
                ],
                'status' => [
                    'value' => $slider['status']
                ]
            ];
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/slider/form-ajax', compact('page', 'data'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/slider/form-default', compact('page', 'data'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/slider'));
        }
    }

    public function update($id)
    {
        $page['title'] = 'Ubah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('status', 'status', 'required');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'id'       => [
                        'value' => $id,
                    ],
                    'foto' => [
                        'value' => set_value('foto')
                    ],
                    'status' => [
                        'value' => set_value('status'),
                        'error' => form_error('status'),
                    ],
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/slider/form-ajax', compact('page', 'data'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'id'       => [
                            'value' => $id,
                        ],
                        'foto' => [
                            'value' => set_value('foto')
                        ],
                        'status' => [
                            'value' => set_value('status'),
                            'error' => form_error('status'),
                        ],
                    ],
                ]);
                redirect(site_url(ADMIN . '/slider/' . $id . '/edit'));
            }
            // else if form validation is success
        } else {
            $result = $this->general->edit('z_slider', [
                'id' => $id,
            ]);
            if ($result->num_rows() === 1) {
                $condition = [
                    'id' => $id
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'status' => $this->input->post('status', true)
                ];
                // udpate data into table and check if update process is success or not
                // if update process is success
                if ($this->general->update('z_slider', $condition, $collection)) {
                    $message = ucwords($this->readable) . ' </strong> berhasil diubah menjadi <strong>' . ($collection['status'] == 1 ? 'aktif' : 'tidak aktif') . '</strong>.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message,
                        ]);
                        redirect(site_url(ADMIN . '/slider'));
                    }
                    // else if update process is error
                } else {
                    $message = 'Maaf, ' . $this->readable . ' gagal diubah. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status'       => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message,
                            ],
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'      => 'alert-danger',
                            'message'   => $message,
                            'form_data' => [
                                'foto' => [
                                    'value' => set_value('foto')
                                ],
                                'status' => [
                                    'value' => set_value('status')
                                ]
                            ],
                        ]);
                        redirect(site_url(ADMIN . '/slider/' . $id . '/edit'));
                    }
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/slider'));
            }
        }
    }

    public function delete($id)
    {

        $page['title']      = 'Hapus ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_slider', [
            'id' => $id,
        ]);
        if ($result->num_rows() === 1) {
            $slider  = $result->row_array();
            $data = [
                'foto' => [
                    'value' => $slider['foto']
                ]
            ];
            $message = 'Apakah anda yakin ingin menghapus ' . $this->readable . ' ini : <strong><a href="' . base_url($slider['foto']) . '" target="_blank">' . base_url($slider['foto']) . '</a></strong>?';
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/slider/delete-ajax', compact('page', 'slider', 'message', 'data'), true),
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/slider/delete-default', compact('page', 'slider', 'message', 'data'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/slider'));
        }
    }

    public function destroy($id)
    {
        $result = $this->general->edit('z_slider', [
            'id' => $id,
        ]);
        if ($result->num_rows() === 1) {
            // delete data into table and check if delete process is success or not
            // if delete process is success
            if ($this->general->delete('z_slider', ['id' => $id])) {
                @unlink('./' . $result->row_array()['foto']);
                $message = ucwords($this->readable) . ' berhasil dihapus.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status'       => true,
                        'notification' => [
                            'type'    => 'success',
                            'message' => $message,
                        ],
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => $message,
                    ]);
                    redirect(site_url(ADMIN . '/slider'));
                }
                // else if insert process is error
            } else {
                $message = 'Maaf, ' . $this->readable . ' gagal dihapus. Silahkan coba lagi.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status'       => false,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message,
                        ],
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message,
                    ]);
                    redirect(site_url(ADMIN . '/slider'));
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/slider'));
        }
    }

    public function statusGet($id)
    {
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_slider', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $slider = $result->row_array();
            $page['title'] = ($slider['status'] == '0' ? 'Aktivasi' : 'Deaktivasi') . ' ' . ucwords($this->readable);
            $data = [
                'id' => [
                    'value' => $slider['id']
                ],
                'foto' => [
                    'value' => $slider['foto']
                ],
                'status' => [
                    'value' => $slider['status']
                ]
            ];
            $message = 'Apakah anda yakin ingin ' . ($slider['status'] == '0' ? 'mengaktifkan' : 'menonaktifkan') . ' ' . $this->readable . ' ini : <strong><a href="' . base_url($slider['foto']) . '" target="_blank">' . base_url($slider['foto']) . '</a></strong>?';
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/slider/activate-deactivate-ajax', compact('page', 'data', 'message'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/slider/activate-deactivate-default', compact('page', 'data', 'message'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/slider'));
        }
    }

    public function statusPost($id)
    {
        $page['title'] = 'Ubah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('status', 'status', 'required');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'id' => [
                        'value' => $id
                    ],
                    'status' => [
                        'value' => set_value('status'),
                        'error' => form_error('status')
                    ]
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/slider/form-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'id' => [
                            'value' => $id
                        ],
                        'status' => [
                            'value' => set_value('status'),
                            'error' => form_error('status')
                        ]
                    ]
                ]);
                redirect(site_url(ADMIN . '/slider/' . $id . '/edit'));
            }
        // else if form validation is success
        } else {
            $result = $this->general->edit('z_slider', [
                'id' => $id
            ]);
            if ($result->num_rows() === 1) {
                $condition = [
                    'id' => $id
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'status' => $this->input->post('status', true)
                ];
                // udpate data into table and check if update process is success or not
                // if update process is success
                if ($this->general->update('z_slider', $condition, $collection)) {
                    $message = ucwords($this->readable) . ' berhasil diubah menjadi <strong>' . ($collection['status'] == '1' ? 'Aktif': 'Tidak aktif') . '</strong>.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message
                        ]);
                        redirect(site_url(ADMIN . '/slider'));
                    }
                // else if update process is error
                } else {
                    $message = 'Maaf, ' . $this->readable . ' gagal ' . ($collection['status'] == '1' ? 'diaktifkan': 'dinonaktifkan') . '. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-danger',
                            'message' => $message,
                            'form_data' => [
                                'status'  => [
                                    'value' => $collection['status']
                                ]
                            ]
                        ]);
                        redirect(site_url(ADMIN . '/slider/' . $id . '/edit'));
                    }
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/slider'));
            }
        }
    }

    private function notFoundData($page, $redirect)
    {
        if (USEAJAX) {
            $message = ucwords($this->readable) . ' tidak ditemukan.';
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/slider/error-ajax', compact('page', 'message'), true),
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-warning',
                'message' => ucwords($this->readable) . ' tidak ditemukan.',
            ]);
            redirect($redirect);
        }
    }
}
/* End of file AdminSlider.php */
/* Location: ./application/controllers/AdminSlider.php */
