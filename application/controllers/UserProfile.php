<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserProfile extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect member if not login
        if (!$this->session->has_userdata('member')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect('login?redirect=' . urlencode(current_url()));
        }
    }

    public function show()
    {
        $page['title']      = 'Profil';
        $page['nav-active'] = 'profile';

        $id = $this->session->userdata('member')['id'];
        $result = $this->general->edit('z_member', ['id' => $id]);
        if ($result->num_rows() === 1) {
            $member   = $result->row_array();
            $data = [
                'id'              => [
                    'value' => $member['id']
                ],
                'username'        => [
                    'value' => $member['username']
                ],
                'email'           => [
                    'value' => $member['email']
                ],
                'fullname'        => [
                    'value' => $member['fullname']
                ],
                'alamat'         => [
                    'value' => $member['alamat']
                ],
                'province'        => [
                    'value' => $member['province']
                ],
                'city'            => [
                    'value' => $member['city']
                ],
                'telp'            => [
                    'value' => $member['telp']
                ],
                'mode-edit'    => false,
                'selected-tab' => 'update-profile'
            ];
            $this->load->view('user/profile/index', compact('page', 'data'));
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Maaf, akun anda tidak ditemukan'
            ]);
            redirect(site_url());
        }
    }

    public function update()
    {
        $id = $this->session->userdata('member')['id'];
        $result = $this->general->edit('z_member', ['id' => $id]);
        $this->load->library('form_validation');
        if ($result->num_rows() === 1) {
            $selectedTab = strlen($this->input->post('selected-tab')) > 0 ? $this->input->post('selected-tab') : '';
            switch ($selectedTab) {
                case 'update-profile': {
                    // see codeigniter reference rules (required, alpha_dash, etc)
                    // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
                    $this->form_validation->set_rules('username', 'username', 'trim|required|alpha_dash|min_length[4]|max_length[15]|edit_unique[z_member.username.id.' .  $id. ']', [
                        'edit_unique' => 'The username has been registered already'
                    ]);
                    $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|edit_unique[z_member.email.id.' .  $id. ']', [
                        'edit_unique' => 'The email has been registered already'
                    ]);
                    $this->form_validation->set_rules('fullname', 'full name', 'required|max_length[100]');
                    $this->form_validation->set_rules('alamat', 'address', 'max_length[200]');
                    $this->form_validation->set_rules('province', 'province', 'max_length[100]');
                    $this->form_validation->set_rules('city', 'city', 'max_length[100]');
                    $this->form_validation->set_rules('telp', 'telephone number', 'numeric|max_length[15]');
                    // if form validation is fail
                    if ($this->form_validation->run() == false) {
                        $this->session->set_flashdata([ // show error form validation is session/flashdata
                            'form_data' => [
                                'id' => [
                                    'value' => $id
                                ],
                                'username'        => [
                                    'value' => set_value('username'),
                                    'error' => form_error('username')
                                ],
                                'email'           => [
                                    'value' => set_value('email'),
                                    'error' => form_error('email')
                                ],
                                'fullname'        => [
                                    'value' => set_value('fullname'),
                                    'error' => form_error('fullname')
                                ],
                                'alamat'         => [
                                    'value' => set_value('alamat'),
                                    'error' => form_error('alamat')
                                ],
                                'province'        => [
                                    'value' => set_value('province'),
                                    'error' => form_error('province')
                                ],
                                'city'            => [
                                    'value' => set_value('city'),
                                    'error' => form_error('city')
                                ],
                                'telp'            => [
                                    'value' => set_value('telp'),
                                    'error' => form_error('telp')
                                ],
                                'mode-edit' => true,
                                'selected-tab' => $selectedTab
                            ]
                        ]);
                        redirect(site_url('profile'));
                    // else if form validation is success
                    } else {
                        $result = $this->general->edit('z_member', [
                            'id' => $id
                        ]);
                        if ($result->num_rows() === 1) {
                            $condition = [
                                'id' => $id
                            ];
                            $collection = [ // collect data from register-form (true for avoid xss injection)
                                'username'   => $this->input->post('username', true),
                                'fullname'   => $this->input->post('fullname', true),
                                'email'      => $this->input->post('email', true),
                                'province'   => $this->input->post('province', true),
                                'city'       => $this->input->post('city', true),
                                'alamat'     => $this->input->post('alamat', true),
                                'telp'       => $this->input->post('telp', true)
                            ];
                            // udpate data into table and check if update process is success or not
                            // if update process is success
                            if ($this->general->update('z_member', $condition, $collection)) {
                                $message = 'Profil anda berhasil diubah.';
                                $this->session->set_flashdata([
                                    'type'    => 'alert-success',
                                    'message' => $message
                                ]);
                                redirect(site_url('profile'));
                            // else if update process is error
                            } else {
                                $message = 'Maaf, profil anda gagal diubah. Silahkan coba lagi.';
                                $this->session->set_flashdata([
                                    'type'    => 'alert-danger',
                                    'message' => $message,
                                    'form_data' => [
                                        'username'  => [
                                            'value' => $collection['username']
                                        ],
                                        'email'     => [
                                            'value' => $collection['email']
                                        ],
                                        'fullname'  => [
                                            'value' => $collection['fullname']
                                        ],
                                        'alamat'   => [
                                            'value' => $collection['alamat']
                                        ],
                                        'province'  => [
                                            'value' => $collection['province']
                                        ],
                                        'city'      => [
                                            'value' => $collection['city']
                                        ],
                                        'telp'      => [
                                            'value' => $collection['telp']
                                        ],
                                        'mode-edit' => true,
                                        'selected-tab' => $selectedTab
                                    ]
                                ]);
                                redirect(site_url('profile'));
                            }
                        } else {
                            show_error('Forbidden access', 403);
                        }
                    }
                }
                break;
                
                case 'reset-password': {
                    $member = $result->row_array();
                    // see codeigniter reference rules (required, alpha_dash, etc)
                    // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
                    $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]');
                    $this->form_validation->set_rules('retype-password', 'retype password', 'trim|required|matches[password]');
                    // if form validation is fail
                    if ($this->form_validation->run() == false) {
                        $this->session->set_flashdata([ // show error form validation is session/flashdata
                            'form_data' => [
                                'id' => [
                                    'value' => $id
                                ],
                                'password'        => [
                                    // empty value
                                    'error' => form_error('password')
                                ],
                                'retype-password' => [
                                    // empty value
                                    'error' => form_error('retype-password')
                                ],
                                'username'        => [
                                    'value' => $member['username']
                                ],
                                'email'           => [
                                    'value' => $member['email']
                                ],
                                'fullname'        => [
                                    'value' => $member['fullname']
                                ],
                                'alamat'         => [
                                    'value' => $member['alamat']
                                ],
                                'province'        => [
                                    'value' => $member['province']
                                ],
                                'city'            => [
                                    'value' => $member['city']
                                ],
                                'telp'            => [
                                    'value' => $member['telp']
                                ],
                                'mode-edit' => true,
                                'selected-tab' => $selectedTab
                            ]
                        ]);
                        redirect(site_url('profile'));
                    // else if form validation is success
                    } else {
                        $result = $this->general->edit('z_member', [
                            'id' => $id
                        ]);
                        if ($result->num_rows() === 1) {
                            $this->load->library('password');
                            $condition = [
                                'id' => $id
                            ];
                            $collection = [ // collect data (true for avoid xss injection)
                                'password' => $this->password->hash($this->input->post('password', true))
                            ];
                            // udpate data into table and check if update process is success or not
                            // if update process is success
                            if ($this->general->update('z_member', $condition, $collection)) {
                                $message = 'Proses ganti password anda berhasil dilakukan.';
                                $this->session->set_flashdata([
                                    'type'    => 'alert-success',
                                    'message' => $message
                                ]);
                                redirect(site_url('profile'));
                            // else if update process is error
                            } else {
                                $message = 'Maaf, proses ganti password anda gagal dilakukan. Silahkan coba lagi.';
                                $this->session->set_flashdata([
                                    'type'    => 'alert-danger',
                                    'message' => $message,
                                    'form_data' => [
                                        'id' => [
                                            'value' => $id
                                        ],
                                        'password'        => [
                                            // empty value
                                            'error' => form_error('password')
                                        ],
                                        'retype-password' => [
                                            // empty value
                                            'error' => form_error('retype-password')
                                        ],
                                        'username'        => [
                                            'value' => $member['username']
                                        ],
                                        'email'           => [
                                            'value' => $member['email']
                                        ],
                                        'fullname'        => [
                                            'value' => $member['fullname']
                                        ],
                                        'alamat'         => [
                                            'value' => $member['alamat']
                                        ],
                                        'province'        => [
                                            'value' => $member['province']
                                        ],
                                        'city'            => [
                                            'value' => $member['city']
                                        ],
                                        'telp'            => [
                                            'value' => $member['telp']
                                        ],
                                        'mode-edit' => true,
                                        'selected-tab' => $selectedTab
                                    ]
                                ]);
                                redirect(site_url('profile'));
                            }
                        } else {
                            show_error('Forbidden access', 403);
                        }
                    }
                }
                break;
            }
        } else {
            show_error('Forbidden access', 403);
        }
    }

    public function dummy()
    {
        $heading = 'Add Dummy Admin Post';
        $result = $this->general->edit('z_user', [
            'username' => 'admin'
        ]);
        if ($result->num_rows() === 1) {
            $content = ''
            . '<h1>' . $heading . '</h1>'
            . '<p>Data was exist</p>';
            $this->load->view('home/blank', compact('content'));
        } else {
            $this->load->library('password');
            $collection = [
                'id'         => (int) $this->general->lastId('z_user') + 1,
                'username'   => 'admin',
                'email'      => 'admin@tutarajaya.lol',
                'password'   => $this->password->hash('admin'),
                'created_at' => date('Y-m-d H:i:s')
            ];
            if ($this->general->add('z_user', $collection)) {
                $content = ''
                . '<h1>' . $heading . '</h1>'
                . '<p>Username : admin </p>'
                . '<p>Email : admin@tutarajaya.lol </p>'
                . '<p>Password : admin </p>';
                $this->load->view('home/blank', compact('content'));
            } else {
                $content = ''
                . '<h1>' . $heading . '</h1>'
                . '<p>Database failed.</p>';
                $this->load->view('home/blank', compact('content'));
            }
        }
    }
}
/* End of file UserProfile.php */
/* Location: ./application/controllers/UserProfile.php */
