<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminMember extends CI_Controller
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     */
    private $readable = 'member';
    private $navActive = 'member';

    public function __construct()
    {
        parent::__construct();
        // Set Error date_default_timezone_set in PHP 5.3
        if (!ini_get('date_default_timezone_set')) {
            date_default_timezone_set('Asia/Makassar');
        }
        // redirect admin if not login
        if (!$this->session->has_userdata('user')) {
            $this->session->set_flashdata([
                'type'    => 'alert-danger',
                'message' => 'Anda harus login untuk mengakses halaman ini'
            ]);
            redirect(ADMIN . '/login?redirect=' . urlencode(current_url()));
        }
    }

    public function index()
    {
        $page['title']      = 'Daftar ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;
        $this->load->view('admin/member/index', compact('page'));
    }

    public function datatables()
    {
        if (!$this->input->is_ajax_request()) {
            show_error('Forbidden access', 403);
        }
        // initiate datatable input
        $data   = [];
        $draw   = $this->input->post('draw', true);
        $start  = $this->input->post('start', true);
        $length = $this->input->post('length', true);
        $order  = $this->input->post('order', true);
        $search = $this->input->post('search', true);

        // get data from table with datatables
        $this->load->model('M_member', 'datatables');
        $list = $this->datatables->get_datatables($start, $length, $order, $search);
        foreach ($list as $key => $value) {
            $row = [];
            array_push($row, $start + $key + 1);
            array_push($row, $value['username']);
            array_push($row, $value['fullname']);
            if ($value['status'] == '1') {
                array_push($row, '<span class="btn btn-success btn-block" style="cursor: default;">Aktif</span>');
            } else {
                array_push($row, '<span class="btn btn-warning btn-block" style="cursor: default;">Tidak aktif</span>');
            }
            $actions = [
                [
                    'href'  => site_url(ADMIN . '/member/' . $value['id'] . '/edit'),
                    'class' => 'btn btn-info' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-pencil"></i> Edit'
                ],
                [
                    'href'  => site_url(ADMIN . '/member/' . $value['id'] . '/password/reset'),
                    'class' => 'btn btn-primary' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-repeat"></i> Reset Password'
                ],
                [
                    'href'  => site_url(ADMIN . '/member/' . $value['id'] . '/delete'),
                    'class' => 'btn btn-danger' . (USEAJAX ? ' action-ajax' : ''),
                    'title' => '<i class="fa fa-trash"></i> Hapus'
                ],
                [
                    'href'  => site_url(ADMIN . '/member/' . $value['id'] . '/status'),
                    'class' => 'btn' . (USEAJAX ? ' action-ajax' : '') . ($value['status'] == '0' ? ' btn-success' : ' btn-warning'),
                    'title' => ($value['status'] == '0' ? '<i class="fa fa-star-o"></i> Aktifkan' : '<i class="fa fa-star"></i> Non-aktifkan')
                ]
            ];
            array_push($row, $this->load->view('layouts/admin/action', compact('actions'), true));
            array_push($data, $row);
        }
        $response = [
            'draw'            => $draw,
            'recordsTotal'    => $this->datatables->count_all(),
            'recordsFiltered' => $this->datatables->count_filtered($order, $search),
            'data'            => $data
        ];
        // output to json format
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create()
    {
        $page['title']      = 'Tambah ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        if (USEAJAX) {
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/member/form-ajax', compact('page'), true)
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->load->view('admin/member/form-default', compact('page'));
        }
    }

    public function store()
    {
        $page['title'] = 'Tambah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('username', 'username', 'trim|required|alpha_dash|min_length[4]|max_length[15]|is_unique[z_member.username]', [
            // custom error message
            'is_unique' => 'The username has been registered already',
        ]);
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[z_member.email]', [
            // custom error message
            'is_unique' => 'The email has been registered already'
        ]);
        $this->form_validation->set_rules('fullname', 'full name', 'required|max_length[100]');
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('retype-password', 'retype password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('alamat', 'address', 'max_length[200]');
        $this->form_validation->set_rules('province', 'province', 'max_length[100]');
        $this->form_validation->set_rules('city', 'city', 'max_length[100]');
        $this->form_validation->set_rules('telp', 'telephone number', 'numeric|max_length[15]');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'username'        => [
                        'value' => set_value('username'),
                        'error' => form_error('username')
                    ],
                    'email'           => [
                        'value' => set_value('email'),
                        'error' => form_error('email')
                    ],
                    'fullname'        => [
                        'value' => set_value('fullname'),
                        'error' => form_error('fullname')
                    ],
                    'password'        => [
                        // empty value
                        'error' => form_error('password')
                    ],
                    'retype-password' => [
                        // empty value
                        'error' => form_error('retype-password')
                    ],
                    'alamat'         => [
                        'value' => set_value('alamat'),
                        'error' => form_error('alamat')
                    ],
                    'province'        => [
                        'value' => set_value('province'),
                        'error' => form_error('province')
                    ],
                    'city'            => [
                        'value' => set_value('city'),
                        'error' => form_error('city')
                    ],
                    'telp'            => [
                        'value' => set_value('telp'),
                        'error' => form_error('telp')
                    ],
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/member/form-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'username'        => [
                            'value' => set_value('username'),
                            'error' => form_error('username')
                        ],
                        'email'           => [
                            'value' => set_value('email'),
                            'error' => form_error('email')
                        ],
                        'fullname'        => [
                            'value' => set_value('fullname'),
                            'error' => form_error('fullname')
                        ],
                        'password'        => [
                            // empty value
                            'error' => form_error('password')
                        ],
                        'retype-password' => [
                            // empty value
                            'error' => form_error('retype-password')
                        ],
                        'alamat'         => [
                            'value' => set_value('alamat'),
                            'error' => form_error('alamat')
                        ],
                        'province'        => [
                            'value' => set_value('province'),
                            'error' => form_error('province')
                        ],
                        'city'            => [
                            'value' => set_value('city'),
                            'error' => form_error('city')
                        ],
                        'telp'            => [
                            'value' => set_value('telp'),
                            'error' => form_error('telp')
                        ],
                    ]
                ]);
                redirect(site_url(ADMIN . '/member/create'));
            }
        // else if form validation is success
        } else {
            $this->load->library('password');
            $collection = [ // collect data from register-form (true for avoid xss injection)
                'id'         => (int) $this->general->lastId('z_member') + 1,
                'username'   => $this->input->post('username', true),
                'fullname'   => $this->input->post('fullname', true),
                'email'      => $this->input->post('email', true),
                'password'   => $this->password->hash($this->input->post('password', true)),
                'created_at' => date('Y-m-d H:i:s'),
                'status'     => '0', // set 0 to inactive account, need email confirm to change it to 1
                'province'   => $this->input->post('province', true),
                'city'       => $this->input->post('city', true),
                'alamat'     => $this->input->post('alamat', true),
                'telp'       => $this->input->post('telp', true)
            ];
            // insert data into table and check if insert process is success or not
            // if insert process is success
            if ($this->general->insertId('z_member', $collection)) {
                $message = ucwords($this->readable) . ' <strong>' . $collection['username'] . '</strong> berhasil ditambahkan.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status' => true,
                        'notification' => [
                            'type'    => 'success',
                            'message' => $message
                        ]
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => $message
                    ]);
                    redirect(site_url(ADMIN . '/member'));
                }
            // else if insert process is error
            } else {
                $message = 'Maaf, ' . $this->readable . ' gagal ditambahkan. Silahkan coba lagi.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status' => false,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message
                        ]
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message,
                        'form_data' => [
                            'username'  => [
                                'value' => $collection['username']
                            ],
                            'email'     => [
                                'value' => $collection['email']
                            ],
                            'fullname'  => [
                                'value' => $collection['fullname']
                            ],
                            'alamat'   => [
                                'value' => $collection['alamat']
                            ],
                            'province'  => [
                                'value' => $collection['province']
                            ],
                            'city'      => [
                                'value' => $collection['city']
                            ],
                            'telp'      => [
                                'value' => $collection['telp']
                            ]
                        ]
                    ]);
                    redirect(site_url(ADMIN . '/member/create'));
                }
            }
        }
    }

    public function show($id)
    {
        $this->edit($id);
    }

    public function edit($id)
    {
        $page['title']      = 'Ubah ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_member', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $member = $result->row_array();
            $data = [
                'id'              => [
                    'value' => $member['id']
                ],
                'username'        => [
                    'value' => $member['username']
                ],
                'email'           => [
                    'value' => $member['email']
                ],
                'fullname'        => [
                    'value' => $member['fullname']
                ],
                'alamat'         => [
                    'value' => $member['alamat']
                ],
                'province'        => [
                    'value' => $member['province']
                ],
                'city'            => [
                    'value' => $member['city']
                ],
                'telp'      => [
                    'value' => $member['telp']
                ]
            ];
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/member/form-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/member/form-default', compact('page', 'data'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/member'));
        }
    }

    public function update($id)
    {
        $page['title'] = 'Ubah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('username', 'username', 'trim|required|alpha_dash|min_length[4]|max_length[15]|edit_unique[z_member.username.id.' .  $id. ']', [
            'edit_unique' => 'The username has been registered already'
        ]);
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|edit_unique[z_member.email.id.' .  $id. ']', [
            'edit_unique' => 'The email has been registered already'
        ]);
        $this->form_validation->set_rules('fullname', 'full name', 'required|max_length[100]');
        $this->form_validation->set_rules('alamat', 'address', 'max_length[200]');
        $this->form_validation->set_rules('province', 'province', 'max_length[100]');
        $this->form_validation->set_rules('city', 'city', 'max_length[100]');
        $this->form_validation->set_rules('telp', 'telephone number', 'numeric|max_length[15]');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'id' => [
                        'value' => $id
                    ],
                    'username'        => [
                        'value' => set_value('username'),
                        'error' => form_error('username')
                    ],
                    'email'           => [
                        'value' => set_value('email'),
                        'error' => form_error('email')
                    ],
                    'fullname'        => [
                        'value' => set_value('fullname'),
                        'error' => form_error('fullname')
                    ],
                    'alamat'         => [
                        'value' => set_value('alamat'),
                        'error' => form_error('alamat')
                    ],
                    'province'        => [
                        'value' => set_value('province'),
                        'error' => form_error('province')
                    ],
                    'city'            => [
                        'value' => set_value('city'),
                        'error' => form_error('city')
                    ],
                    'telp'            => [
                        'value' => set_value('telp'),
                        'error' => form_error('telp')
                    ],
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/member/form-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'id' => [
                            'value' => $id
                        ],
                        'username'        => [
                            'value' => set_value('username'),
                            'error' => form_error('username')
                        ],
                        'email'           => [
                            'value' => set_value('email'),
                            'error' => form_error('email')
                        ],
                        'fullname'        => [
                            'value' => set_value('fullname'),
                            'error' => form_error('fullname')
                        ],
                        'alamat'         => [
                            'value' => set_value('alamat'),
                            'error' => form_error('alamat')
                        ],
                        'province'        => [
                            'value' => set_value('province'),
                            'error' => form_error('province')
                        ],
                        'city'            => [
                            'value' => set_value('city'),
                            'error' => form_error('city')
                        ],
                        'telp'            => [
                            'value' => set_value('telp'),
                            'error' => form_error('telp')
                        ],
                    ]
                ]);
                redirect(site_url(ADMIN . '/member/' . $id . '/edit'));
            }
        // else if form validation is success
        } else {
            $result = $this->general->edit('z_member', [
                'id' => $id
            ]);
            if ($result->num_rows() === 1) {
                $condition = [
                    'id' => $id
                ];
                $collection = [ // collect data from register-form (true for avoid xss injection)
                    'username'   => $this->input->post('username', true),
                    'fullname'   => $this->input->post('fullname', true),
                    'email'      => $this->input->post('email', true),
                    'province'   => $this->input->post('province', true),
                    'city'       => $this->input->post('city', true),
                    'alamat'     => $this->input->post('alamat', true),
                    'telp'       => $this->input->post('telp', true)
                ];
                // udpate data into table and check if update process is success or not
                // if update process is success
                if ($this->general->update('z_member', $condition, $collection)) {
                    $message = ucwords($this->readable) . ' <strong>' . $result->row_array()['username'] . ' </strong> berhasil diubah.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message
                        ]);
                        redirect(site_url(ADMIN . '/member'));
                    }
                // else if update process is error
                } else {
                    $message = 'Maaf, ' . $this->readable . ' <strong>' . $result->row_array()['username'] . '</strong> gagal diubah. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-danger',
                            'message' => $message,
                            'form_data' => [
                                'username'  => [
                                    'value' => $collection['username']
                                ],
                                'email'     => [
                                    'value' => $collection['email']
                                ],
                                'fullname'  => [
                                    'value' => $collection['fullname']
                                ],
                                'alamat'   => [
                                    'value' => $collection['alamat']
                                ],
                                'province'  => [
                                    'value' => $collection['province']
                                ],
                                'city'      => [
                                    'value' => $collection['city']
                                ],
                                'telp'      => [
                                    'value' => $collection['telp']
                                ]
                            ]
                        ]);
                        redirect(site_url(ADMIN . '/member/' . $id . '/edit'));
                    }
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/member'));
            }
        }
    }

    public function delete($id)
    {
        
        $page['title']      = 'Hapus ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_member', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $member = $result->row_array();
            $message = 'Apakah anda yakin ingin menghapus ' . $this->readable . ' ini : <strong>' . $member['fullname'] . '</strong> ?<br> (*) Proses ini sekaligus akan <strong>menghapus seluruh order</strong> dari user tersebut.';
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/member/delete-ajax', compact('page', 'member', 'message'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/member/delete-default', compact('page', 'member', 'message'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/member'));
        }
    }

    public function destroy($id)
    {
        $result = $this->general->edit('z_member', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            // delete data into table and check if delete process is success or not
            // if delete process is success
            if ($this->general->delete('z_member', ['id' => $id])) {
                $message = ucwords($this->readable) . ' <strong>' . $result->row_array()['fullname'] . '</strong> berhasil dihapus.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status' => true,
                        'notification' => [
                            'type'    => 'success',
                            'message' => $message
                        ]
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-success',
                        'message' => $message
                    ]);
                    redirect(site_url(ADMIN . '/member'));
                }
            // else if insert process is error
            } else {
                $message = 'Maaf, ' . $this->readable . ' <strong>' . $result->row_array()['fullname'] . '</strong> gagal dihapus. Silahkan coba lagi.';
                // check if request are come from ajax or default
                if (USEAJAX) {
                    if (!$this->input->is_ajax_request()) {
                        show_error('Forbidden access', 403);
                    }
                    $response = [
                        'status' => false,
                        'notification' => [
                            'type'    => 'danger',
                            'message' => $message
                        ]
                    ];
                    $this->output->set_content_type('application/json')->set_output(json_encode($response));
                } else {
                    $this->session->set_flashdata([
                        'type'    => 'alert-danger',
                        'message' => $message
                    ]);
                    redirect(site_url(ADMIN . '/member'));
                }
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/member'));
        }
    }

    public function statusGet($id)
    {
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_member', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $member = $result->row_array();
            $page['title'] = ($member['status'] == '0' ? 'Aktivasi' : 'Deaktivasi') . ' ' . ucwords($this->readable);
            $data = [
                'id' => [
                    'value' => $member['id']
                ],
                'status' => [
                    'value' => $member['status']
                ]
            ];
            $message = 'Apakah anda yakin ingin ' . ($member['status'] == '0' ? 'mengaktifkan' : 'menonaktifkan') . ' ' . $this->readable . ' ini : <strong>' . $member['fullname'] . '</strong> ?';
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/member/activate-deactivate-ajax', compact('page', 'data', 'message'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/member/activate-deactivate-default', compact('page', 'data', 'message'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/member'));
        }
    }

    public function statusPost($id)
    {
        $page['title'] = 'Ubah ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('status', 'status', 'required');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'id' => [
                        'value' => $id
                    ],
                    'status' => [
                        'value' => set_value('status'),
                        'error' => form_error('status')
                    ]
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/member/form-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'id' => [
                            'value' => $id
                        ],
                        'status' => [
                            'value' => set_value('status'),
                            'error' => form_error('status')
                        ]
                    ]
                ]);
                redirect(site_url(ADMIN . '/member/' . $id . '/edit'));
            }
        // else if form validation is success
        } else {
            $result = $this->general->edit('z_member', [
                'id' => $id
            ]);
            if ($result->num_rows() === 1) {
                $condition = [
                    'id' => $id
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'status' => $this->input->post('status', true)
                ];
                // udpate data into table and check if update process is success or not
                // if update process is success
                if ($this->general->update('z_member', $condition, $collection)) {
                    $message = ucwords($this->readable) . ' <strong>' . $result->row_array()['fullname'] . ' </strong> berhasil diubah menjadi <strong>' . ($collection['status'] == '1' ? 'Aktif': 'Tidak aktif') . '</strong>.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message
                        ]);
                        redirect(site_url(ADMIN . '/member'));
                    }
                // else if update process is error
                } else {
                    $message = 'Maaf, ' . $this->readable . ' <strong>' . $result->row_array()['fullname'] . '</strong> gagal ' . ($collection['status'] == '1' ? 'diaktifkan': 'dinonaktifkan') . '. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-danger',
                            'message' => $message,
                            'form_data' => [
                                'status'  => [
                                    'value' => $collection['status']
                                ]
                            ]
                        ]);
                        redirect(site_url(ADMIN . '/member/' . $id . '/edit'));
                    }
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/member'));
            }
        }
    }

    public function passwordResetGet($id)
    {
        $page['title'] = 'Reset Password ' . ucwords($this->readable);
        $page['nav-active'] = $this->navActive;

        $result = $this->general->edit('z_member', [
            'id' => $id
        ]);
        if ($result->num_rows() === 1) {
            $member = $result->row_array();
            $data = [
                'id' => [
                    'value' => $member['id']
                ],
                'fullname' => [
                    'value' => $member['fullname']
                ]
            ];
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $response = [
                    'html' => $this->load->view('admin/member/password-reset-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->load->view('admin/member/password-reset-default', compact('page', 'data'));
            }
        } else {
            $this->notFoundData($page, site_url(ADMIN . '/member'));
        }
    }

    public function passwordResetPost($id)
    {
        $page['title'] = 'Reset Password ' . ucwords($this->readable);
        $this->load->library('form_validation');
        // see codeigniter reference rules (required, alpha_dash, etc)
        // in https://www.codeigniter.com/userguide3/libraries/form_validation.html#id32)
        $this->form_validation->set_rules('fullname', 'full name', 'required|max_length[100]');
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('retype-password', 'retype password', 'trim|required|matches[password]');
        // if form validation is fail
        if ($this->form_validation->run() == false) {
            // check if request are come from ajax or default
            if (USEAJAX) {
                if (!$this->input->is_ajax_request()) {
                    show_error('Forbidden access', 403);
                }
                $data = [
                    'id' => [
                        'value' => $id
                    ],
                    'fullname' => [
                        'value' => set_value('fullname'),
                        'error' => form_error('fullname')
                    ],
                    'password'        => [
                        // empty value
                        'error' => form_error('password')
                    ],
                    'retype-password' => [
                        // empty value
                        'error' => form_error('retype-password')
                    ]
                ];
                $response = [
                    'status' => false,
                    'html'   => $this->load->view('admin/member/password-reset-ajax', compact('page', 'data'), true)
                ];
                $this->output->set_content_type('application/json')->set_output(json_encode($response));
            } else {
                $this->session->set_flashdata([ // show error form validation is session/flashdata
                    'form_data' => [
                        'id' => [
                            'value' => $id
                        ],
                        'fullname' => [
                            'value' => set_value('fullname'),
                            'error' => form_error('fullname')
                        ],
                        'password'        => [
                            // empty value
                            'error' => form_error('password')
                        ],
                        'retype-password' => [
                            // empty value
                            'error' => form_error('retype-password')
                        ]
                    ]
                ]);
                redirect(site_url(ADMIN . '/member/' . $id . '/password/reset'));
            }
        // else if form validation is success
        } else {
            $result = $this->general->edit('z_member', [
                'id' => $id
            ]);
            if ($result->num_rows() === 1) {
                $this->load->library('password');
                $condition = [
                    'id' => $id
                ];
                $collection = [ // collect data (true for avoid xss injection)
                    'password' => $this->password->hash($this->input->post('password', true))
                ];
                // udpate data into table and check if update process is success or not
                // if update process is success
                if ($this->general->update('z_member', $condition, $collection)) {
                    $message = 'Proses reset password ' . $this->readable . ' terhadap user <strong>' . $result->row_array()['fullname'] . ' </strong> berhasil dilakukan.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => true,
                            'notification' => [
                                'type'    => 'success',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-success',
                            'message' => $message
                        ]);
                        redirect(site_url(ADMIN . '/member'));
                    }
                // else if update process is error
                } else {
                    $message = 'Maaf, proses reset password ' . $this->readable . ' <strong>' . $result->row_array()['fullname'] . '</strong> gagal dilakukan. Silahkan coba lagi.';
                    // check if request are come from ajax or default
                    if (USEAJAX) {
                        if (!$this->input->is_ajax_request()) {
                            show_error('Forbidden access', 403);
                        }
                        $response = [
                            'status' => false,
                            'notification' => [
                                'type'    => 'danger',
                                'message' => $message
                            ]
                        ];
                        $this->output->set_content_type('application/json')->set_output(json_encode($response));
                    } else {
                        $this->session->set_flashdata([
                            'type'    => 'alert-danger',
                            'message' => $message,
                            'form_data' => [
                                'id' => [
                                    'value' => $id
                                ],
                                'fullname' => [
                                    'value' => set_value('fullname'),
                                    'error' => form_error('fullname')
                                ],
                                'password'        => [
                                    // empty value
                                    'error' => form_error('password')
                                ],
                                'retype-password' => [
                                    // empty value
                                    'error' => form_error('retype-password')
                                ]
                            ]
                        ]);
                        redirect(site_url(ADMIN . '/member/' . $id . '/password/reset'));
                    }
                }
            } else {
                $this->notFoundData($page, site_url(ADMIN . '/member'));
            }
        }
    }

    private function notFoundData($page, $redirect)
    {
        if (USEAJAX) {
            $message = ucwords($this->readable) . ' tidak ditemukan.';
            if (!$this->input->is_ajax_request()) {
                show_error('Forbidden access', 403);
            }
            $response = [
                'html' => $this->load->view('admin/member/error-ajax', compact('page', 'message'), true)
            ];
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        } else {
            $this->session->set_flashdata([
                'type'    => 'alert-warning',
                'message' => ucwords($this->readable) . ' tidak ditemukan.'
            ]);
            redirect($redirect);
        }
    }

}
/* End of file AdminMember.php */
/* Location: ./application/controllers/AdminMember.php */
