<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_datatables extends CI_Model
{

    /**
     * @author [ianrizky] <[ian.rizkypratama@gmail.com]>
     * Datatables Server Mode Query Builder
     */

    public $table         = '';
    public $column_order  = ['id']; // set column field database for datatable orderable
    public $column_search = [null, 'id'];
    // set column field database for datatable searchable
    public $order = [ // default order
        'id' => 'asc'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function mysql_custom_query()
    {
        $this->db->select('*')->from($this->table);
    }

    private function get_datatables_query($order = [], $search = [])
    {
        $this->mysql_custom_query();
        if (!empty($search)) {
            for ($i=0; $i < count($this->column_search); $i++) { 
                // loop column
                if ($search['value']) { // if datatable send GET for search
                    if ($i === 0) { // first loop
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->escape_like_str($this->column_search[$i], $search['value']);
                    } else {
                        $this->db->or_like($this->column_search[$i], $search['value']);
                    }

                    if (count($this->column_search) - 1 == $i) {
                        // last loop
                        $this->db->group_end();
                    } // close bracket
                }
            }
        }

        if (!empty($order)) {
            // here order processing
            $this->db->order_by(
                $this->column_order[$order['0']['column']],
                $order['0']['dir']
            );
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(
                key($order),
                $order[key($order)]
            );
        }
    }

    public function get_datatables($start, $length, $order, $search)
    {
        $this->get_datatables_query($order, $search);
        if ($length != -1) {
            $this->db->limit($length, $start);
        }
        return $this->db->get()->result_array();
    }

    public function count_filtered()
    {
        $this->get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->get_datatables_query();
        return $this->db->count_all_results();
    }

}
/* End of file MY_datatables.php */
/* Location: ./application/model/MY_datatables.php */
