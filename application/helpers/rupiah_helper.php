<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('format_rupiah')) {
    function format_rupiah($value)
    {
    	return 'Rp' . number_format($value, 2, ',', '.');
    }
}
