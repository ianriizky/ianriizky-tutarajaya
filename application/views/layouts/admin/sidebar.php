<aside class="app-sidebar" id="sidebar">
    <div class="sidebar-header">
        <a class="sidebar-brand" href="<?=site_url(ADMIN)?>">
            <span class="highlight">Admin</span> <span style="font-size: 0.8em;"><?=$this->config->item('site_name')?></span>
        </a>
        <button type="button" class="sidebar-toggle">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidebar-menu">
        <ul class="sidebar-nav">
            <li<?=(isset($page['nav-active']) and $page['nav-active'] == 'dashboard') == true ? ' class="active"' : ''?>>
                <a href="<?=site_url(ADMIN)?>">
                    <div class="icon">
                        <i class="fa fa-tasks" aria-hidden="true"></i>
                    </div>
                    <div class="title">Dashboard</div>
                </a>
            </li>
            <li class="dropdown<?=(isset($page['nav-active']) and $page['nav-active'] == 'master') == true ? ' active' : ''?>">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                    <div class="icon">
                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                    </div>
                    <div class="title">Master</div>
                </a>
                <div class="dropdown-menu">
                    <ul>
                        <li class="section">
                            <i class="fa fa-th-list" aria-hidden="true"></i>
                            <a href="<?=site_url(ADMIN . '/category')?>">Kategori</a>
                        </li>
                        <li class="line"></li>
                        <li class="section">
                            <i class="fa fa-sliders" aria-hidden="true"></i>
                            <a href="<?=site_url(ADMIN . '/slider')?>">Slider</a>    
                        </li>
                        <li class="line"></li>
                        <li class="section">
                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                            <a href="<?=site_url(ADMIN . '/product')?>">Produk</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li<?=(isset($page['nav-active']) and $page['nav-active'] == 'transaction') == true ? ' class="active"' : ''?>>
                <a href="<?=site_url(ADMIN . '/transaction')?>">
                    <div class="icon">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                    </div>
                    <div class="title">Transaksi</div>
                </a>
            </li>
            <li<?=(isset($page['nav-active']) and $page['nav-active'] == 'report') == true ? ' class="active"' : ''?>>
                <a href="<?=site_url(ADMIN . '/report')?>">
                    <div class="icon">
                        <i class="fa fa-book" aria-hidden="true"></i>
                    </div>
                    <div class="title">Laporan</div>
                </a>
            </li>
            <li<?=(isset($page['nav-active']) and $page['nav-active'] == 'member') == true ? ' class="active"' : ''?>>
                <a href="<?=site_url(ADMIN . '/member')?>">
                    <div class="icon">
                        <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <div class="title">Member</div>
                </a>
            </li>
        </ul>
    </div>
    <!-- <div class="sidebar-footer">
        <ul class="menu">
            <li>
                <a href="<?=site_url(ADMIN)?>" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                </a>
            </li>
            <li><a href="javascript:void(0);"><span class="flag-icon flag-icon-gb flag-icon-squared"></span></a></li>
            <li><a href="javascript:void(0);"><span class="flag-icon flag-icon-id flag-icon-squared"></span></a></li>
        </ul>
    </div> -->
</aside>