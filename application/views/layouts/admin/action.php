<div class="btn-group">
    <?php if (!empty($actions)): ?>
        <?php foreach ($actions as $action): ?>
            <a href="<?=isset($action['href']) ? $action['href'] : 'javascript:void(0)'?>"<?=isset($action['class']) ? ' class="' . $action['class'] . '"' : ''?>>
                <?=isset($action['title']) ? $action['title'] : 'Action'?>
            </a>
        <?php endforeach?>
    <?php endif?>
</div>