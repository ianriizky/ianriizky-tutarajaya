<nav class="navbar navbar-default" id="navbar">
    <div class="container-fluid">
        <div class="navbar-collapse collapse in">
            <ul class="nav navbar-nav navbar-mobile">
                <li>
                    <button type="button" class="sidebar-toggle">
                        <i class="fa fa-bars"></i>
                    </button>
                </li>
                <li class="logo">
                    <a class="navbar-brand" href="<?=site_url(ADMIN)?>"><span class="highlight">Admin</span> <span style="font-size: 0.8em;"><?=$this->config->item('site_name')?></span></a>
                </li>
                <li>
                    <button type="button" class="navbar-toggle">
                        <img class="profile-img" src="<?=strlen($this->session->userdata('user')['icon']) > 0 ? base_url($this->session->userdata('user')['icon']) : base_url('assets/img/user.jpg')?>">
                    </button>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <li class="navbar-title"><?=isset($page['title']) ? $page['title'] : ''?></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown profile">
                    <a href="<?=site_url(ADMIN . '/profile');?>" class="dropdown-toggle hebi"  data-toggle="dropdown">
                        <img class="profile-img" src="<?=strlen($this->session->userdata('user')['icon']) > 0 ? base_url($this->session->userdata('user')['icon']) : base_url('assets/img/user.jpg')?>">
                        <div class="title">Profil</div>
                    </a>
                    <?php
                        $dropdownMenus = [
                            [
                                'href' => site_url(ADMIN . '/profile'),
                                'text' => 'Profil'
                            ],
                            [
                                'onclick' => 'logout()',
                                'href' => site_url(ADMIN . '/logout'),
                                'text' => 'Logout'
                            ]
                        ]
                    ?>
                    <ul class="action kimi">
                        <?php foreach ($dropdownMenus as $dropdownMenu): ?>
                            <li>
                                <a<?=isset($dropdownMenu['href']) ? ' href="' . $dropdownMenu['href'] . '"' : ' href="javascript:void(0);"'?><?=isset($dropdownMenu['onclick']) ? ' onclick="' . $dropdownMenu['onclick'] . '"' : ''?>>
                                    <?=isset($dropdownMenu['text']) ? $dropdownMenu['text'] : ''?>
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                    <div class="dropdown-menu">
                        <div class="profile-info">
                            <h4 class="username"><?=strlen($this->session->userdata('user')['username']) > 0 ? $this->session->userdata('user')['username'] : '(unknown)'?></h4>
                        </div>
                        <ul class="action">
                            <?php foreach ($dropdownMenus as $dropdownMenu): ?>
                                <li>
                                    <a<?=isset($dropdownMenu['href']) ? ' href="' . $dropdownMenu['href'] . '"' : ' href="javascript:void(0);"'?><?=isset($dropdownMenu['onclick']) ? ' onclick="' . $dropdownMenu['onclick'] . '"' : ''?>>
                                        <?=isset($dropdownMenu['text']) ? $dropdownMenu['text'] : ''?>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<?=form_open(site_url(ADMIN . '/logout'), ['id' => 'logout-form', 'style' => 'diplay: none;'])?>
<?=form_close()?>
<script>
    function logout() {
        event.preventDefault();
        if (confirm ('Apakah anda yakin ingin logout?')) {
            document.getElementById('logout-form').submit();
        }
    }
</script>