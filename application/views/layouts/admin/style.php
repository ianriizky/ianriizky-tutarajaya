<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="<?=$this->config->item('site_name')?>">
<meta name="author" content="<?=$this->config->item('site_name')?>">
<title><?=((isset($page['title']) and strlen($page['title']) > 0) == true ? $page['title'] . ' - ' : 'Admin - ') . $this->config->item('site_name')?></title>
<link rel="stylesheet" href="<?=base_url('assets/css/vendor.min.css')?>">
<link rel="stylesheet" href="<?=base_url('assets/css/flat-admin.min.css')?>">
<link rel="stylesheet" href="<?=base_url('assets/css/animate.min.css')?>">
<!-- Theme -->
<link rel="stylesheet" href="<?=base_url('assets/css/theme/navy.css')?>">
<!-- Set Favicon -->
<link rel="icon" href="<?=base_url('assets/img/favicon.png')?>">