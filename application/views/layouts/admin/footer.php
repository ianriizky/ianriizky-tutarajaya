<footer class="app-footer">
    <div class="row">
        <div class="col-xs-12">
            <div class="footer-copyright">
                Copyright © <?=date('Y') . ' ' . $this->config->item('site_name')?>
            </div>
        </div>
    </div>
</footer>