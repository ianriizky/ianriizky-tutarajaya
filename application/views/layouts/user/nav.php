<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <!-- Navbar Title -->
            <a class="navbar-brand" href="<?=site_url()?>"><i class="fa fa-anchor"></i> <?=$this->config->item('site_name')?></a>
            <!-- /.Navbar Title -->

            <!-- Burger Button Navbar (only show in mobile device) -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- /.Burger Button Navbar -->

            <!-- Navbar Menu -->
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link<?=(isset($page['nav-active']) and $page['nav-active'] == 'home') == true ? ' active' : ''?>" href="<?=site_url()?>"><i class="fa fa-home"></i> Beranda</a>
                    </li>
                    <?php if ($this->session->has_userdata('member')): // check if user has login or not ?>
                        <li class="nav-item dropdown bg-light">
                            <a class="nav-link dropdown-toggle text-body" href="javascript:void(0);" role="button" data-toggle="dropdown">
                                <i class="fa fa-user-circle-o"></i> Hi, <?=isset($this->session->userdata('member')['fullname']) ? (strlen($this->session->userdata('member')['fullname']) > 10 ? substr($this->session->userdata('member')['fullname'], 0, 10): $this->session->userdata('member')['fullname']) : '(unknown)'?>
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item<?=(isset($page['nav-active']) and $page['nav-active'] == 'profile') == true ? ' active' : ''?>" href="<?=site_url('profile')?>"><i class="fa fa-user-circle-o"></i> Profil</a>
                                <a class="dropdown-item<?=(isset($page['nav-active']) and $page['nav-active'] == 'cart') == true ? ' active' : ''?>" href="<?=site_url('cart')?>"><i class="fa fa-shopping-cart"></i> Keranjang</a>
                                <a class="dropdown-item<?=(isset($page['nav-active']) and $page['nav-active'] == 'transaction') == true ? ' active' : ''?>" href="<?=site_url('transaction')?>"><i class="fa fa-shopping-basket"></i> Transaksi</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item<?=(isset($page['nav-active']) and $page['nav-active'] == 'logout') == true ? ' active' : ''?>" href="<?=site_url('logout')?>" onclick="logout()"><i class="fa fa-sign-out"></i> Logout</a>
                                <?=form_open(site_url('logout'), ['id' => 'logout-form', 'style' => 'diplay: none;'])?>
                                <?=form_close()?>
                            </div>
                        </li>
                    <?php else: // guest mode ?>
                        <li class="nav-item">
                            <a class="nav-link<?=(isset($page['nav-active']) and $page['nav-active'] == 'login') == true ? ' active' : ''?>" href="<?=site_url('login')?>"><i class="fa fa-sign-in"></i> Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link<?=(isset($page['nav-active']) and $page['nav-active'] == 'register') == true ? ' active' : ''?>" href="<?=site_url('register')?>"><i class="fa fa-user-plus"></i> Daftar</a>
                        </li>
                    <?php endif?>
                </ul>
            </div>
            <!-- /.Navbar Menu -->
        </div>
    </nav>
</header>
<script>
    function logout() {
        event.preventDefault();
        if (confirm ('Apakah anda yakin ingin logout? Seluruh isi keranjang anda akan terhapus secara otomatis.')) {
            document.getElementById('logout-form').submit();
        }
    }
</script>
