<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="<?=$this->config->item('site_name')?>">
<meta name="author" content="<?=$this->config->item('site_name')?>">
<title><?=((isset($page['title']) and strlen($page['title']) > 0) == true ? $page['title'] . ' - ' : '') . $this->config->item('site_name')?></title>
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?=base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>">
<link rel="stylesheet" href="<?=base_url('assets/plugins/font-awesome/css/font-awesome.min.css')?>">
<link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/datatables.min.css')?>">
<!-- Set Favicon -->
<link rel="icon" href="<?=base_url('assets/img/favicon.png')?>">
<style>
    html, body {
        height: 100%;
        width: 100%;
    }

    article {
        min-height: 100%;
        display: flex;
        flex-direction: column;
        align-items: stretch;
    }

    section.container {
        flex-grow: 1;
    }

    header, section.container, footer {
        flex-shrink: 0;
    }

    footer {
        height: 96px;
    }

    body {
        padding-top: 54px;
    }

    @media (min-width: 992px) {
        body {
            padding-top: 56px;
        }
    }
</style>
