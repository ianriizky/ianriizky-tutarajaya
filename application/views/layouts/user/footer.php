<footer class="footer py-5 bg-dark">
	<div class="container">
		<p class="col-sm-12 m-0 text-center text-white">Copyright &copy; <?=$this->config->item('site_name') . ' ' . date('Y')?></p>
	</div>
</footer>
