<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div style="margin-bottom: 15px;">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>
                    <div class="card">
                        <div class="card-body">
                            <?php if (isset($data['id']['value'])): ?>
                                <?=form_open(ADMIN . '/slider/' . $data['id']['value'], ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <?php else: ?>
                                <?=form_open_multipart(ADMIN . '/slider', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <?php endif?>
                            <!-- Form -->
                            <div class="section">
                                <div class="section-body">
                                    <?php if (empty($data['foto']['value'])): ?>
                                        <!-- First Row -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- foto -->
                                                <div class="form-group<?=!empty($data['foto']['error']) ? ' has-error' : ''?>">
                                                    <label class="col-md-3 col-xs-12 control-label">Foto</label>
                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="input-group" style="border: none;">
                                                            <label class="input-group-btn">
                                                                <span class="btn btn-primary">
                                                                    Cari <input type="file" id="foto" name="foto" style="display: none;">
                                                                </span>
                                                            </label>
                                                            <input type="text" id="image-location" class="form-control" readonly="readonly">
                                                        </div>
                                                        <?php if (!empty($data['foto']['error'])): ?>
                                                            <span class="help-block"><?=$data['foto']['error']?></span>
                                                        <?php endif?>
                                                    </div>
                                                </div>
                                                <!-- /.foto -->
                                            </div>
                                        </div>
                                        <!-- /.First Row -->
                                    <?php endif?>

                                    <!-- Second Row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if (empty($data['foto']['value'])): ?>
                                                <!-- Image Preview (display only) -->
                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-push-3 col-xs-12">
                                                        <img class="img-responsive" id="image-preview" src="<?=base_url('assets/img/slider/noimage.png')?>" alt="Slider image">
                                                    </div>
                                                </div>
                                                <!-- /.Image Preview (display only) -->
                                            <?php else: ?>
                                                <!-- Image Preview (display only) -->
                                                <div class="form-group">
                                                    <label class="col-md-3 col-xs-12 control-label">Foto</label>
                                                    <div class="col-md-6 col-xs-12">
                                                        <img class="img-responsive" id="image-preview" src="<?=base_url($data['foto']['value'])?>" alt="Slider image">
                                                    </div>
                                                </div>
                                                <!-- /.Image Preview (display only) -->
                                            <?php endif?>
                                        </div>
                                    </div>
                                    <!-- /.Second Row -->

                                    <div class="row"><div class="col-md-12"><p>&nbsp;</p></div></div>

                                    <!-- Third Row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- status -->
                                            <div class="form-group<?=!empty($data['status']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <select name="status" id="status" class="form-control">
                                                        <option disabled="disabled"<?=empty($data['status']['value']) ? ' selected="selected"': ''?>>--- Pilih salah satu ---</option>
                                                        <option value="1"<?=(!empty($data['status']['value']) and $data['status']['value'] == '1') == true ? ' selected="selected"': ''?>>Aktif</option>
                                                        <option value="0"<?=(!empty($data['status']['value']) and $data['status']['value'] == '0') == true ? ' selected="selected"': ''?>>Tidak aktif</option>
                                                    </select>
                                                    <?php if (!empty($data['status']['error'])): ?>
                                                        <span class="help-block"><?=$data['status']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.status -->
                                        </div>
                                    </div>
                                    <!-- /.Third Row -->
                                </div>
                            </div>
                            <!-- /.Form -->

                            <!-- Action Button -->
                            <div class="form-footer">
                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3">
                                        <a href="<?=site_url(ADMIN . '/slider')?>" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>
                                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Action Button -->
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    <?php $this->load->view('layouts/admin/script') ?>
    <script type="text/javascript">
        $(document).ready(function () {
            imageHandler("#foto", "#image-location", "#image-preview");
        });
    </script>
</body>
</html>