<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title"><?=isset($page['title']) ? $page['title'] : ''?></h4>
</div>
<?=form_open(ADMIN . '/slider/' . $data['id']['value'] . '/status', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'], [
    'status' => ($data['status']['value'] == '0' ? '1' : '0')
])?>
    <div class="modal-body">
        <!-- Form -->
        <div class="section">
            <div class="section-body">
                <!-- First Row -->
                <div class="row">
                    <div class="col-md-12">
                        <p><?=isset($message) ? $message : ''?></p>
                        <?php if (!empty($data['status']['error'])): ?>
                            <span class="text-danger"><?=$data['status']['error']?></span>
                        <?php endif?>
                    </div>
                    <!-- Image Preview (display only) -->
                    <div class="col-md-12">
                        <img class="img-responsive" id="image-preview" src="<?=base_url($data['foto']['value'])?>" alt="Slider image">
                    </div>
                    <!-- /.Image Preview (display only) -->
                </div>
                <!-- /.First Row -->
            </div>
        </div>
        <!-- /.Form -->
    </div>
    <!-- Action Button -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-undo"></i> Batal</button>
        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ya</button>
    </div>
    <!-- /.Action Button -->
<?=form_close()?>
<script>
    $(document).ready(function () {
        $('form#modal-ajax-form').submit(function () {
            var formElement = $(this);
            var buttonElement = formElement.find('button[type=submit]');
            var buttonHtml = buttonElement.html();
            buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
            activateAjax({
                // response setting
                responsePlace: '#modal-ajax-content',
                // ajax setting
                url: formElement.attr('action'),
                type: formElement.attr('method'),
                data: formElement.serialize(),
                // function after ajax setting
                functionDone: function (data) {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                    // if (data.status) {
                        $('#modal-ajax').modal('hide');
                        table.ajax.reload();
                    // }
                },
                functionFail: function () {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                }
            });

            return false;
        });
    });
</script>