<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title"><?=isset($page['title']) ? $page['title'] : ''?></h4>
</div>
<?php if (!isset($data['id']['value'])): ?>
    <?=form_open_multipart(ADMIN . '/slider', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
<?php else: ?>
    <?=form_open(ADMIN . '/slider/' . $data['id']['value'], ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
<?php endif?>
    <div class="modal-body">
        <!-- Form -->
        <div class="section">
            <div class="section-body">
                <?php if (empty($data['foto']['value'])): ?>
                    <!-- First Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- foto -->
                            <div class="form-group<?=!empty($data['foto']['error']) ? ' has-error' : ''?>">
                                <label class="col-md-3 col-xs-12 control-label">Foto</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group" style="border: none;">
                                        <label class="input-group-btn">
                                            <span class="btn btn-primary">
                                                Cari <input type="file" id="foto" name="foto" style="display: none;">
                                            </span>
                                        </label>
                                        <input type="text" id="image-location" class="form-control" readonly="readonly">
                                    </div>
                                    <?php if (!empty($data['foto']['error'])): ?>
                                        <span class="help-block"><?=$data['foto']['error']?></span>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.foto -->
                        </div>
                    </div>                            
                    <!-- /.First Row -->
                <?php endif?>

                <!-- Second Row -->
                <div class="row">
                    <div class="col-md-12">
                        <?php if (empty($data['foto']['value'])): ?>
                            <!-- Image Preview (display only) -->
                            <div class="form-group">
                                <div class="col-md-9 col-md-push-3 col-xs-12">
                                    <img class="img-responsive" id="image-preview" src="<?=base_url('assets/img/slider/noimage.png')?>" alt="Slider image">
                                </div>
                            </div>
                            <!-- /.Image Preview (display only) -->
                        <?php else: ?>
                            <!-- Image Preview (display only) -->
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Foto</label>
                                <div class="col-md-9 col-xs-12">
                                    <img class="img-responsive" id="image-preview" src="<?=base_url($data['foto']['value'])?>" alt="Slider image">
                                </div>
                            </div>
                            <!-- /.Image Preview (display only) -->
                        <?php endif?>
                    </div>
                </div>
                <!-- /.Second Row -->

                <div class="row"><div class="col-md-12"><p>&nbsp;</p></div></div>

                <!-- Third Row -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- status -->
                        <div class="form-group<?=!empty($data['status']['error']) ? ' has-error' : ''?>">
                            <label class="col-md-3 col-xs-12 control-label">Status</label>
                            <div class="col-md-9 col-xs-12">
                                <select name="status" id="status" class="form-control">
                                    <option disabled="disabled"<?=empty($data['status']['value']) ? ' selected="selected"': ''?>>--- Pilih salah satu ---</option>
                                    <option value="1"<?=(!empty($data['status']['value']) and $data['status']['value'] == '1') == true ? ' selected="selected"': ''?>>Aktif</option>
                                    <option value="0"<?=(!empty($data['status']['value']) and $data['status']['value'] == '0') == true ? ' selected="selected"': ''?>>Tidak aktif</option>
                                </select>
                                <?php if (!empty($data['status']['error'])): ?>
                                    <span class="help-block"><?=$data['status']['error']?></span>
                                <?php endif?>
                            </div>
                        </div>
                        <!-- /.status -->
                    </div>
                </div>
                <!-- /.Third Row -->
            </div>
        </div>
        <!-- /.Form -->
    </div>
    <!-- Action Button -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-undo"></i> Batal</button>
        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
    </div>
    <!-- /.Action Button -->
<?=form_close()?>
<script>
    $(document).ready(function () {
        imageHandler("#foto", "#image-location", "#image-preview");

        $('form#modal-ajax-form').submit(function () {
            var formElement = $(this);
            var buttonElement = formElement.find('button[type=submit]');
            var buttonHtml = buttonElement.html();
            buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
            activateAjax({
                // response setting
                responsePlace: '#modal-ajax-content',
                // ajax setting
                url: formElement.attr('action'),
                type: formElement.attr('method'),
                <?php if (!isset($data['id']['value'])): ?>
                    data: new FormData(formElement[0]),
                    contentType: false,
                    processData: false,
                <?php else: ?>
                    data: formElement.serialize(),
                <?php endif?>
                // function after ajax setting
                functionDone: function (data) {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                    if (data.hasOwnProperty('status') && data.status === true) {
                        $('#modal-ajax').modal('hide');
                        table.ajax.reload();
                    }
                },
                functionFail: function () {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                }
            });

            return false;
        });
    });
</script>