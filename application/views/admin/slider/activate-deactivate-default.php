<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div style="margin-bottom: 15px;">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>
                    <div class="card">
                        <div class="card-body">
                            <?=form_open(ADMIN . '/slider/' . $data['id']['value'] . '/status', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'], [
                                'status' => ($data['status']['value'] == '0' ? '1' : '0')
                            ])?>
                                <!-- Form -->
                                <div class="section">
                                    <div class="section-body">
                                        <!-- First Row -->
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <p><?=isset($message) ? $message : ''?></p>
                                                <?php if (!empty($data['status']['error'])): ?>
                                                    <span class="text-danger"><?=$data['status']['error']?></span>
                                                <?php endif?>
                                            </div>
                                            <!-- Image Preview (display only) -->
                                            <div class="col-md-6 col-xs-12">
                                                <img class="img-responsive" id="image-preview" src="<?=base_url($data['foto']['value'])?>" alt="Slider image">
                                            </div>
                                            <!-- /.Image Preview (display only) -->
                                        </div>
                                        <!-- /.First Row -->
                                    </div>
                                </div>
                                <!-- /.Form -->

                                <!-- Action Button -->
                                <div class="form-footer">
                                    <div class="form-group">
                                        <div class="col-md-9 col-md-offset-3">
                                            <a href="<?=site_url(ADMIN . '/slider')?>" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ya</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.Action Button -->
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    <?php $this->load->view('layouts/admin/script') ?>
</body>
</html>