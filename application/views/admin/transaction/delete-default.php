<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div style="margin-bottom: 15px;">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>
                    <div class="card">
                        <div class="card-body">
                            <?php if (isset($transaction['id'])): ?>
                                <?=form_open(ADMIN . '/transaction/' . $transaction['id'] . '/destroy', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <?php endif?>
                                <!-- Form -->
                                <div class="section">
                                    <div class="section-body">
                                        <!-- First Row -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p><?=isset($message) ? $message : ''?></p>
                                            </div>
                                        </div>
                                        <!-- /.First Row -->
                                    </div>
                                </div>
                                <!-- /.Form -->

                                <!-- Action Button -->
                                <div class="form-footer">
                                    <div class="form-group">
                                        <div class="col-md-9 col-md-offset-3">
                                            <a href="<?=site_url(ADMIN . '/transaction')?>" class="btn btn-success"><i class="fa fa-undo"></i> Batal</a>
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Hapus</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.Action Button -->
                            <?php if (isset($transaction['id'])): ?>
                                <?=form_close()?>
                            <?php endif?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    <?php $this->load->view('layouts/admin/script') ?>
</body>
</html>