<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <!-- alert -->
                    <div style="margin-bottom: 15px;" id="alert-content"<?=$this->session->flashdata('message') == '' ? ' class="hidden"': ''?>>
                        <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->

                    <!-- Content -->
                    <div class="card">
                        <div class="card-header row">
                            <div class="col-xs-12 col-md-3 col-md-push-9 m-0">
                                <p>&nbsp;</p>
                            </div>
                        </div>
                        <!-- Datatables -->
                        <div class="card-body no-padding table-responsive">
                            <table class="table table-striped table-hover primary" cellspacing="0" width="100%" id="table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th>Kode</th>
                                        <th>Nama Penerima</th>
                                        <th>Tanggal</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th style="width: 22%;">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.Datatables -->
                    </div>
                    <!-- /.Content -->
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    </div>

    <!-- Modal Ajax -->
    <div class="modal fade" id="modal-ajax">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-ajax-content">
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.Modal Ajax -->

    <?php $this->load->view('layouts/admin/script') ?>
    <script>
        var table;
        $(document).ready(function () {
            // Activate Datatables (function available on views/layout/admin/script.php)
            table = activateDatatables('table', {
                <?php if (USEAJAX): ?>
                fnDrawCallback: function () {
                    showFormAjax('.action-ajax');
                },
                <?php endif?>
                ajax: {
                    url: '<?=site_url(ADMIN . '/transaction/datatables')?>',
                    type: 'POST'
                },
                columnDefs: [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });

            <?php if (USEAJAX): ?>
            // Create Ajax Function
            function showFormAjax(element) {
                $(element).click(function (event) {
                    var buttonElement = $(this);
                    var buttonHtml = buttonElement.html();
                    buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
                    activateAjax({
                        // response setting
                        responsePlace: '#modal-ajax-content',
                        // ajax setting
                        url: buttonElement.attr('href'),
                        type: 'GET',
                        // function after ajax setting
                        functionDone: function () {
                            $('#modal-ajax').modal({
                                show: true,
                                backdrop: 'static'
                            });
                            $('.modal-dialog').addClass('modal-lg');
                            buttonElement.html(buttonHtml).removeAttr('disabled');
                        },
                        functionFail: function () {
                            buttonElement.html(buttonHtml).removeAttr('disabled');
                        }
                    });

                    return false;
                });
            }

            // Main Function
            showFormAjax('#create-ajax');
            <?php endif?>
        });
    </script>
</body>
</html>