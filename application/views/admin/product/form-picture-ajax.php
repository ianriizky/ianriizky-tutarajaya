<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title"><?=isset($page['title']) ? $page['title'] : ''?></h4>
</div>
<?=form_open_multipart(ADMIN . '/product/' . $data['product_id']['value'] . '/picture', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
    <div class="modal-body">
        <!-- Form -->
        <div class="section">
            <div class="section-body">
                <!-- First Row -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- gambar -->
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Gambar</label>
                            <div class="col-md-9 col-xs-12">
                                <div class="input-group" style="border: none;">
                                    <label class="input-group-btn">
                                        <span class="btn btn-primary">
                                            Cari <input type="file" id="gambar" name="gambar" style="display: none;">
                                        </span>
                                    </label>
                                    <input type="text" id="image-location" class="form-control" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <!-- /.gambar -->
                    </div>
                </div>                            
                <!-- /.First Row -->

                <!-- Second Row -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Image Preview (display only) -->
                        <div class="form-group">
                            <div class="col-md-9 col-md-push-3 col-xs-12">
                                <img class="img-responsive" id="image-preview" src="<?=base_url('assets/img/product/noimage.png')?>" alt="Slider image">
                            </div>
                        </div>
                        <!-- /.Image Preview (display only) -->
                    </div>
                </div>
                <!-- /.Second Row -->
            </div>
        </div>
        <!-- /.Form -->
    </div>
    <!-- Action Button -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-undo"></i> Batal</button>
        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
    </div>
    <!-- /.Action Button -->
<?=form_close()?>
<script>
    $(document).ready(function () {
        imageHandler("#gambar", "#image-location", "#image-preview");

        $('form#modal-ajax-form').submit(function () {
            var formElement = $(this);
            var buttonElement = formElement.find('button[type=submit]');
            var buttonHtml = buttonElement.html();
            buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
            activateAjax({
                // response setting
                responsePlace: '#modal-ajax-content',
                // ajax setting
                url: formElement.attr('action'),
                type: formElement.attr('method'),
                data: new FormData(formElement[0]),
                contentType: false,
                processData: false,
                // function after ajax setting
                functionDone: function (data) {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                    if (data.hasOwnProperty('status') && data.status === true) {
                        $('#modal-ajax').modal('hide');
                        table.ajax.reload();
                    }
                },
                functionFail: function () {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                }
            });

            return false;
        });
    });
</script>