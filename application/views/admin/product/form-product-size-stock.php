<div class="col-md-12">
    <!-- size -->
    <div class="col-xs-5" style="padding-left: 0px;">
        <label class="control-label" style="margin-bottom: 10px;">Size</label>
        <input type="text" class="form-control" name="size[]">
    </div>
    <!-- /.size -->

    <!-- stok -->
    <div class="col-xs-5" style="padding-left: 0px;">
        <label class="control-label" style="margin-bottom: 10px;">Harga</label>
        <div class="input-group">
            <input type="text" class="form-control" value="0" name="stok[]">
            <span class="input-group-addon">pcs</span>
        </div>
    </div>
    <!-- /.stok -->

    <!-- delete button -->
    <div class="col-xs-1">
        <label class="control-label" style="margin-bottom: 10px;">&nbsp;</label>
        <button type="button" class="btn btn-danger delete-field"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </div>
</div>