<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <!-- alert -->
                    <div style="margin-bottom: 15px;" id="alert-content"<?=$this->session->flashdata('message') == '' ? ' class="hidden"': ''?>>
                        <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->

                    <!-- Content -->
                    <div class="card">
                        <div class="card-header row" style="display: block;">
                            <div class="col-xs-12 col-md-4 m-0">
                                <a href="<?=site_url(ADMIN . '/product')?>" class="btn btn-warning btn-block">
                                    <i class="fa fa-chevron-left"></i> Kembali ke Daftar Produk
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-3 col-md-push-5 m-0">
                                <a href="<?=site_url(ADMIN . '/product' . (!empty($data['product_id']['value']) ? '/' . $data['product_id']['value'] : '') . '/picture/create')?>" class="btn btn-navy btn-block" id="create-ajax">
                                    <i class="fa fa-plus"></i> Tambah Gambar
                                </a>
                            </div>
                            <div class="col-xs-12" style="margin-top: 25px;">
                                <p><i>* jumlah gambar yang bisa diupload per produk <b>maksimal hanya <span id="maximum-data-length">5</span> file</b></i></p>
                            </div>
                        </div>
                        <!-- Datatables -->
                        <div class="card-body no-padding table-responsive">
                            <table class="table table-striped table-hover primary" cellspacing="0" width="100%" id="table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th style="width: 40%;">Foto</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.Datatables -->
                    </div>
                    <!-- /.Content -->
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    </div>

    <!-- Modal Ajax -->
    <div class="modal fade" id="modal-ajax">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-ajax-content">
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.Modal Ajax -->

    <?php $this->load->view('layouts/admin/script') ?>
    <script type="text/javascript">
        var table;
        $(document).ready(function () {
            // Activate Datatables (function available on views/layout/admin/script.php)
            table = activateDatatables('table', {
                <?php if (USEAJAX): ?>
                fnDrawCallback: function (data) {
                    showFormAjax('.action-ajax');
                    var jsonDataLength = data.json.data.length;
                    var maximumDataLength = parseInt($('#maximum-data-length').text());
                    if (jsonDataLength > maximumDataLength) {
                        $('#create-ajax').addClass('hidden');
                    } else {
                        $('#create-ajax').removeClass('hidden');
                    }
                },
                <?php endif?>
                ajax: {
                    url: '<?=site_url(ADMIN . '/product/picture/datatables')?>',
                    type: 'POST',
                    data: {
                        product_id: '<?=!empty($data['product_id']['value']) ? $data['product_id']['value'] : ''?>'
                    }
                },
                columnDefs: [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });

            <?php if (USEAJAX): ?>
            // Create Ajax Function
            function showFormAjax(element) {
                $(element).click(function (event) {
                    var buttonElement = $(this);
                    var buttonHtml = buttonElement.html();
                    buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
                    activateAjax({
                        // response setting
                        responsePlace: '#modal-ajax-content',
                        // ajax setting
                        url: buttonElement.attr('href'),
                        type: 'GET',
                        // function after ajax setting
                        functionDone: function () {
                            $('#modal-ajax').modal({
                                show: true,
                                backdrop: 'static'
                            });
                            buttonElement.html(buttonHtml).removeAttr('disabled');
                        },
                        functionFail: function () {
                            buttonElement.html(buttonHtml).removeAttr('disabled');
                        }
                    });

                    return false;
                });
            }

            // Main Function
            showFormAjax('#create-ajax');
            <?php endif?>
        });
    </script>
</body>
</html>