<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title"><?=isset($page['title']) ? $page['title'] : ''?></h4>
</div>
<div class="modal-body">
    <div class="section">
        <div class="section-body">
            <p><?=isset($message) ? $message : ''?></p>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-undo"></i> Kembali</button>
</div>