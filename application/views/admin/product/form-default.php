<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div style="margin-bottom: 15px;">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>
                    <div class="card">
                        <div class="card-body">
                            <?php if (!empty($data['id']['value'])): ?>
                                <?=form_open(ADMIN . '/product/' . $data['id']['value'], ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <?php else: ?>
                                <?=form_open(ADMIN . '/product', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <?php endif?>
                            <!-- Form -->
                            <div class="section">
                                <div class="section-body">
                                    <!-- First Row -->
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <!-- kode -->
                                            <div class="form-group<?=!empty($data['kode']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Kode</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" name="kode" placeholder="Kode produk" readonly="readonly"<?=!empty($data['kode']['value']) ? ' value="' . $data['kode']['value'] . '"': (!empty($preparation['productCode']) ? ' value="' . $preparation['productCode'] . '"': '')?>>
                                                    <?php if (!empty($data['kode']['error'])): ?>
                                                        <span class="help-block"><?=$data['kode']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.kode -->
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <!-- kategori -->
                                            <div class="form-group<?=!empty($data['kategori']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Kategori Produk</label>
                                                <div class="col-md-12">
                                                    <select class="select2" name="kategori" style="width: 100%;">
                                                        <option value="" disabled="disabled"<?=empty($data['kategori']['value']) ? ' selected="selected"': ''?>>--- Pilih salah satu ---</option>
                                                        <?php if (!empty($preparation['kategori'])): ?>
                                                            <?php foreach ($preparation['kategori'] as $kategori): ?>
                                                                <option value="<?=$kategori['id']?>"<?=(!empty($data['kategori']['value']) and $data['kategori']['value'] == $kategori['id']) == true ? ' selected="selected"' : ''?>><?=$kategori['kategori']?></option>
                                                            <?php endforeach?>
                                                        <?php endif?>
                                                    </select>
                                                    <?php if (!empty($data['kategori']['error'])): ?>
                                                        <span class="help-block"><?=$data['kategori']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.kategori -->
                                        </div>
                                    </div>
                                    <!-- /.First Row -->

                                    <!-- Second Row -->
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <!-- nama -->
                                            <div class="form-group<?=!empty($data['nama']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Nama</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" name="nama" placeholder="Nama"<?=!empty($data['nama']['value']) ? ' value="' . $data['nama']['value'] . '"': ''?>>
                                                    <?php if (!empty($data['nama']['error'])): ?>
                                                        <span class="help-block"><?=$data['nama']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.nama -->
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <!-- deskripsi -->
                                            <div class="form-group<?=!empty($data['deskripsi']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Deskripsi</label>
                                                <div class="col-md-12">
                                                    <textarea type="text" class="form-control" name="deskripsi" placeholder="Deskripsi" style="resize: vertical;" rows="4"><?=!empty($data['deskripsi']['value']) ? $data['deskripsi']['value'] : ''?></textarea>
                                                    <?php if (!empty($data['deskripsi']['error'])): ?>
                                                        <span class="help-block"><?=$data['deskripsi']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.deskripsi -->
                                        </div>
                                    </div>
                                    <!-- /.Second Row -->

                                    <!-- Third Row -->
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <!-- harga -->
                                            <div class="form-group<?=!empty($data['harga']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Harga</label>
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Rp</span>
                                                        <input id="harga" type="text" class="form-control" name="harga" placeholder="Harga"<?=isset($data['harga']['value']) ? ' value="' . $data['harga']['value'] . '"': ''?>>
                                                    </div>
                                                    <?php if (!empty($data['harga']['error'])): ?>
                                                        <span class="help-block"><?=$data['harga']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.harga -->
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <!-- diskon -->
                                            <div class="form-group<?=!empty($data['diskon']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Diskon</label>
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="diskon" placeholder="Diskon"<?=isset($data['diskon']['value']) ? ' value="' . $data['diskon']['value'] . '"': ''?>>
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                    <?php if (!empty($data['diskon']['error'])): ?>
                                                        <span class="help-block"><?=$data['diskon']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.diskon -->
                                        </div>
                                    </div>
                                    <!-- /.Third Row -->

                                    <!-- Fourth Row -->
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <!-- status -->
                                            <div class="form-group<?=!empty($data['status']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Status</label>
                                                <div class="col-md-12">
                                                    <select class="form-control" name="status" style="width: 100%;">
                                                        <option disabled="disabled"<?=!isset($data['status']['value']) ? ' selected="selected"': ''?>>--- Pilih salah satu ---</option>
                                                        <option value="0"<?=(isset($data['status']['value']) and $data['status']['value'] == '0') == true ? ' selected="selected"': ''?>>Tidak aktif</option>
                                                        <option value="1"<?=(isset($data['status']['value']) and $data['status']['value'] == '1') == true ? ' selected="selected"': ''?>>Aktif</option>
                                                    </select>
                                                    <?php if (!empty($data['status']['error'])): ?>
                                                        <span class="help-block"><?=$data['status']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.status -->
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <!-- popular -->
                                            <div class="form-group<?=!empty($data['popular']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Populer</label>
                                                <div class="col-md-12">
                                                    <select class="form-control" name="popular" style="width: 100%;">
                                                        <option disabled="disabled"<?=!isset($data['popular']['value']) ? ' selected="selected"': ''?>>--- Pilih salah satu ---</option>
                                                        <option value="0"<?=(isset($data['popular']['value']) and $data['popular']['value'] == '0') == true ? ' selected="selected"': ''?>>Tidak</option>
                                                        <option value="1"<?=(isset($data['popular']['value']) and $data['popular']['value'] == '1') == true ? ' selected="selected"': ''?>>Iya</option>
                                                    </select>
                                                    <?php if (!empty($data['popular']['error'])): ?>
                                                        <span class="help-block"><?=$data['popular']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.popular -->
                                        </div>
                                    </div>
                                    <!-- /.Fourth Row -->

                                    <!-- Fifth Row -->
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <!-- stok -->
                                            <div class="form-group<?=!empty($data['stok']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Stok</label>
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input id="stok" type="text" class="form-control" name="stok" placeholder="Stok"<?=isset($data['stok']['value']) ? ' value="' . $data['stok']['value'] . '"': ''?>>
                                                        <span class="input-group-addon">pcs</span>
                                                    </div>
                                                    <?php if (!empty($data['stok']['error'])): ?>
                                                        <span class="help-block"><?=$data['stok']['error']?></span>
                                                    <?php endif ?>
                                                </div>
                                            </div>
                                            <!-- /.stok -->
                                        </div>
                                    </div>
                                    <!-- /.Fifth Row -->
                                </div>
                            </div>
                            <!-- /.Form -->

                            <!-- Action Button -->
                            <div class="form-footer">
                                <div class="form-group">
                                    <div class="col-md-4 col-md-push-9 col-xs-10 col-xs-push-2">
                                        <a href="<?=site_url(ADMIN . '/product')?>" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>
                                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Action Button -->
                            <?=form_close();?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    <?php $this->load->view('layouts/admin/script') ?>
    <script>
        $(document).ready(function () {
            // $('#harga').number(true, 2);
            $(".select2").select2();
        });
    </script>
</body>
</html>