<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div style="margin-bottom: 15px;">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>
                    <div class="card">
                        <div class="card-body">
                            <?=form_open_multipart(ADMIN . '/product/' . $data['product_id']['value'] . '/picture', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <!-- Form -->
                            <div class="section">
                                <div class="section-body">
                                    <!-- First Row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- gambar -->
                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Gambar</label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group" style="border: none;">
                                                        <label class="input-group-btn">
                                                            <span class="btn btn-primary">
                                                                Cari <input type="file" id="gambar" name="gambar" style="display: none;">
                                                            </span>
                                                        </label>
                                                        <input type="text" id="image-location" class="form-control" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.gambar -->
                                        </div>
                                    </div>                            
                                    <!-- /.First Row -->

                                    <!-- Second Row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Image Preview (display only) -->
                                            <div class="form-group">
                                                <div class="col-md-9 col-md-push-3 col-xs-12">
                                                    <img class="img-responsive" id="image-preview" src="<?=base_url('assets/img/product/noimage.png')?>" alt="Slider image">
                                                </div>
                                            </div>
                                            <!-- /.Image Preview (display only) -->
                                        </div>
                                    </div>
                                    <!-- /.Second Row -->
                                </div>
                            </div>
                            <!-- /.Form -->

                            <!-- Action Button -->
                            <div class="form-footer">
                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3">
                                        <a href="<?=site_url(ADMIN . '/product/' . $data['product_id']['value'] . '/picture')?>" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>
                                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Action Button -->
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    <?php $this->load->view('layouts/admin/script') ?>
    <script>
        $(document).ready(function () {
            imageHandler("#gambar", "#image-location", "#image-preview");
        });
    </script>
</body>
</html>