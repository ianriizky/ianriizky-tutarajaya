<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language');?>">
<head>
    <?php $this->load->view('layouts/admin/style');?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar');?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav');?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <?php if (strlen($this->session->flashdata('message')) > 0): // check if alert is exist or not ?>
                                    <!-- alert -->
                                    <div class="col-xs-12 col-md-12 mt-3">
                                        <div class="alert alert-dismissible<?=strlen($this->session->flashdata('type')) > 0 ? ' ' . $this->session->flashdata('type') : ''?>" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <span><?=$this->session->flashdata('message')?></span>
                                        </div>
                                    </div>
                                    <!-- /.alert -->
                                <?php endif?>
                                <div class="col-xs-12 col-md-12">
                                    <div class="panel-group" id="profile-accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading-update-profile">
                                                <h4 class="panel-title">
                                                    <span role="button" data-toggle="collapse" data-parent="#profile-accordion" href="#update-profile" aria-expanded="true" aria-controls="update-profile">
                                                        <i class="fa fa-user"></i> Profil Saya
                                                    </span>
                                                </h4>
                                            </div>
                                            <div id="update-profile" class="panel-collapse collapse<?=(isset($data['selected-tab']) and $data['selected-tab'] == 'update-profile') == true ? ' in' : ''?>" role="tabpanel" aria-labelledby="heading-update-profile">
                                                <div class="panel-body">
                                                    <!-- My Profile Form -->
                                                    <?=form_open(site_url(ADMIN . '/profile'), ['class' => 'form'], ['selected-tab' => 'update-profile'])?>
                                                        <fieldset id="profile-form"<?=(isset($data['mode-edit']) and $data['mode-edit'] == false) == true ? ' disabled="disabled"' : ''?>>
                                                            <div class="row">
                                                                <!-- Username -->
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="form-group<?=!empty($data['username']['error']) ? ' has-error' : ''?>">
                                                                        <label for="username">Username</label>
                                                                        <input type="text" class="form-control" name="username" id="username" placeholder="Masukkan username"<?=!empty($data['username']['value']) ? ' value="' . $data['username']['value'] . '"': ''?>>
                                                                        <?php if (!empty($data['username']['error'])): ?>
                                                                            <span class="help-block"><?=$data['username']['error']?></span>
                                                                        <?php endif?>
                                                                    </div>
                                                                </div>
                                                                <!-- /.Username -->

                                                                <!-- E-mail address -->
                                                                <div class="col-xs-12 col-md-6">
                                                                    <div class="form-group<?=!empty($data['email']['error']) ? ' has-error' : ''?>">
                                                                        <label for="email">Alamat Email</label>
                                                                        <input type="email" class="form-control" name="email" id="email" placeholder="Masukkan email"<?=!empty($data['email']['value']) ? ' value="' . $data['email']['value'] . '"': ''?>>
                                                                        <?php if (!empty($data['email']['error'])): ?>
                                                                            <span class="help-block"><?=$data['email']['error']?></span>
                                                                        <?php endif?>
                                                                    </div>
                                                                </div>
                                                                <!-- /.E-mail address -->
                                                            </div>
                                                        </fieldset>
                                                        <div class="row mb-5">
                                                            <!-- Action Button -->
                                                            <div class="col-xs-12 col-md-6 col-md-push-6" id="profile-button-action"<?=(isset($data['mode-edit']) and $data['mode-edit'] == false) == true ? ' style="display: none;"' : ''?>>
                                                                <button type="button" class="btn btn-danger col-md-5" id="profile-button-cancel" style="margin-right: 15px;"><i class="fa fa-angle-double-down"></i> Batal</button>
                                                                <button type="submit" class="btn btn-success col-md-6"><i class="fa fa-save"></i> Simpan</button>
                                                            </div>
                                                            <div class="col-xs-12 col-md-6 col-md-push-6" id="profile-button-request"<?=(isset($data['mode-edit']) and $data['mode-edit'] == true) == true ? ' style="display: none;"' : ''?>>
                                                                <button type="button" class="btn btn-info btn-block" id="profile-button-change"><i class="fa fa-angle-double-down"></i> Ubah Profil</button>
                                                            </div>
                                                            <!-- /.Action Button -->
                                                        </div>
                                                    <?=form_close()?>
                                                    <!-- /.My Profile Form -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading-reset-password">
                                                <h4 class="panel-title">
                                                    <span role="button" data-toggle="collapse" data-parent="#profile-accordion" href="#reset-password" aria-expanded="false" aria-controls="reset-password" class="collapsed">
                                                        <i class="fa fa-key"></i> Ganti Password
                                                    </span>
                                                </h4>
                                            </div>
                                            <div id="reset-password" class="panel-collapse collapse<?=(isset($data['selected-tab']) and $data['selected-tab'] == 'reset-password') == true ? ' in' : ''?>" role="tabpanel" aria-labelledby="heading-reset-password">
                                                <div class="panel-body">
                                                    <!-- Change Password Form -->
                                                    <?=form_open(site_url(ADMIN . '/profile'), ['class' => 'form'], ['selected-tab' => 'reset-password'])?>
                                                        <div class="row mt-5 mb-3">
                                                            <!-- Password -->
                                                            <div class="col-xs-12 col-md-6">
                                                                <div class="form-group<?=!empty($data['password']['error']) ? ' has-error' : ''?>">
                                                                    <label for="password">Password</label>
                                                                    <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan password"<?=!empty($data['password']['value']) ? ' value="' . $data['password']['value'] . '"': ''?>>
                                                                    <?php if (!empty($data['password']['error'])): ?>
                                                                        <span class="help-block"><?=$data['password']['error']?></span>
                                                                    <?php endif?>
                                                                </div>
                                                            </div>
                                                            <!-- /.Password -->

                                                            <!-- Retype Password -->
                                                            <div class="col-xs-12 col-md-6">
                                                                <div class="form-group<?=!empty($data['retype-password']['error']) ? ' has-error' : ''?>">
                                                                    <label for="retype-password">Ulangi Password</label>
                                                                    <input type="password" class="form-control" name="retype-password" id="retype-password" placeholder="Masukkan ulang password"<?=!empty($data['retype-password']['value']) ? ' value="' . $data['retype-password']['value'] . '"': ''?>>
                                                                    <?php if (!empty($data['retype-password']['error'])): ?>
                                                                        <span class="help-block"><?=$data['retype-password']['error']?></span>
                                                                    <?php endif?>
                                                                </div>
                                                            </div>
                                                            <!-- /.Retype Password -->
                                                        </div>
                                                        <div class="row">
                                                            <!-- Action Button -->
                                                            <div class="col-xs-12 col-md-12">
                                                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                                                            </div>
                                                            <!-- /.Action Button -->
                                                        </div>
                                                    <?=form_close()?>
                                                    <!-- /.Change Password Form -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer');?>
            <!-- /.Footer -->
        </div>
    </div>
    <?php $this->load->view('layouts/admin/script');?>
    <script src="<?=base_url('assets/plugins/jquery/jquery-ui.min.js')?>"></script>
    <script>
        $(document).ready(function () {
            $('#profile-button-change').click(function () {
                $('#profile-button-request').hide('drop', {direction: 'right'}, 'slow');
                $('#profile-button-action').show('drop', {direction: 'left'}, 'slow');
                $('#profile-form').removeAttr('disabled');
            });
            $('#profile-button-cancel').click(function () {
                $('#profile-button-request').show('drop', {direction: 'left'}, 'slow');
                $('#profile-button-action').hide('drop', {direction: 'right'}, 'slow');
                $('#profile-form').attr('disabled', 'disabled');
            });
        });
    </script>
</body>
</html>
