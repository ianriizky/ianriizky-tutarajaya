<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div style="margin-bottom: 15px;">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>
                    <div class="card">
                        <div class="card-body">
                            <?php if (isset($data['id']['value'])): ?>
                                <?=form_open(ADMIN . '/category/' . $data['id']['value'], ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <?php else: ?>
                                <?=form_open(ADMIN . '/category', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <?php endif?>
                            <!-- Form -->
                            <div class="section">
                                <div class="section-body">
                                    <!-- First Row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- kategori -->
                                            <div class="form-group<?=!empty($data['kategori']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-3 col-xs-12 control-label">Nama Kategori</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control" name="kategori" id="kategori" placeholder="Nama kategori"<?=!empty($data['kategori']['value']) ? ' value="' . $data['kategori']['value'] . '"': ''?>>
                                                    <?php if (!empty($data['kategori']['error'])): ?>
                                                        <span class="help-block"><?=$data['kategori']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.kategori -->
                                        </div>
                                    </div>
                                    <!-- /.First Row -->
                                </div>
                            </div>
                            <!-- /.Form -->

                            <!-- Action Button -->
                            <div class="form-footer">
                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3">
                                        <a href="<?=site_url(ADMIN . '/category')?>" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>
                                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Action Button -->
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    <?php $this->load->view('layouts/admin/script') ?>
</body>
</html>