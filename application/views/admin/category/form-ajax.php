<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title"><?=isset($page['title']) ? $page['title'] : ''?></h4>
</div>
<?php if (isset($data['id']['value'])): ?>
    <?=form_open(ADMIN . '/category/' . $data['id']['value'], ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
<?php else: ?>
    <?=form_open(ADMIN . '/category', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
<?php endif?>
    <div class="modal-body">
        <!-- Form -->
        <div class="section">
            <div class="section-body">
                <!-- First Row -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- kategori -->
                        <div class="form-group<?=!empty($data['kategori']['error']) ? ' has-error' : ''?>">
                            <label class="col-md-3 col-xs-12 control-label">Nama Kategori</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="kategori" id="kategori" placeholder="Nama kategori"<?=!empty($data['kategori']['value']) ? ' value="' . $data['kategori']['value'] . '"': ''?>>
                                <?php if (!empty($data['kategori']['error'])): ?>
                                    <span class="help-block"><?=$data['kategori']['error']?></span>
                                <?php endif?>
                            </div>
                        </div>
                        <!-- /.kategori -->
                    </div>
                </div>
                <!-- /.First Row -->
            </div>
        </div>
        <!-- /.Form -->
    </div>
    <!-- Action Button -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-undo"></i> Batal</button>
        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
    </div>
    <!-- /.Action Button -->
<?=form_close()?>
<script>
    $(document).ready(function () {
        $('form#modal-ajax-form').submit(function () {
            var formElement = $(this);
            var buttonElement = formElement.find('button[type=submit]');
            var buttonHtml = buttonElement.html();
            buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
            activateAjax({
                // response setting
                responsePlace: '#modal-ajax-content',
                // ajax setting
                url: formElement.attr('action'),
                type: formElement.attr('method'),
                data: formElement.serialize(),
                // function after ajax setting
                functionDone: function (data) {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                    if (data.hasOwnProperty('status') && data.status === true) {
                        $('#modal-ajax').modal('hide');
                        table.ajax.reload();
                    }
                },
                functionFail: function () {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                }
            });

            return false;
        });
    });
</script>