<!DOCTYPE html>
<html lang="<?=$this->config->item('language');?>">
<head>
    <?php $this->load->view('layouts/admin/style');?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar');?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav');?>
            <?php if (strlen($this->session->flashdata('message')) > 0): // check if alert is exist or not ?>
                <div class="row">
                    <!-- alert -->
                    <div class="col-xs-12 col-md-12">
                        <div class="alert alert-dismissible<?=strlen($this->session->flashdata('type')) > 0 ? ' ' . $this->session->flashdata('type') : ''?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->
                </div>
            <?php endif?>
            <div class="row">
                <!-- First Row -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <!-- Transaksi hari ini -->
                    <a class="card card-banner card-orange-light">
                        <div class="card-body">
                            <i class="icon fa fa-thumbs-o-up fa-4x"></i>
                            <div class="content">
                                <div class="title" style="font-size: 78%;">Menunggu konfirmasi</div>
                                <div class="value">
                                    <span class="sign"></span>
                                    <?=isset($data['confirmation-total']) ? $data['confirmation-total'] : ''?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- /.Transaksi hari ini -->
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <!-- Total transaksi -->
                    <a class="card card-banner card-blue-light">
                        <div class="card-body">
                            <i class="icon fa fa-shopping-basket fa-4x"></i>
                            <div class="content">
                                <div class="title" style="font-size: 85%;">Transaksi pekan ini</div>
                                <div class="value">
                                    <span class="sign"></span>
                                    <?=isset($data['transaction-week']) ? $data['transaction-week'] : ''?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- /.Total transaksi -->
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <!-- Member baru -->
                    <a class="card card-banner card-yellow-light">
                        <div class="card-body">
                            <i class="icon fa fa-line-chart fa-4x"></i>
                            <div class="content">
                                <div class="title">Total transaksi</div>
                                <div class="value">
                                    <span class="sign"></span>
                                    <?=isset($data['transaction-total']) ? $data['transaction-total'] : ''?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- /.Member baru -->
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <!-- Member baru -->
                    <a class="card card-banner card-green-light">
                        <div class="card-body">
                            <i class="icon fa fa-users fa-4x"></i>
                            <div class="content">
                                <div class="title">Total member</div>
                                <div class="value">
                                    <span class="sign"></span>
                                    <?=isset($data['member-total']) ? $data['member-total'] : ''?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!-- /.Member baru -->
                </div>
                <!-- /.First Row -->

                <!-- Second Row -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-mini">
                        <div class="card-header">
                            <div class="card-title">Transaksi Pekan Ini</div>
                            <ul class="card-action">
                                <li>
                                    <a href="<?=site_url(ADMIN . '/transaction')?>" style="color: #444;">
                                        <i class="fa fa-long-arrow-right"></i> Lihat Daftar Transaksi
                                    </a>
                                    &nbsp;
                                    <a href="<?=site_url(ADMIN . '/report')?>" style="color: #444;">
                                        <i class="fa fa-long-arrow-right"></i> Lihat Laporan
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body no-padding table-responsive">
                            <table class="table card-table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th>Kode</th>
                                        <th>Nama Penerima</th>
                                        <th>Tanggal</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th style="width: 17%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($data['transaction-list'])): ?>
                                        <?php foreach ($data['transaction-list'] as $index => $transaction): ?>
                                            <tr>
                                                <td><?=($index + 1)?></td>
                                                <td><?=$transaction['kode']?></td>
                                                <td><?=$transaction['nama_penerima']?></td>
                                                <td><?=date('j F Y H.i.s', strtotime($transaction['created_at']))?></td>
                                                <td><?=format_rupiah($transaction['total'])?></td>
                                                <td>
                                                    <span class="badge <?=$transaction['status_badge']?> badge-icon">
                                                        <i class="fa fa-check" aria-hidden="true"></i><span><?=$transaction['status_text']?></span>
                                                    </span>
                                                </td>
                                                <td>
                                                    <a href="<?=site_url(ADMIN . '/transaction/' . $transaction['id'])?>" class="badge badge-success badge-icon">
                                                        <i class="fa fa-bookmark" aria-hidden="true"></i><span>Lihat</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="7" class="text-center">Tidak ada transaksi</td>
                                        </tr>
                                    <?php endif?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.Second Row -->
            </div>
            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer');?>
            <!-- /.Footer -->
        </div>
    </div>
    <?php $this->load->view('layouts/admin/script');?>
</body>
</html>