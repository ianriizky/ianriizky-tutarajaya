<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : [];
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language');?>">
<head>
   <?php $this->load->view('layouts/admin/style');?>
</head>
<body>
    <div class="app app-navy">
        <div class="app-container app-logins-navy">
            <div class="flex-center" style="min-height: 100vh; margin-top: 0px;">
                <div class="app-body">
                    <div class="app-block">
                        <div class="app-form">
                            <div class="form-header">
                                <div class="app-brand text-center">
                                    <span class="highlight"><?=isset($page['title']) ? $page['title'] : 'Admin';?></span> <a href="<?=site_url();?>" style="color: #0A1123;"><?=$this->config->item('site_name');?></a>
                                </div>
                            </div>
                            <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                                <!-- alert -->
                                <div style="margin-bottom: 15px;">
                                    <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning';?>" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <span><?=$this->session->flashdata('message');?></span>
                                    </div>
                                </div>
                                <!-- /.alert -->
                            <?php endif;?>

                            <!-- Login Form -->
                            <?=form_open(site_url(ADMIN . '/login'), '', [
                                'redirect' => htmlspecialchars(strip_tags($this->input->get('redirect')))
                            ])?>
                                <!-- Email Address -->
                                <div class="input-group">
                                    <span class="input-group-addon" id="email">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </span>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Username/email"<?=isset($data['email']['value']) ? 'value="' . $data['email']['value'] . '"': '';?>>
                                </div>
                                <?=isset($data['email']['error']) ? '<small class="form-text text-danger">' . $data['email']['error'] . '</small>' : '';?>
                                <!-- /.Email Address -->

                                <!-- Password -->
                                <div class="input-group">
                                    <span class="input-group-addon" id="pass">
                                        <i class="fa fa-key" aria-hidden="true"></i>
                                    </span>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password"<?=isset($data['password']['value']) ? 'value="' . $data['password']['value'] . '"': '';?>>
                                </div>
                                <?=isset($data['password']['error']) ? '<small class="form-text text-danger">' . $data['password']['error'] . '</small>' : '';?>
                                <!-- /.Password -->

                                <!-- Submit Button -->
                                <div class="text-center">
                                    <input type="submit" class="btn btn-navy btn-submit" value="Login">
                                </div>
                                <!-- /.Submit Button -->

                                <!-- Forgot Password -->
                                <!-- <div style="margin-top: 0.5em;">
                                    <p class="text-center">
                                        <a href="<?=site_url(ADMIN . '/password/reset');?>">Lupa password ?</a>
                                    </p>
                                </div> -->
                                <!-- /.Forgot Password -->
                            <?=form_close();?>
                            <!-- /.Login Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('layouts/admin/script');?>
</body>
</html>