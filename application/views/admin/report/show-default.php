<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div style="margin-bottom: 15px;">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>
                    <div class="card">
                        <div class="card-body form form-horizontal">
                            <!-- Show -->
                            <div class="section">
                                <div class="section-body">
                                    <!-- First Row -->
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <h4>Informasi Pembeli</h4>
                                        </div>
                                        <!-- Nama Penerima -->
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Nama Penerima</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" id="nama_penerima" value="<?=$transaction['nama_penerima']?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.Nama Penerima -->

                                        <div class="col-xs-12 col-md-6">
                                            <div class="row">
                                                <!-- City -->
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label" style="margin-bottom: 10px;">Kota</label>
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control" id="city" value="<?=$member['city']?>" readonly="readonly">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.City -->

                                                <!-- Province -->
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label" style="margin-bottom: 10px;">Provinsi</label>
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control" id="province" value="<?=$member['province']?>" readonly="readonly">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.Province -->
                                            </div>
                                        </div>

                                        <!-- Alamat -->
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Alamat</label>
                                                <div class="col-md-12">
                                                    <textarea class="form-control" id="alamat" rows="5" style="resize: vertical;" readonly="readonly"><?=$member['alamat']?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.Alamat -->

                                        <!-- Telephone -->
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Nomor Telepon Penerima</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" id="no_telp" value="<?=$transaction['no_telp']?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.Telephone -->

                                        <!-- Catatan -->
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Catatan</label>
                                                <div class="col-md-12">
                                                    <textarea class="form-control" id="catatan" rows="5" style="resize: vertical;" readonly="readonly"><?=$transaction['catatan']?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.Catatan -->
                                    </div>
                                    <!-- /.First Row -->

                                    <!-- Second Row -->
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <h4>Bukti Transaksi</h4>
                                        </div>

                                        <?php if (!empty($transaction['bukti_transfer'])): ?>
                                            <?php @file_get_contents(base_url(explode('|', $transaction['bukti_transfer'])[0])) ?>
                                            <?php if (strpos($http_response_header[0], '200') !== false): ?>
                                                <div class="col-xs-12 col-md-6">
                                                    <img src="<?=base_url(explode('|', $transaction['bukti_transfer'])[0])?>" class="img-responsive" alt="<?=$transaction['kode']?>">
                                                </div>
                                            <?php else: ?>
                                                <div class="col-xs-12 col-md-12">
                                                    <p>
                                                        Transaksi ini belum disertai dengan bukti transaksi. Silahkan hubungi member melalui e-mail di <strong><?=$member['email']?></strong> untuk proses transaksi selanjutnya.
                                                        <br>
                                                        Link upload bukti transaksi : <a href="<?=site_url('transaction/' . explode('|', $transaction['bukti_transfer'])[0] . '/upload')?>" target="_blank"><?=site_url('transaction/' . explode('|', $transaction['bukti_transfer'])[0] . '/upload')?></a>
                                                    </p>
                                                </div>
                                            <?php endif?>
                                        <?php else: ?>
                                            <div class="col-xs-12 col-md-12">
                                                <p>
                                                    Ada kesalahan dalam proses upload bukti transaksi. Silahkan hubungi member melalui e-mail di <strong><?=$member['email']?></strong> untuk proses transaksi selanjutnya.
                                                    <br>
                                                    Link upload bukti transaksi : <a href="<?=site_url('transaction/' . explode('|', $transaction['bukti_transfer'])[0] . '/upload')?>" target="_blank"><?=site_url('transaction/' . explode('|', $transaction['bukti_transfer'])[0] . '/upload')?></a>
                                                </p>
                                            </div>
                                        <?php endif?>
                                    </div>
                                    <!-- /.Second Row -->

                                    <!-- Third Row -->
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <h4>Detail Transaksi</h4>
                                        </div>

                                        <div class="col-xs-12 col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover primary">
                                                    <!-- Cart Header -->
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 5%;">No</th>
                                                            <th style="width: 15%;">Nama Produk</th>
                                                            <th style="width: 15%;" class="text-right">Jumlah</th>
                                                            <th class="text-right">Harga</th>
                                                            <th class="text-right">Subtotal</th>
                                                        </tr>
                                                    </thead>
                                                    <!-- /.Cart Header -->

                                                    <!-- Cart Body -->
                                                    <tbody>
                                                        <?php foreach ($transactionDetails as $i => $transactionDetail): ?>
                                                            <tr>
                                                                <td>
                                                                    <?=($i + 1)?>
                                                                </td>
                                                                <td>
                                                                    <?=$transactionDetail['p_nama']?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?=$transactionDetail['dt_jumlah']?> pcs
                                                                </td>
                                                                <td class="text-right">
                                                                    <?=format_rupiah($transactionDetail['dt_harga'])?>
                                                                </td>
                                                                <td class="text-right">
                                                                    <?=format_rupiah($transactionDetail['dt_subtotal'])?>
                                                                </td>
                                                            </tr>
                                                            <?php $i++; ?>
                                                        <?php endforeach?>
                                                    </tbody>
                                                    <!-- /.Cart Body -->

                                                    <?php if (!empty($transactionDetails)): ?>
                                                        <!-- Cart Footer -->
                                                        <tbody>
                                                            <tr>
                                                                <th colspan="4" class="text-right">Total Bayar</th>
                                                                <th class="text-right">
                                                                    <?=format_rupiah($transaction['total'])?>
                                                                </th>
                                                            </tr>
                                                        </tbody>
                                                        <!-- /.Cart Footer -->
                                                    <?php endif ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.Third Row -->
                                </div>
                            </div>
                            <!-- /.Show -->

                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    <?php $this->load->view('layouts/admin/script') ?>
    <script>
        $(document).ready(function () {
            $('#status').change(function () {
                $('#change-status').submit();
            });
        });
    </script>
</body>
</html>