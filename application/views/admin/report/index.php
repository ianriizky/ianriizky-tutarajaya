<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <!-- alert -->
                    <div style="margin-bottom: 15px;" id="alert-content"<?=$this->session->flashdata('message') == '' ? ' class="hidden"': ''?>>
                        <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->

                    <!-- Content -->
                    <div class="card">
                        <div class="card-header row" style="display: block;">
                            <div class="col-xs-12 col-md-6">
                                <input type="text" id="daterange-trigger" class="form-control" placeholder="Tanggal Transaksi">
                                <input type="hidden" id="daterange-startdate-value">
                                <input type="hidden" id="daterange-enddate-value">
                            </div>
                            <?php if (!empty($listStatus)): ?>
                                <div class="col-xs-12 col-md-3">
                                    <select id="status" class="form-control select2" style="width: 100%;">
                                        <option disabled="disabled" selected="selected">-- Status Transaksi --</option>
                                        <option value="-1">Semua</option>
                                        <?php foreach ($listStatus as $status): ?>
                                            <option value="<?=$status['code']?>"><?=$status['text']?></option>
                                        <?php endforeach?>
                                    </select>
                                </div>
                            <?php endif?>
                            <div class="col-xs-12 col-md-3">
                                <button type="button" id="datatables-filter-button" class="btn btn-navy btn-block">
                                    Cari
                                </button>
                            </div>
                        </div>
                        <!-- Datatables -->
                        <div class="card-body no-padding table-responsive">
                            <table class="table table-striped table-hover primary" cellspacing="0" width="100%" id="table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">No</th>
                                        <th>Kode</th>
                                        <th>Nama Penerima</th>
                                        <th>Tanggal</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th style="width: 17%;">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.Datatables -->
                    </div>
                    <!-- /.Content -->
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    </div>

    <!-- Modal Ajax -->
    <div class="modal fade" id="modal-ajax">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-ajax-content">
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.Modal Ajax -->

    <?php $this->load->view('layouts/admin/script') ?>
    <script src="<?=base_url('assets/js/moment.min.js')?>"></script>
    <script src="<?=base_url('assets/js/daterangepicker.min.js')?>"></script>
    <link rel="stylesheet" href="<?=base_url('assets/css/daterangepicker.min.css')?>">
    <script>
        var table;
        // ---
        var status;
        var start_date;
        var end_date;
        $(document).ready(function () {
            // Activate Datatables (function available on views/layout/admin/script.php)
            table = activateDatatables('table', {
                <?php if (USEAJAX): ?>
                fnDrawCallback: function () {
                    showFormAjax('.action-ajax');
                },
                <?php endif?>
                ajax: {
                    url: '<?=site_url(ADMIN . '/report/datatables')?>',
                    type: 'POST',
                    data: function (data) {
                        data.status = $('#status').val();
                        data.start_date = $('#daterange-startdate-value').val();
                        data.end_date = $('#daterange-enddate-value').val();
                    }
                },
                columnDefs: [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });

            // daterangepicker
            var daterange = $('#daterange-trigger');
            daterange.daterangepicker({
                locale: {
                    format: 'DD MMMM YYYY',
                    separator: ' - ',
                    firstDay: 0,
                },
                autoUpdateInput : false,
                linkedCalendars: false,
                startDate: new Date(),
                endDate: new Date((new Date()).valueOf() + 1000 * 3600 * 24),
                // minDate: new Date(),
                opens: 'right'
            });
            daterange.on("apply.daterangepicker", function(ev, picker) {
                daterange.val(picker.startDate.format('DD MMMM YYYY') + ' - ' + picker.endDate.format('DD MMMM YYYY'));
                $('#daterange-startdate-value').val(picker.startDate.format('YYYY-MM-DD'));
                $('#daterange-enddate-value').val(picker.endDate.format('YYYY-MM-DD'));
            });
            daterange.on("cancel.daterangepicker", function(ev, picker) {
                daterange.val(daterange.attr("placeholder"));
                $('#daterange-startdate-value').val('');
                $('#daterange-enddate-value').val('');
            });
            $('#datatables-filter-button').click(function (argument) {
                table.ajax.reload();
            });

            <?php if (USEAJAX): ?>
            // Create Ajax Function
            function showFormAjax(element) {
                $(element).click(function (event) {
                    var buttonElement = $(this);
                    var buttonHtml = buttonElement.html();
                    buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
                    activateAjax({
                        // response setting
                        responsePlace: '#modal-ajax-content',
                        // ajax setting
                        url: buttonElement.attr('href'),
                        type: 'GET',
                        // function after ajax setting
                        functionDone: function () {
                            $('#modal-ajax').modal({
                                show: true,
                                backdrop: 'static'
                            });
                            $('.modal-dialog').addClass('modal-lg');
                            buttonElement.html(buttonHtml).removeAttr('disabled');
                        },
                        functionFail: function () {
                            buttonElement.html(buttonHtml).removeAttr('disabled');
                        }
                    });

                    return false;
                });
            }

            // Main Function
            showFormAjax('#create-ajax');
            <?php endif?>
        });
    </script>
</body>
</html>