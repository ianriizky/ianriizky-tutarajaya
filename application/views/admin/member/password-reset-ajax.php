<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title"><?=isset($page['title']) ? $page['title'] : ''?></h4>
</div>
<?php if (isset($data['id']['value'])): ?>
    <?=form_open(ADMIN . '/member/' . $data['id']['value'] . '/password/reset', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
<?php endif?>
    <div class="modal-body">
        <!-- Form -->
        <div class="section">
            <div class="section-body">
                <!-- First Row -->
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- fullnamemember -->
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Nama member</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Masukkan ulang password"<?=!empty($data['fullname']['value']) ? ' value="' . $data['fullname']['value'] . '"': ''?> readonly="readonly">
                                </div>
                            </div>
                            <!-- /.fullnamemember -->
                        </div>
                    </div>
                    <!-- /.First Row -->

                    <!-- Second Row -->
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- password -->
                            <div class="form-group<?=!empty($data['password']['error']) ? ' has-error' : ''?>">
                                <label class="col-md-3 col-xs-12 control-label">Password baru</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan password baru"<?=!empty($data['password']['value']) ? ' value="' . $data['password']['value'] . '"': ''?>>
                                    <?php if (!empty($data['password']['error'])): ?>
                                        <span class="help-block"><?=$data['password']['error']?></span>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.password -->
                        </div>
                    </div>
                    <!-- /.Second Row -->

                    <!-- Third Row -->
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- retype-password -->
                            <div class="form-group<?=!empty($data['retype-password']['error']) ? ' has-error' : ''?>">
                                <label class="col-md-3 col-xs-12 control-label">Password baru</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="password" class="form-control" name="retype-password" id="retype-password" placeholder="Masukkan ulang password"<?=!empty($data['retype-password']['value']) ? ' value="' . $data['retype-password']['value'] . '"': ''?>>
                                    <?php if (!empty($data['retype-password']['error'])): ?>
                                        <span class="help-block"><?=$data['retype-password']['error']?></span>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.retype-password -->
                        </div>
                    </div>
                    <!-- /.Third Row -->
            </div>
        </div>
        <!-- /.Form -->
    </div>
    <!-- Action Button -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-undo"></i> Batal</button>
        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
    </div>
    <!-- /.Action Button -->
<?php if (isset($data['id']['value'])): ?>
    <?=form_close()?>
<?php endif?>
<script>
    $(document).ready(function () {
        $('form#modal-ajax-form').submit(function () {
            var formElement = $(this);
            var buttonElement = formElement.find('button[type=submit]');
            var buttonHtml = buttonElement.html();
            buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
            activateAjax({
                // response setting
                responsePlace: '#modal-ajax-content',
                showNotification: 'notify', // notify / alert / none
                // ajax setting
                url: formElement.attr('action'),
                type: formElement.attr('method'),
                data: formElement.serialize(),
                // function after ajax setting
                functionDone: function (data) {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                    if (data.hasOwnProperty('status') && data.status === true) {
                        $('#modal-ajax').modal('hide');
                        table.ajax.reload();
                    }
                },
                functionFail: function () {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                }
            });

            return false;
        });
    });
</script>