<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/admin/style') ?>
</head>
<body>
    <div class="app app-navy">
        <?php $this->load->view('layouts/admin/sidebar') ?>
        <div class="app-container">
            <?php $this->load->view('layouts/admin/nav') ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div style="margin-bottom: 15px;">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>
                    <div class="card">
                        <div class="card-body">
                            <?php if (isset($data['id']['value'])): ?>
                                <?=form_open(ADMIN . '/member/' . $data['id']['value'] . '/password/reset', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
                            <?php endif?>
                            <!-- Form -->
                            <div class="section">
                                <div class="section-body">
                                    <!-- First Row -->
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <!-- fullname (readonly) -->
                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Nama member</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Masukkan ulang password"<?=!empty($data['fullname']['value']) ? ' value="' . $data['fullname']['value'] . '"': ''?> readonly="readonly">
                                                </div>
                                            </div>
                                            <!-- /.fullname (readonly) -->
                                        </div>
                                    </div>
                                    <!-- /.First Row -->

                                    <!-- Second Row -->
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <!-- password -->
                                            <div class="form-group<?=!empty($data['password']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-3 col-xs-12 control-label">Password baru</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan password baru"<?=!empty($data['password']['value']) ? ' value="' . $data['password']['value'] . '"': ''?>>
                                                    <?php if (!empty($data['password']['error'])): ?>
                                                        <span class="help-block"><?=$data['password']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.password -->
                                        </div>
                                    </div>
                                    <!-- /.Second Row -->

                                    <!-- Third Row -->
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <!-- retype-password -->
                                            <div class="form-group<?=!empty($data['retype-password']['error']) ? ' has-error' : ''?>">
                                                <label class="col-md-3 col-xs-12 control-label">Password baru</label>
                                                <div class="col-md-6 col-xs-12">
                                                    <input type="password" class="form-control" name="retype-password" id="retype-password" placeholder="Masukkan ulang password"<?=!empty($data['retype-password']['value']) ? ' value="' . $data['retype-password']['value'] . '"': ''?>>
                                                    <?php if (!empty($data['retype-password']['error'])): ?>
                                                        <span class="help-block"><?=$data['retype-password']['error']?></span>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.retype-password -->
                                        </div>
                                    </div>
                                    <!-- /.Third Row -->
                                </div>
                            </div>
                            <!-- /.Form -->

                            <!-- Action Button -->
                            <div class="form-footer">
                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3">
                                        <a href="<?=site_url(ADMIN . '/category')?>" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</a>
                                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Action Button -->
                            <?php if (isset($data['id']['value'])): ?>
                                <?=form_close()?>
                            <?php endif?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('layouts/admin/footer') ?>
            <!-- /.Footer -->
        </div>
    <?php $this->load->view('layouts/admin/script') ?>
</body>
</html>