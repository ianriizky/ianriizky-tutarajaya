<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title"><?=isset($page['title']) ? $page['title'] : ''?></h4>
</div>
<?php if (isset($data['id']['value'])): ?>
    <?=form_open(ADMIN . '/member/' . $data['id']['value'], ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
<?php else: ?>
    <?=form_open(ADMIN . '/member', ['class' => 'form form-horizontal', 'id' => 'modal-ajax-form'])?>
<?php endif?>
    <div class="modal-body">
        <!-- Form -->
        <div class="section">
            <div class="section-body">
                <!-- First Row -->
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <!-- Fullname -->
                        <div class="form-group<?=!empty($data['fullname']['error']) ? ' has-error' : ''?>">
                            <label class="col-md-12 control-label" style="margin-bottom: 10px;">Nama Lengkap</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="fullname" placeholder="Masukkan nama lengkap"<?=!empty($data['fullname']['value']) ? ' value="' . $data['fullname']['value'] . '"': ''?>>
                                <?php if (!empty($data['fullname']['error'])): ?>
                                    <span class="help-block"><?=$data['fullname']['error']?></span>
                                <?php endif?>
                            </div>
                        </div>
                        <!-- /.Fullname -->
                    </div>
                </div>
                <!-- /.First Row -->
                    
                <!-- Second Row -->
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <!-- Username -->
                        <div class="form-group<?=!empty($data['username']['error']) ? ' has-error' : ''?>">
                            <label class="col-md-12 control-label" style="margin-bottom: 10px;">Username</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="username" placeholder="Masukkan username"<?=!empty($data['username']['value']) ? ' value="' . $data['username']['value'] . '"': ''?>>
                                <?php if (!empty($data['username']['error'])): ?>
                                    <span class="help-block"><?=$data['username']['error']?></span>
                                <?php endif?>
                            </div>
                        </div>
                        <!-- /.Username -->
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <!-- E-mail address -->
                        <div class="form-group<?=!empty($data['email']['error']) ? ' has-error' : ''?>">
                            <label class="col-md-12 control-label" style="margin-bottom: 10px;">Alamat Email</label>
                            <div class="col-md-12">
                                <input type="email" class="form-control" name="email" placeholder="Masukkan email"<?=!empty($data['email']['value']) ? ' value="' . $data['email']['value'] . '"': ''?>>
                                <?php if (!empty($data['email']['error'])): ?>
                                    <span class="help-block"><?=$data['email']['error']?></span>
                                <?php endif?>
                            </div>
                        </div>
                        <!-- /.E-mail address -->
                    </div>
                </div>
                <!-- /.Second Row -->
                    
                <?php if (!isset($data['id']['value'])): ?>
                    <!-- Third Row -->
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <!-- Password -->
                            <div class="form-group<?=!empty($data['password']['error']) ? ' has-error' : ''?>">
                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" name="password" placeholder="Masukkan password"<?=!empty($data['password']['value']) ? ' value="' . $data['password']['value'] . '"': ''?>>
                                    <?php if (!empty($data['password']['error'])): ?>
                                        <span class="help-block"><?=$data['password']['error']?></span>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.Password -->
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <!-- Retype Password -->
                            <div class="form-group<?=!empty($data['retype-password']['error']) ? ' has-error' : ''?>">
                                <label class="col-md-12 control-label" style="margin-bottom: 10px;">Ulangi Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" name="retype-password" placeholder="Masukkan password"<?=!empty($data['retype-password']['value']) ? ' value="' . $data['retype-password']['value'] . '"': ''?>>
                                    <?php if (!empty($data['retype-password']['error'])): ?>
                                        <span class="help-block"><?=$data['retype-password']['error']?></span>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.Retype Password -->
                        </div>
                    </div>
                    <!-- /.Third Row -->
                <?php endif?>

                <!-- Fourth Row -->
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <!-- Alamat -->
                        <div class="form-group<?=!empty($data['address']['error']) ? ' has-error' : ''?>">
                            <label class="col-md-12 control-label" style="margin-bottom: 10px;">Alamat</label>
                            <div class="col-md-12">
                                <textarea class="form-control" name="address" id="address" rows="5" placeholder="Masukan alamat" style="resize: vertical;"><?=!empty($data['address']['value']) ? $data['address']['value'] : ''?></textarea>
                                <?php if (!empty($data['address']['error'])): ?>
                                    <div class="invalid-feedback"><?=$data['address']['error']?></div>
                                <?php endif?>
                            </div>
                        </div>
                        <!-- /.Alamat -->
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <!-- City -->
                                <div class="form-group<?=!empty($data['city']['error']) ? ' has-error' : ''?>">
                                    <label class="col-md-12 control-label" style="margin-bottom: 10px;">Kota</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="city" placeholder="Masukkan kota"<?=!empty($data['city']['value']) ? ' value="' . $data['city']['value'] . '"': ''?>>
                                        <?php if (!empty($data['city']['error'])): ?>
                                            <div class="invalid-feedback"><?=$data['city']['error']?></div>
                                        <?php endif?>
                                    </div>
                                </div>
                                <!-- /.City -->
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <!-- Province -->
                                <div class="form-group<?=!empty($data['province']['error']) ? ' has-error' : ''?>">
                                    <label class="col-md-12 control-label" style="margin-bottom: 10px;">Provinsi</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="province" placeholder="Masukkan provinsi"<?=!empty($data['province']['value']) ? ' value="' . $data['province']['value'] . '"': ''?>>
                                        <?php if (!empty($data['province']['error'])): ?>
                                            <div class="invalid-feedback"><?=$data['province']['error']?></div>
                                        <?php endif?>
                                    </div>
                                </div>
                                <!-- /.Province -->
                            </div>
                            <div class="col-xs-12 col-md-12">
                                <!-- Telephone -->
                                <div class="form-group<?=!empty($data['telp']['error']) ? ' has-error' : ''?>">
                                    <label class="col-md-12 control-label" style="margin-bottom: 10px;">Nomor Telepon</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="telp" placeholder="Masukkan nomor telepon"<?=!empty($data['telp']['value']) ? ' value="' . $data['telp']['value'] . '"': ''?>>
                                        <?php if (!empty($data['telp']['error'])): ?>
                                            <div class="invalid-feedback"><?=$data['telp']['error']?></div>
                                        <?php endif?>
                                    </div>
                                </div>
                                <!-- /.Telephone -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.Fourth Row -->
            </div>
        </div>
        <!-- /.Form -->
    </div>
    <!-- Action Button -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-undo"></i> Batal</button>
        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
    </div>
    <!-- /.Action Button -->
<?=form_close()?>
<script>
    $(document).ready(function () {
        $('form#modal-ajax-form').submit(function () {
            var formElement = $(this);
            var buttonElement = formElement.find('button[type=submit]');
            var buttonHtml = buttonElement.html();
            buttonElement.html('<i class="fa fa-circle-o-notch fa-spin"></i> Tunggu sebentar').attr('disabled', 'disabled');
            activateAjax({
                // response setting
                responsePlace: '#modal-ajax-content',
                // ajax setting
                url: formElement.attr('action'),
                type: formElement.attr('method'),
                data: formElement.serialize(),
                // function after ajax setting
                functionDone: function (data) {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                    if (data.hasOwnProperty('status') && data.status === true) {
                        $('#modal-ajax').modal('hide');
                        table.ajax.reload();
                    }
                },
                functionFail: function () {
                    buttonElement.html(buttonHtml).removeAttr('disabled');
                }
            });

            return false;
        });
    });
</script>