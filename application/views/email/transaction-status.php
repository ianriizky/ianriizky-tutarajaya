<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            min-width: 100%!important;
        }
        .content {
            width: 100%;
            max-width: 600px;
        }
    </style>
    <title><?=isset($page['name']) ? $page['name'] : 'UD. Tutara Jaya'?> - <?=isset($page['title']) ? $page['title'] : 'Konfirmasi Transaksi'?></title>
</head>
<body yahoo bgcolor="#f6f8f1">
    <h1><?=isset($page['title']) ? $page['title'] : 'Konfirmasi Akun'?></h1>
    <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            Transaksi anda telah mengalami perubahan status dari <?=isset($page['before']) ? $page['before'] : '-'?> ke <?=isset($page['after']) ? $page['after'] : '-'?>, silahkan klik link berikut ini : <a href="<?=isset($page['link']) ? $page['link'] : ''?>"><?=isset($page['link']) ? $page['link'] : ''?></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Terima kasih.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>