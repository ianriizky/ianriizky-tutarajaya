<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                <?=isset($page['title']) ? $page['title'] : 'Login'?> <small>UD. Tutara Jaya</small>
            </h1>
            <!-- /.Page Title -->

            <!-- Login Form -->
            <?=form_open(site_url('login'), '', [
                'redirect' => htmlspecialchars(strip_tags($this->input->get('redirect')))
            ])?>
                <div class="row mb-3">
                    <div class="col-xs-12 col-md-6 offset-md-3">
                        <div class="row">
                            <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                                <!-- alert -->
                                <div class="col-md-12 col-xs-12 mt-3">
                                    <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <span><?=$this->session->flashdata('message')?></span>
                                    </div>
                                </div>
                                <!-- /.alert -->
                            <?php endif?>

                            <!-- E-mail address -->
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="email">Username / Alamat Email</label>
                                    <input type="text" class="form-control<?=!empty($data['email']['error']) ? ' is-invalid' : ''?>" name="email" id="email" placeholder="Masukkan username atau email"<?=!empty($data['email']['value']) ? ' value="' . $data['email']['value'] . '"': ''?>>
                                    <?php if (!empty($data['email']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['email']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.E-mail address -->

                            <!-- Password -->
                            <div class="col-xs-12 col-md-12">
                                 <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control<?=!empty($data['password']['error']) ? ' is-invalid' : ''?>" name="password" id="password" placeholder="Masukkan password"<?=!empty($data['password']['value']) ? ' value="' . $data['password']['value'] . '"': ''?>>
                                    <?php if (!empty($data['password']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['password']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.Password -->

                            <!-- Remember Me Button -->
                            <!-- <div class="col-xs-12 col-md-12">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" name="remember_me" id="remember_me">
                                    <label class="form-check-label" for="remember_me">Remember me</label>
                                </div>
                            </div> -->
                            <!-- /.Remember Me Button -->

                            <!-- Submit Button -->
                            <div class="col-xs-12 col-md-12 text-right">
                                <button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i> Login</button>
                            </div>
                            <!-- /.Submit Button -->

                            <!-- Forgot Password -->
                            <div class="col-xs-12 col-md-12 mt-3 text-right">
                                <a href="<?=site_url('password/reset')?>">Lupa Password?</a>
                            </div>
                            <!-- /.Forgot Password -->
                        </div>
                    </div>
                </div>
            <?=form_close()?>
            <!-- /.Login Form -->
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>