<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                <?=isset($page['title']) ? $page['title'] : 'Register'?> <small>UD. Tutara Jaya</small>
            </h1>
            <!-- /.Page Title -->

            <!-- Register Form -->
            <?=form_open(site_url('register'), ['id' => 'register-form'])?>
                <div class="row mb-5">
                    <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                        <!-- alert -->
                        <div class="col-xs-12 col-md-12">
                            <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span><?=$this->session->flashdata('message')?></span>
                            </div>
                        </div>
                        <!-- /.alert -->
                    <?php endif?>

                    <!-- Fullname -->
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <label for="fullname">Nama Lengkap</label>
                            <input type="text" class="form-control<?=!empty($data['fullname']['error']) ? ' is-invalid' : ''?>" name="fullname" id="fullname" placeholder="Masukkan nama lengkap"<?=!empty($data['fullname']['value']) ? ' value="' . $data['fullname']['value'] . '"': ''?>>
                            <?php if (!empty($data['fullname']['error'])): ?>
                                <div class="invalid-feedback"><?=$data['fullname']['error']?></div>
                            <?php endif?>
                        </div>
                    </div>
                    <!-- /.Fullname -->

                    <!-- Username -->
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control<?=!empty($data['username']['error']) ? ' is-invalid' : ''?>" name="username" id="username" placeholder="Masukkan username"<?=!empty($data['username']['value']) ? ' value="' . $data['username']['value'] . '"': ''?>>
                            <?php if (!empty($data['username']['error'])): ?>
                                <div class="invalid-feedback"><?=$data['username']['error']?></div>
                            <?php endif?>
                        </div>
                    </div>
                    <!-- /.Username -->

                    <!-- E-mail address -->
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="email">Alamat Email</label>
                            <input type="email" class="form-control<?=!empty($data['email']['error']) ? ' is-invalid' : ''?>" name="email" id="email" placeholder="Masukkan email"<?=!empty($data['email']['value']) ? ' value="' . $data['email']['value'] . '"': ''?>>
                            <?php if (!empty($data['email']['error'])): ?>
                                <div class="invalid-feedback"><?=$data['email']['error']?></div>
                            <?php endif?>
                        </div>
                    </div>
                    <!-- /.E-mail address -->

                    <!-- Password -->
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control<?=!empty($data['password']['error']) ? ' is-invalid' : ''?>" name="password" id="password" placeholder="Masukkan password"<?=!empty($data['password']['value']) ? ' value="' . $data['password']['value'] . '"': ''?>>
                            <?php if (!empty($data['password']['error'])): ?>
                                <div class="invalid-feedback"><?=$data['password']['error']?></div>
                            <?php endif?>
                        </div>
                    </div>
                    <!-- /.Password -->

                    <!-- Retype Password -->
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="retype-password">Ulangi Password</label>
                            <input type="password" class="form-control<?=!empty($data['retype-password']['error']) ? ' is-invalid' : ''?>" name="retype-password" id="retype-password" placeholder="Masukkan ulang password"<?=!empty($data['retype-password']['value']) ? ' value="' . $data['retype-password']['value'] . '"': ''?>>
                            <?php if (!empty($data['retype-password']['error'])): ?>
                                <div class="invalid-feedback"><?=$data['retype-password']['error']?></div>
                            <?php endif?>
                        </div>
                    </div>
                    <!-- /.Retype Password -->

                    <!-- Address -->
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <div class="form-group">
                                <textarea class="form-control<?=!empty($data['alamat']['error']) ? ' is-invalid' : ''?>" name="alamat" id="alamat" rows="5" placeholder="Masukan alamat" style="resize: vertical;"><?=!empty($data['alamat']['value']) ? $data['alamat']['value'] : ''?></textarea>
                                <?php if (!empty($data['alamat']['error'])): ?>
                                    <div class="invalid-feedback"><?=$data['alamat']['error']?></div>
                                <?php endif?>
                            </div>
                        </div>
                    </div>
                    <!-- /.Address -->

                    <div class="col-xs-12 col-md-6">
                        <div class="row">
                            <!-- City -->
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="city">Kota</label>
                                    <input type="text" class="form-control<?=!empty($data['city']['error']) ? ' is-invalid' : ''?>" name="city" id="city" placeholder="Masukkan kota"<?=!empty($data['city']['value']) ? ' value="' . $data['city']['value'] . '"': ''?>>
                                    <?php if (!empty($data['city']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['city']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.City -->

                            <!-- Province -->
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="province">Provinsi</label>
                                    <input type="text" class="form-control<?=!empty($data['province']['error']) ? ' is-invalid' : ''?>" name="province" id="province" placeholder="Masukkan provinsi"<?=!empty($data['province']['value']) ? ' value="' . $data['province']['value'] . '"': ''?>>
                                    <?php if (!empty($data['province']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['province']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.Province -->

                            <!-- Telephone -->
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="telp">Nomor Telepon</label>
                                    <input type="text" class="form-control<?=!empty($data['telp']['error']) ? ' is-invalid' : ''?>" name="telp" id="telp" placeholder="Masukkan nomor telepon "<?=!empty($data['telp']['value']) ? ' value="' . $data['telp']['value'] . '"': ''?>>
                                    <?php if (!empty($data['telp']['error'])): ?>
                                        <div class="text-danger"><?=$data['telp']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.Telephone -->
                        </div>
                    </div>

                    <!-- Term and Aggreement Button -->
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input<?=!empty($data['term-agreement']['error']) ? ' is-invalid' : ''?>" id="term-agreement" name="term-agreement">
                            <label class="form-check-label" for="term-agreement">Saya menyetujui <a href="#">syarat dan ketentuan</a> yang berlaku di UD. Tutara Jaya</label>
                            <?php if (!empty($data['term-agreement']['error'])): ?>
                                <div class="invalid-feedback"><?=$data['term-agreement']['error']?></div>
                            <?php endif?>
                        </div>
                    </div>
                    <!-- /.Term and Aggreement Button -->

                    <!-- Submit Button -->
                    <div class="col-sm-12 col-md-12 text-right">
                        <button type="submit" class="btn btn-success" disabled="disabled"><i class="fa fa-user-plus"></i> Submit</button>
                    </div>
                    <!-- /.Submit Button -->
                </div>
            <?=form_close()?>
            <!-- /.Register Form -->
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
    <script>
        $(document).ready(function () {
            $('input#term-agreement').change(function () {
                if ($(this).is(':checked')) {
                    $('form#register-form').find('button[type=submit]').removeAttr('disabled');
                } else {
                    $('form#register-form').find('button[type=submit]').attr('disabled', 'disabled');
                }
            });
        });
    </script>
</body>
</html>