<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                <?=isset($page['title']) ? $page['title'] : 'Reset Password'?> <small>UD. Tutara Jaya</small>
            </h1>
            <!-- /.Page Title -->

            <!-- Forgot Password Form -->
            <?=form_open(site_url('password/reset/verify/' . (isset($page['link']) ? $page['link'] : '')), '', [
                // hidden value
                'id' => isset($page['id']) ? $page['id'] : ''
            ])?>
                <div class="row">
                    <div class="col-xs-12 col-md-6 offset-md-3">
                        <div class="row mb-3">
                            <!-- Explanation for user -->
                            <div class="col-xs-12 col-md-12">
                                <p>Masukkan password baru anda di bawah ini.</p>
                            </div>
                            <!-- /.Explanation for user -->

                            <!-- New password -->
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control<?=!empty($data['password']['error']) ? ' is-invalid' : ''?>" name="password" id="password" placeholder="Masukkan password"<?=!empty($data['password']['value']) ? 'value="' . $data['password']['value'] . '"': ''?>>
                                    <?php if (!empty($data['password']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['password']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.New password -->

                            <!-- Retype password -->
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="password">Ulangi Password</label>
                                    <input type="password" class="form-control<?=!empty($data['retype-password']['error']) ? ' is-invalid' : ''?>" name="retype-password" id="retype-password" placeholder="Ulangi password"<?=!empty($data['retype-password']['value']) ? ' value="' . $data['retype-password']['value'] . '"': ''?>>
                                    <?php if (!empty($data['retype-password']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['retype-password']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.Retype password -->

                            <!-- Action Button -->
                            <div class="col-xs-12 col-md-12 text-right">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                            </div>
                            <!-- /.Action Button -->
                        </div>
                    </div>
                </div>
            <?=form_close()?>
            <!-- /.Forgot Password Form -->
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>