<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                <?=isset($page['title']) ? $page['title'] : 'Lupa Password'?> <small>UD. Tutara Jaya</small>
            </h1>
            <!-- /.Page Title -->

            <!-- Forgot Password Form -->
            <?=form_open(site_url('password/reset'))?>
                <div class="row">
                    <div class="col-xs-12 col-md-6 offset-md-3">
                        <div class="row mb-3">
                            <!-- Explanation for user -->
                            <div class="col-xs-12 col-md-12">
                                <p>Masukkan email anda di bawah ini untuk memproses reset password anda.</p>
                            </div>
                            <!-- /.Explanation for user -->

                            <!-- E-mail address -->
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="email">Alamat Email</label>
                                    <input type="text" class="form-control<?=!empty($data['email']['error']) ? ' is-invalid' : ''?>" name="email" id="email" placeholder="Masukkan email"<?=!empty($data['email']['value']) ? ' value="' . $data['email']['value'] . '"': ''?>>
                                    <?php if (!empty($data['email']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['email']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                            <!-- /.E-mail address -->

                            <!-- Action Button -->
                            <div class="col-xs-12 col-md-12 text-right">
                                <a href="<?=site_url('login')?>" class="btn btn-danger"><i class="fa fa-chevron-left"></i> Kembali ke halaman Login</a>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                            </div>
                            <!-- /.Action Button -->
                        </div>
                    </div>
                </div>
            <?=form_close()?>
            <!-- /.Forgot Password Form -->
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>