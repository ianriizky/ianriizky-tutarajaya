<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                <?=isset($title) ? $title : 'Success'?> <small>UD. Tutara Jaya</small>
            </h1>
            <!-- /.Page Title -->

            <!-- Email Confirmation Notice -->
            <div class="row mb-5">
                <?php if (isset($message)): ?>
                    <!-- Explanation for user -->
                    <div class="col-xs-12 col-md-12">
                        <p><?=$message?></p>
                    </div>
                    <!-- /.Explanation for user -->
                <?php endif?>

                <!-- Action Button -->
                <div class="col-xs-12 col-md-12">
                    <a href="<?=site_url()?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali ke Beranda</a>
                </div>
                <!-- /.Action Button -->
            </div>
            <!-- /.Email Confirmation Notice -->
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>