<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                Pembayaran <small>UD. Tutara Jaya</small>
            </h1>
            <!-- /.Page Title -->

            <?php if (strlen($this->session->flashdata('message')) > 0): // check if alert is exist or not ?>
                <div class="row">
                    <!-- alert -->
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="alert alert-dismissible<?=strlen($this->session->flashdata('type')) > 0 ? ' ' . $this->session->flashdata('type') : ''?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->
                </div>
            <?php endif?>

            <?php if (!empty($this->cart->contents())): ?>
                <?=form_open(site_url('checkout'), '', [
                    'member_id' => !empty($data['member_id']['value']) ? $data['member_id']['value'] : ''
                ])?>
                    <!-- Member Information -->
                    <h4>Informasi Pembeli</h4>
                    <div class="row mb-5">
                        <!-- Nama Penerima -->
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="nama_penerima">Nama Penerima</label>
                                <input type="text" class="form-control<?=!empty($data['nama_penerima']['error']) ? ' is-invalid' : ''?>" name="nama_penerima" id="nama_penerima" placeholder="Masukkan nama lengkap penerima"<?=!empty($data['nama_penerima']['value']) ? ' value="' . $data['nama_penerima']['value'] . '"': ''?>>
                                <?php if (!empty($data['nama_penerima']['error'])): ?>
                                    <div class="invalid-feedback"><?=$data['nama_penerima']['error']?></div>
                                <?php endif?>
                            </div>
                        </div>
                        <!-- /.Nama Penerima -->

                        <div class="col-xs-12 col-md-6">
                            <div class="row">
                                <!-- City -->
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="city">Kota</label>
                                        <input type="text" class="form-control<?=!empty($data['city']['error']) ? ' is-invalid' : ''?>" name="city" id="city" placeholder="Masukkan kota"<?=!empty($data['city']['value']) ? ' value="' . $data['city']['value'] . '"': ''?>>
                                        <?php if (!empty($data['city']['error'])): ?>
                                            <div class="invalid-feedback"><?=$data['city']['error']?></div>
                                        <?php endif?>
                                    </div>
                                </div>
                                <!-- /.City -->

                                <!-- Province -->
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="province">Provinsi</label>
                                        <input type="text" class="form-control<?=!empty($data['province']['error']) ? ' is-invalid' : ''?>" name="province" id="province" placeholder="Masukkan provinsi"<?=!empty($data['province']['value']) ? ' value="' . $data['province']['value'] . '"': ''?>>
                                        <?php if (!empty($data['province']['error'])): ?>
                                            <div class="invalid-feedback"><?=$data['province']['error']?></div>
                                        <?php endif?>
                                    </div>
                                </div>
                                <!-- /.Province -->
                            </div>
                        </div>

                        <!-- Alamat -->
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <div class="form-group">
                                    <textarea class="form-control<?=!empty($data['alamat']['error']) ? ' is-invalid' : ''?>" name="alamat" id="alamat" rows="5" placeholder="Masukan alamat" style="resize: vertical;"><?=!empty($data['alamat']['value']) ? $data['alamat']['value'] : ''?></textarea>
                                    <?php if (!empty($data['alamat']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['alamat']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                        </div>
                        <!-- /.Alamat -->

                        <!-- Telephone -->
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="no_telp">Nomor Telepon Penerima</label>
                                <input type="text" class="form-control<?=!empty($data['no_telp']['error']) ? ' is-invalid' : ''?>" name="no_telp" id="no_telp" placeholder="Masukkan nomor telepon "<?=!empty($data['no_telp']['value']) ? ' value="' . $data['no_telp']['value'] . '"': ''?>>
                                <?php if (!empty($data['no_telp']['error'])): ?>
                                    <div class="text-danger"><?=$data['no_telp']['error']?></div>
                                <?php endif?>
                            </div>
                        </div>
                        <!-- /.Telephone -->

                        <!-- Catatan -->
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label for="catatan">Catatan</label>
                                <div class="form-group">
                                    <textarea class="form-control<?=!empty($data['catatan']['error']) ? ' is-invalid' : ''?>" name="catatan" id="catatan" rows="6" placeholder="Masukan catatan" style="resize: vertical;"><?=!empty($data['catatan']['value']) ? $data['catatan']['value'] : ''?></textarea>
                                    <?php if (!empty($data['catatan']['error'])): ?>
                                        <div class="invalid-feedback"><?=$data['catatan']['error']?></div>
                                    <?php endif?>
                                </div>
                            </div>
                        </div>
                        <!-- /.Catatan -->
                    </div>
                    <!-- /.Member Information -->

                    <!-- Cart Detail -->
                    <h4>Detail Transaksi</h4>
                    <div class="row table-responsive">
                        <table class="table">
                            <!-- Cart Header -->
                            <thead class="thead-dark">
                                <tr>
                                    <th style="width: 5%;">No</th>
                                    <th style="width: 15%;">Nama Produk</th>
                                    <th style="width: 15%;" class="text-right">Jumlah</th>
                                    <th class="text-right">Harga</th>
                                    <th class="text-right">Subtotal</th>
                                </tr>
                            </thead>
                            <!-- /.Cart Header -->

                            <!-- Cart Body -->
                            <tbody>
                                <?php
                                $i = 0;
                                $discounts = [];
                                ?>
                                <?php foreach ($this->cart->contents() as $cart): ?>
                                    <?php $discounts[$i] = ($cart['price'] * $cart['discount'] * 0.01) * $cart['qty']; ?>
                                    <tr>
                                        <td>
                                            <?=($i + 1)?>
                                            <input type="hidden" name="detail-transaksi[<?=$i?>][id_barang]" value="<?=$cart['id']?>">
                                            <input type="hidden" name="detail-transaksi[<?=$i?>][harga]" value="<?=$cart['price'] - ($cart['price'] * $cart['discount'] * 0.01)?>">
                                            <input type="hidden" name="detail-transaksi[<?=$i?>][jumlah]" value="<?=$cart['qty']?>">
                                            <input type="hidden" name="detail-transaksi[<?=$i?>][subtotal]" value="<?=$cart['subtotal'] - $discounts[$i]?>">
                                        </td>
                                        <td>
                                            <?=$cart['name']?>
                                            <?php if (!empty($cart['picture'])): ?>
                                                <img class="img-fluid" src="<?=base_url($cart['picture'])?>" alt="<?=$cart['name']?>">
                                            <?php else: ?>
                                                <img class="img-fluid" src="<?=base_url('assets/img/product/noimage.png')?>" alt="<?=$cart['name']?>">
                                            <?php endif ?>
                                        </td>
                                        <td class="text-right">
                                            <?=$cart['qty']?> pcs
                                        </td>
                                        <td class="text-right">
                                            <?=format_rupiah($cart['price'])?>
                                            <?php if (!empty($cart['discount'])): ?>
                                                <br>
                                                <small>- <?=format_rupiah($cart['price'] * $cart['discount'] * 0.01)?></small>
                                            <?php endif ?>
                                        </td>
                                        <td class="text-right">
                                            <?=format_rupiah($cart['subtotal'])?>
                                            <?php if (!empty($discounts[$i])): ?>
                                                <br>
                                                <small>- <?=format_rupiah($discounts[$i])?></small>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach?>
                            </tbody>
                            <!-- /.Cart Body -->

                            <?php if (!empty($this->cart->contents())): ?>
                                <!-- Cart Footer -->
                                <tbody>
                                    <tr>
                                        <td colspan="4" class="text-right">Total Harga</td>
                                        <td class="text-right">
                                            <?=format_rupiah($this->cart->total())?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="text-right">Total Potongan</td>
                                        <td class="text-right">
                                            <?=format_rupiah(array_sum($discounts))?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="4" class="text-right">Total Bayar</th>
                                        <th class="text-right">
                                            <?=format_rupiah($this->cart->total() - array_sum($discounts))?>
                                            <input type="hidden" name="total" value="<?=$this->cart->total() - array_sum($discounts)?>">
                                        </th>
                                    </tr>
                                </tbody>
                                <!-- /.Cart Footer -->
                            <?php endif ?>
                        </table>
                    </div>
                    <!-- /.Cart Detail -->

                    <!-- Submit Button -->
                    <div class="row mb-3">
                        <div class="col-sm-12 col-md-12 text-right">
                            <div class="btn-group">
                                <a href="<?=site_url('cart')?>" class="btn btn-warning text-white">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Kembali ke keranjang
                                </a>
                                <button type="submit" class="btn btn-success"><i class="fa fa-money"></i> Proses Transaksi</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.Submit Button -->
                <?=form_close()?>
            <?php endif?>
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>