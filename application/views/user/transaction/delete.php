<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                Transaksi - <?=$transaction['kode']?>
            </h1>
            <!-- /.Page Title -->

            <?php if (!empty($transaction)): ?>
                <!-- Confirmation -->
                <div class="row mb-3">
                    <div class="col-xs-12 col-md-6">
                        <p>Apakah anda yakin ingin membatalkan transaksi ini?</p>
                    </div>
                </div>
                <!-- /.Confirmation -->

                <!-- Cart Detail -->
                <div class="row table-responsive">
                    <table class="table">
                        <!-- Cart Header -->
                        <thead class="thead-dark">
                            <tr>
                                <th style="width: 5%;">No</th>
                                <th style="width: 15%;">Nama Produk</th>
                                <th style="width: 15%;" class="text-right">Jumlah</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right">Subtotal</th>
                            </tr>
                        </thead>
                        <!-- /.Cart Header -->

                        <!-- Cart Body -->
                        <tbody>
                            <?php foreach ($transactionDetails as $i => $transactionDetail): ?>
                                <tr>
                                    <td>
                                        <?=($i + 1)?>
                                    </td>
                                    <td>
                                        <?=$transactionDetail['p_nama']?>
                                    </td>
                                    <td class="text-right">
                                        <?=$transactionDetail['dt_jumlah']?> pcs
                                    </td>
                                    <td class="text-right">
                                        <?=format_rupiah($transactionDetail['dt_harga'])?>
                                    </td>
                                    <td class="text-right">
                                        <?=format_rupiah($transactionDetail['dt_subtotal'])?>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach?>
                        </tbody>
                        <!-- /.Cart Body -->

                        <?php if (!empty($transactionDetails)): ?>
                            <!-- Cart Footer -->
                            <tbody>
                                <tr>
                                    <th colspan="4" class="text-right">Total Bayar</th>
                                    <th class="text-right">
                                        <?=format_rupiah($transaction['total'])?>
                                    </th>
                                </tr>
                            </tbody>
                            <!-- /.Cart Footer -->
                        <?php endif ?>
                    </table>
                </div>
                <!-- /.Cart Detail -->

                <!-- Action -->
                <?=form_open(site_url('transaction/' . $transaction['id'] . '/destroy'))?>
                    <div class="row mb-5 mt-3">
                        <div class="col-xs-12 col-md-12">
                            <div class="btn-group">
                                <a href="<?=site_url('transaction')?>" class="btn btn-warning text-white">
                                    <i class="fa fa-chevron-left" aria-hidden="true"></i> Kembali ke Transaksi
                                </a>
                                <button type="submit" class="btn btn-danger text-white">
                                    <i class="fa fa-trash" aria-hidden="true"></i> Hapus Transaksi
                                </button>
                            </div>
                        </div>
                    </div>
                <?=form_close()?>
                <!-- /.Action -->
            <?php endif?>
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>