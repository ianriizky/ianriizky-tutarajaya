<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                Transaksi Belanja <small>UD. Tutara Jaya</small>
            </h1>
            <!-- /.Page Title -->

            <?php if (strlen($this->session->flashdata('message')) > 0): // check if alert is exist or not ?>
                <div class="row">
                    <!-- alert -->
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="alert alert-dismissible<?=strlen($this->session->flashdata('type')) > 0 ? ' ' . $this->session->flashdata('type') : ''?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->
                </div>
            <?php endif?>

            <!-- Cart Detail -->
            <div class="row mb-5">
                <div class="col-xs-12 col-md-12 table-responsive">
                    <table class="table">
                        <!-- Cart Header -->
                        <thead class="thead-dark">
                            <tr>
                                <th style="width: 5%;">No</th>
                                <th>Kode</th>
                                <th>Tanggal</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th style="width: 10%;">Opsi</th>
                            </tr>
                        </thead>
                        <!-- /.Cart Header -->

                        <!-- Cart Body -->
                        <tbody>
                            <?php $this->load->model('M_transaction', 'transaction') ?>
                            <?php foreach ($transactions as $i => $transaction): ?>
                                <tr>
                                    <td>
                                        <?=($i + 1)?>
                                    </td>
                                    <td>
                                        <?=$transaction['kode']?>
                                    </td>
                                    <td>
                                        <?=date('j F Y H.i.s', strtotime($transaction['created_at']))?>
                                    </td>
                                    <td>
                                        <?=format_rupiah($transaction['total'])?>
                                    </td>
                                    <td>
                                        <span class="btn btn-sm <?=$transaction['status_color']?> btn-block text-white" style="cursor: default;"><?=$transaction['status_text']?></span>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?=site_url('transaction/' . $transaction['id'])?>" class="btn btn-success btn-sm">
                                                <i class="fa fa-bookmark" aria-hidden="true"></i> Lihat
                                            </a>
                                            <?php if ($transaction['status'] == $this->transaction->statusOnProcess): ?>
                                                <a href="<?=site_url('transaction/' . $transaction['id'] . '/delete')?>" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash" aria-hidden="true"></i> Batal
                                                </a>
                                            <?php else: ?>
                                                <a class="btn btn-danger btn-sm text-white" title="Transaksi yang sudah terkonfirmasi atau sudah diproses oleh admin tidak dapat dihapus atau dibatalkan" onclick="alert('Transaksi yang sudah terkonfirmasi atau sudah diproses oleh admin tidak dapat dihapus atau dibatalkan')">
                                                    <i class="fa fa-trash" aria-hidden="true"></i> Batal
                                                </a>
                                            <?php endif?>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach?>
                        </tbody>
                        <!-- /.Cart Body -->
                    </table>
                </div>
            </div>
            <!-- /.Cart Detail -->
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
    <script>
        $(document).ready(function (argument) {
            $('table').DataTable({
                language: {
                    emptyTable: 'Anda tidak memiliki daftar transaksi belanja saat ini'
                },
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, 'All']
                ],
                columnDefs: [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });
        });
    </script>
</body>
</html>