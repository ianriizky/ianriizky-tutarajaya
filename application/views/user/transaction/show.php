<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                Transaksi - <?=$transaction['kode']?> <small><span class="btn btn-sm <?=$transaction['status_color']?> text-white" style="cursor: default;"><?=$transaction['status_text']?></span></small>
            </h1>
            <!-- /.Page Title -->

            <?php if (strlen($this->session->flashdata('message')) > 0): // check if alert is exist or not ?>
                <div class="row">
                    <!-- alert -->
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="alert alert-dismissible<?=strlen($this->session->flashdata('type')) > 0 ? ' ' . $this->session->flashdata('type') : ''?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->
                </div>
            <?php endif?>

            <?php if (!empty($transaction)): ?>
                <!-- Member Information -->
                <h4>Informasi Pembeli</h4>
                <div class="row mb-5">
                    <!-- Nama Penerima -->
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="nama_penerima">Nama Penerima</label>
                            <input type="text" class="form-control" id="nama_penerima" value="<?=$transaction['nama_penerima']?>" readonly="readonly">
                        </div>
                    </div>
                    <!-- /.Nama Penerima -->

                    <div class="col-xs-12 col-md-6">
                        <div class="row">
                            <!-- City -->
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="city">Kota</label>
                                    <input type="text" class="form-control" id="city" value="<?=$member['city']?>" readonly="readonly">
                                </div>
                            </div>
                            <!-- /.City -->

                            <!-- Province -->
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="province">Provinsi</label>
                                    <input type="text" class="form-control" id="province" value="<?=$member['province']?>" readonly="readonly">
                                </div>
                            </div>
                            <!-- /.Province -->
                        </div>
                    </div>

                    <!-- Alamat -->
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <div class="form-group">
                                <textarea class="form-control" id="alamat" rows="5" style="resize: vertical;" readonly="readonly"><?=$member['alamat']?></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- /.Alamat -->

                    <!-- Telephone -->
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="no_telp">Nomor Telepon Penerima</label>
                            <input type="text" class="form-control" id="no_telp" value="<?=$transaction['no_telp']?>" readonly="readonly">
                        </div>
                    </div>
                    <!-- /.Telephone -->

                    <!-- Catatan -->
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <label for="catatan">Catatan</label>
                            <div class="form-group">
                                <textarea class="form-control" id="catatan" rows="5" style="resize: vertical;" readonly="readonly"><?=$transaction['catatan']?></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- /.Catatan -->
                </div>
                <!-- /.Member Information -->

                <!-- Proof of Transaction -->
                <h4>Bukti Transaksi</h4>
                <?php if (!empty($transaction['bukti_transfer'])): ?>
                    <?php @file_get_contents(base_url(explode('|', $transaction['bukti_transfer'])[0])) ?>
                    <?php if (strpos($http_response_header[0], '200') !== false): ?>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <img src="<?=base_url(explode('|', $transaction['bukti_transfer'])[0])?>" class="img-fluid" alt="<?=$transaction['kode']?>">
                            </div>
                        </div>
                        <div class="row mt-1 mb-5">
                            <div class="col-xs-12 col-md-4">
                                <?=form_open(site_url('transaction/' . $transaction['id'] . '/upload/destroy'), '', ['transaction-encrypt-code' => explode('|', $transaction['bukti_transfer'])[1]])?>
                                    <button type="submit" class="btn btn-danger btn-block text-white">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Hapus Bukti Transaksi
                                    </button>
                                <?=form_close()?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="row mb-5">
                            <div class="col-xs-12 col-md-12">
                                <p>Anda belum melampirkan bukti transaksi. Silahkan cek email anda di <strong><?=$member['email']?></strong> atau hubungi admin untuk proses transaksi selanjutnya.</p>
                            </div>
                        </div>
                    <?php endif?>
                <?php else: ?>
                    <div class="row mb-5">
                        <div class="col-xs-12 col-md-12">
                            <p>Ada keselahan dalam proses upload bukti transaksi. Silahkan cek email anda di <strong><?=$member['email']?></strong> atau hubungi admin untuk proses transaksi selanjutnya.</p>
                        </div>
                    </div>
                <?php endif?>
                <!-- /.Proof of Transaction -->

                <!-- Cart Detail -->
                <h4>Detail Transaksi</h4>
                <div class="row table-responsive">
                    <table class="table">
                        <!-- Cart Header -->
                        <thead class="thead-dark">
                            <tr>
                                <th style="width: 5%;">No</th>
                                <th style="width: 15%;">Nama Produk</th>
                                <th style="width: 15%;" class="text-right">Jumlah</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right">Subtotal</th>
                            </tr>
                        </thead>
                        <!-- /.Cart Header -->

                        <!-- Cart Body -->
                        <tbody>
                            <?php foreach ($transactionDetails as $i => $transactionDetail): ?>
                                <tr>
                                    <td>
                                        <?=($i + 1)?>
                                    </td>
                                    <td>
                                        <?=$transactionDetail['p_nama']?>
                                    </td>
                                    <td class="text-right">
                                        <?=$transactionDetail['dt_jumlah']?> pcs
                                    </td>
                                    <td class="text-right">
                                        <?=format_rupiah($transactionDetail['dt_harga'])?>
                                    </td>
                                    <td class="text-right">
                                        <?=format_rupiah($transactionDetail['dt_subtotal'])?>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach?>
                        </tbody>
                        <!-- /.Cart Body -->

                        <?php if (!empty($transactionDetails)): ?>
                            <!-- Cart Footer -->
                            <tbody>
                                <tr>
                                    <th colspan="4" class="text-right">Total Bayar</th>
                                    <th class="text-right">
                                        <?=format_rupiah($transaction['total'])?>
                                    </th>
                                </tr>
                            </tbody>
                            <!-- /.Cart Footer -->
                        <?php endif ?>
                    </table>
                </div>
                <!-- /.Cart Detail -->
            <?php endif?>
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>