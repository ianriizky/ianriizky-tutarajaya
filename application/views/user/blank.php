<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/home/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/home/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 mt-3 text-center">
                    <?=$content?>
                </div>
            </div>
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/home/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/home/script') ?>
</body>
</html>