<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <?php if (strlen($this->session->flashdata('message')) > 0): // check if alert is exist or not ?>
                <div class="row">
                    <!-- alert -->
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="alert alert-dismissible<?=strlen($this->session->flashdata('type')) > 0 ? ' ' . $this->session->flashdata('type') : ''?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->
                </div>
            <?php endif?>

            <div class="row">
                <!-- left side -->
                <div class="col-xs-12 col-md-12 col-lg-3" style="border-right: 1px solid #F0F0F0;">
                    <!-- category -->
                    <h2 class="my-4">Produk Kami</h2>
                    <ul class="list-group">
                        <a href="<?=site_url('category')?>" class="list-group-item bg-dark text-white">Semua</a>
                        <?php if (count($categories) > 0): // check if categories is exist or not ?>
                            <a data-toggle="collapse" href="#category-collapse" class="list-group-item text-body">Kategori lainnya</a>
                            <?php
                                // Open collapse if there is categorySelect in URL
                                if (isset($categorySelect)) {
                                    foreach ($categories as $category) {
                                        if ($categorySelect == $category['id']) {
                                            $collapseOpen = ' show';
                                            break;
                                        }
                                    }
                                }
                            ?>
                            <div id="category-collapse" class="panel-collapse collapse<?=isset($categorySelect) ? $collapseOpen : ''?>">
                                <ul class="list-group">
                                    <?php foreach ($categories as $category): ?>
                                        <a href="<?=site_url('category/' . $category['id'])?>" class="list-group-item<?=(isset($categorySelect) and $categorySelect == $category['id']) == true ? ' active' : ''?>"><?=$category['kategori']?></a>
                                    <?php endforeach?>
                                </ul>
                            </div>
                        <?php endif?>
                    </ul>
                    <!-- /.category -->
                </div>
                <!-- /.left side -->

                <!-- right side -->
                <div class="col-xs-12 col-md-12 col-lg-9">
                    <div class="row">
                        <!-- slider -->
                        <?php $this->load->view('user/home/list-slider') ?>
                        <!-- /.slider -->

                        <!-- content (put your main content here) -->
                        <?php $this->load->view('user/home/list-product') ?>
                        <!-- /.content -->
                    </div>
                </div>
                <!-- /.right side -->
            </div>
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>