<?php if (count($products) > 0): // check if products is exist or not ?>
    <?php foreach ($products as $product): ?>
        <?php
        if (!empty($product['pg_gambar'])) {
            $productsImage = explode('|', $product['pg_gambar']);
        } else {
            $productsImage = null;
        }
        ?>
        <div class="col-xs-12 col-md-6 col-lg-4 mb-4">
            <div class="card h-100">
                <a href="<?=site_url('product/' . $product['p_id'])?>">
                    <?php if (!empty($productsImage) and count($productsImage) > 1): // Multiple Image ?>
                        <div id="product-image-<?=$product['p_id']?>" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php foreach ($productsImage as $imageKey => $imageValue): ?>
                                    <li data-target="#product-image-<?=$product['p_id']?>" data-slide-to="<?=$imageKey?>"<?=$imageKey == 0 ? ' class="active"' : ''?>></li>
                                <?php endforeach?>
                            </ol>
                            <div class="carousel-inner">
                                <?php foreach ($productsImage as $imageKey => $imageValue): ?>
                                    <div class="carousel-item<?=$imageKey == 0 ? ' active' : ''?>">
                                        <img class="d-block w-100 card-img-top" src="<?=site_url($imageValue)?>" alt="<?=$product['p_nama']?>">
                                    </div>
                                <?php endforeach?>
                            </div>
                            <a class="carousel-control-prev" href="#product-image-<?=$product['p_id']?>" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#product-image-<?=$product['p_id']?>" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    <?php elseif (isset($productsImage) and !empty($productsImage)): // Single Image ?>
                        <img id="product-<?=$product['p_id']?>" src="<?=site_url($productsImage[0])?>" class="card-img-top" alt="<?=$product['p_nama']?>">
                    <?php else: ?>
                        <img src="<?=site_url('assets/img/product/noimage.png')?>" class="card-img-top" style="width: 100%;" alt="<?=$product['p_nama']?>">
                    <?php endif?>
                </a>
                <div class="card-body">
                    <h4 class="card-title">
                        <?php if (!empty($product['ps_stok'])): ?>
                            <a href="<?=site_url('product/' . $product['p_id'])?>"><?=$product['p_nama']?></a>
                        <?php else: ?>
                            <s><a href="<?=site_url('product/' . $product['p_id'])?>"><?=$product['p_nama']?></a></s>
                        <?php endif?>
                    </h4>
                    <?php if (!empty($product['p_diskon'])): ?>
                        <small><s><?=format_rupiah($product['p_harga'])?></s></small>
                        <h5><?=format_rupiah($product['p_harga'] - ($product['p_harga'] * $product['p_diskon'] * 0.01))?></h5>
                    <?php else: ?>
                        <h5><?=format_rupiah($product['p_harga'])?></h5>
                    <?php endif?>
                    <p class="card-text"><?=strlen($product['p_deskripsi']) > 100 ? substr($product['p_deskripsi'], 0, 100) . ' &hellip;' : $product['p_deskripsi']?></p>
                </div>
                <div class="card-footer">
                    <?php if ($product['p_popular'] == '1'): ?>
                        <small class="text-muted"><i class="fa fa-star"></i> Popular</small>
                        <br>
                    <?php else: ?>
                        <small class="text-muted"><i class="fa fa-star-o"></i> Non-popular</small>
                        <br>
                    <?php endif?>
                    <?php if (!empty($product['p_diskon'])): ?>
                        <small class="text-muted text-uppercase"><i class="fa fa-tag"></i> <?=$product['p_diskon']?>% off</small>
                        <br>
                    <?php endif?>
                    <?php if (empty($product['ps_stok'])): ?>
                        <small class="text-muted"><i class="fa fa-times"></i> Stok habis</small>
                    <?php endif?>
                </div>
            </div>
        </div>
    <?php endforeach?>
<?php else: ?>
    <div class="col-xs-12 col-md-12 mb-4">
        <p class="text-center">Maaf, barang yang anda cari sedang tidak tersedia.</p>
    </div>
<?php endif?>