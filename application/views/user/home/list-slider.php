<div class="col-xs-12 col-md-12 col-lg-12">
    <?php if (!empty($sliders) and count($sliders) > 1): // Multiple Slider?>
        <div id="slider" class="carousel slide my-4" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php foreach ($sliders as $sliderIndex => $slider): ?>
                    <li data-target="#slider" data-slide-to="<?=$sliderIndex?>"<?=$sliderIndex == 0 ? ' class="active"' : ''?>></li>
                <?php endforeach?>
            </ol>
            <div class="carousel-inner" role="listbox">
                <?php foreach ($sliders as $sliderIndex => $slider): ?>
                    <div class="carousel-item<?=$sliderIndex == 0 ? ' active' : ''?>">
                        <img class="d-block img-fluid" style="width: 100%; height: 350px;" src="<?=base_url($slider['foto'])?>" alt="<?=$slider['id']?>">
                    </div>
                <?php endforeach?>
            </div>
            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    <?php elseif(!empty($sliders)): // Single Image?>
        <div class="my-4">
            <img class="d-block img-fluid" style="width: 100%; height: 350px;" src="<?=base_url($sliders[0]['foto'])?>" alt="<?=$sliders[0]['id']?>">
        </div>
    <?php endif?>
</div>