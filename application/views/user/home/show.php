<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                <?=$product['p_nama']?>
            </h1>
            <!-- /.Page Title -->

            <!-- Product Detail -->
            <div class="row">
                <?php if ($this->session->flashdata('message') != ''): // check if alert is exist or not ?>
                    <!-- alert -->
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="alert alert-dismissible<?=$this->session->flashdata('type') != '' ? ' ' . $this->session->flashdata('type') : ' alert-warning'?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->
                <?php endif?>
                <!-- Product Image -->
                <div class="col-xs-12 col-md-8">
                    <?php if (!empty($product['pg_gambar'])): ?>
                        <?php $productsImage = explode('|', $product['pg_gambar']); ?>
                        <?php if (!empty($productsImage) and count($productsImage) > 1): // Multiple Image ?>
                            <div id="product-image-<?=$product['p_id']?>" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <?php foreach ($productsImage as $imageKey => $imageValue): ?>
                                        <li data-target="#product-image-<?=$product['p_id']?>" data-slide-to="<?=$imageKey?>"<?=$imageKey == 0 ? ' class="active"' : ''?>></li>
                                    <?php endforeach?>
                                </ol>
                                <div class="carousel-inner">
                                    <?php foreach ($productsImage as $imageKey => $imageValue): ?>
                                        <div class="carousel-item<?=$imageKey == 0 ? ' active' : ''?>">
                                            <img class="d-block w-100 card-img-top" src="<?=site_url($imageValue)?>" alt="<?=$product['p_nama']?>">
                                        </div>
                                    <?php endforeach?>
                                </div>
                                <a class="carousel-control-prev" href="#product-image-<?=$product['p_id']?>" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#product-image-<?=$product['p_id']?>" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <div class="row">
                                <?php foreach ($productsImage as $imageKey => $imageValue): ?>
                                    <div class="col-md-3 d-none d-sm-block">
                                        <a href="#product-image-<?=$product['p_id']?>" data-slide-to="<?=$imageKey?>">
                                            <img src="<?=site_url($imageValue)?>" alt="<?=$product['p_nama']?>" class="w-100 mt-5">
                                        </a>
                                    </div>
                                <?php endforeach?>
                            </div>
                        <?php elseif (isset($productsImage) and !empty($productsImage)): // Single Image ?>
                            <img id="product-<?=$product['p_id']?>" src="<?=site_url($productsImage[0])?>" class="card-img-top" alt="<?=$product['p_nama']?>">
                        <?php else: ?>
                            <img src="<?=site_url('assets/img/product/noimage.png')?>" class="card-img-top" style="width: 100%;" alt="<?=$product['p_nama']?>">
                        <?php endif?>
                    <?php else: ?>
                        <img class="img-fluid" src="<?=base_url('assets/img/product/noimage.png')?>" alt="<?=$product['p_nama']?>">
                    <?php endif?>
                </div>
                <!-- /.Product Image -->

                <!-- Blank Spaces (for Mobile Only) -->
                <div class="col-xs-12 d-sm-block d-md-none">
                    <p>&nbsp;</p>
                </div>
                <!-- /.Blank Spaces (for Mobile Only) -->

                <!-- Product Detail -->
                <div class="col-xs-12 col-md-4">
                    <?php if (!empty($product)): ?>
                        <?php if (!empty($product['ps_stok'])): ?>
                            <?=form_open(site_url('cart'), '', [
                                'id'        => $product['p_id'],
                                'price'     => $product['p_harga'],
                                'discount'  => $product['p_diskon'],
                                'stock'     => $product['ps_stok'],
                                'name'      => $product['p_nama'],
                                'picture'   => !empty($product['pg_gambar']) ? explode('|', $product['pg_gambar'])[0] : null,
                                'redirect'  => htmlspecialchars(strip_tags(urlencode(current_url())))
                            ])?>
                                <div class="row">
                                    <div class="col-xs-12 col-md-5">
                                        <div class="input-group">
                                            <select class="form-control" name="qty" id="qty">
                                                <?php for ($i=0; $i < $product['ps_stok']; $i++): ?>
                                                    <option value="<?=($i + 1)?>"<?=$i == 0 ? ' selected="selected"' : ''?>><?=($i + 1)?></option>
                                                <?php endfor?>
                                            </select>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">pcs</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 d-sm-block d-md-none">
                                        <span>&nbsp;</span>
                                    </div>
                                    <div class="col-xs-12 col-md-7">
                                        <button type="submit" class="btn btn-success btn-block">
                                            <span><i class="fa fa-shopping-cart"></i> Add to cart</span>
                                        </button>
                                    </div>
                                </div>
                            <?=form_close()?>
                        <?php else: ?>
                            <button class="btn btn-success btn-block" disabled="disabled">
                                <span><i class="fa fa-shopping-cart" aria-hidden="true"></i> Stok habis</span>
                            </button>
                        <?php endif?>
                        <h3 class="my-3">Kategori</h3>
                        <p>
                            <?=isset($product['k_kategori']) ? $product['k_kategori'] : ''?>
                        </p>
                        <h3 class="my-3">Harga</h3>
                        <p>
                            <?php if (!empty($product['p_diskon'])): ?>
                                <span class="font-weight-bold" style="font-size: 1.2rem;"><?=format_rupiah($product['p_harga'] - ($product['p_harga'] * $product['p_diskon'] * 0.01))?></span>
                                <small><s><?=format_rupiah($product['p_harga'])?></s></small>
                                <small class="text-muted text-uppercase"><i class="fa fa-tag"></i> <?=$product['p_diskon']?>% off</small>
                            <?php else: ?>
                                <span class="font-weight-bold" style="font-size: 1.2rem;"><?=format_rupiah($product['p_harga'])?></span>
                            <?php endif ?>
                        </p>
                        <h3 class="my-3">Deskripsi Produk</h3>
                        <p>
                            <?=isset($product['p_deskripsi']) ? $product['p_deskripsi'] : '(Deskripsi produk tidak tersedia)'?>
                        </p>
                    <?php else: ?>
                        <button type="button" class="btn btn-success btn-block" disabled="disabled">
                            <span>Detail product not available</span>
                        </button>
                    <?php endif ?>
                </div>
                <!-- /.Product Detail -->

                <!-- Blank Spaces for Footer -->
                <div class="col-xs-12 col-md-12">
                    <br><br><br>
                </div>
                <!-- /.Blank Spaces for Footer -->
            </div>
            <!-- /.Product Detail -->
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
</body>
</html>