<?php
/**
 * Catch $this->session->flashdata('form_data')
 * and change it into $data
 */

$data = $this->session->flashdata('form_data') != '' ? $this->session->flashdata('form_data') : (isset($data) ? $data : []);
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <?php if (strlen($this->session->flashdata('message')) > 0): // check if alert is exist or not ?>
                <div class="row">
                    <!-- alert -->
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="alert alert-dismissible<?=strlen($this->session->flashdata('type')) > 0 ? ' ' . $this->session->flashdata('type') : ''?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->
                </div>
            <?php endif?>

            <div class="row">
                <!-- left side -->
                <div class="col-xs-12 col-md-12 col-lg-3" style="border-right: 1px solid #F0F0F0;">
                    <h2 class="my-4">Profil</h2>
                    <!-- category -->
                    <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                        <a class="nav-link<?=(isset($data['selected-tab']) and $data['selected-tab'] == 'update-profile') == true ? ' active' : ''?>" data-toggle="pill" href="#update-profile" role="tab"><i class="fa fa-user"></i> Profil Saya</a>
                        <a class="nav-link<?=(isset($data['selected-tab']) and $data['selected-tab'] == 'reset-password') == true ? ' active' : ''?>" data-toggle="pill" href="#reset-password" role="tab"><i class="fa fa-key"></i> Ganti Password</a>
                    </div>
                    <!-- /.category -->
                </div>
                <!-- /.left side -->

                <!-- right side -->
                <div class="col-xs-12 col-md-12 col-lg-9">
                    <div class="row">
                        <!-- content (put your main content here) -->
                        <div class="tab-content">
                            <div class="tab-pane fade<?=(isset($data['selected-tab']) and $data['selected-tab'] == 'update-profile') == true ? ' show active' : ''?>" id="update-profile" role="tabpanel">
                                <div class="container">
                                    <!-- My Profile Form -->
                                    <?=form_open(site_url('profile'), ['class' => 'form'], ['selected-tab' => 'update-profile'])?>
                                        <fieldset id="profile-form"<?=(isset($data['mode-edit']) and $data['mode-edit'] == false) == true ? ' disabled="disabled"' : ''?>>
                                            <div class="row mt-5">
                                                <!-- Fullname -->
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="form-group">
                                                        <label for="fullname">Nama Lengkap</label>
                                                        <input type="text" class="form-control<?=!empty($data['fullname']['error']) ? ' is-invalid' : ''?>" name="fullname" id="fullname" placeholder="Masukkan nama lengkap"<?=!empty($data['fullname']['value']) ? ' value="' . $data['fullname']['value'] . '"': ''?>>
                                                        <?php if (!empty($data['fullname']['error'])): ?>
                                                            <div class="invalid-feedback"><?=$data['fullname']['error']?></div>
                                                        <?php endif?>
                                                    </div>
                                                </div>
                                                <!-- /.Fullname -->

                                                <!-- Username -->
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group">
                                                        <label for="username">Username</label>
                                                        <input type="text" class="form-control<?=!empty($data['username']['error']) ? ' is-invalid' : ''?>" name="username" id="username" placeholder="Masukkan username"<?=!empty($data['username']['value']) ? ' value="' . $data['username']['value'] . '"': ''?>>
                                                        <?php if (!empty($data['username']['error'])): ?>
                                                            <div class="invalid-feedback"><?=$data['username']['error']?></div>
                                                        <?php endif?>
                                                    </div>
                                                </div>
                                                <!-- /.Username -->

                                                <!-- E-mail address -->
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">Alamat Email</label>
                                                        <input type="email" class="form-control<?=!empty($data['email']['error']) ? ' is-invalid' : ''?>" name="email" id="email" placeholder="Masukkan email"<?=!empty($data['email']['value']) ? ' value="' . $data['email']['value'] . '"': ''?>>
                                                        <?php if (!empty($data['email']['error'])): ?>
                                                            <div class="invalid-feedback"><?=$data['email']['error']?></div>
                                                        <?php endif?>
                                                    </div>
                                                </div>
                                                <!-- /.E-mail address -->

                                                <!-- Address -->
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat</label>
                                                        <div class="form-group">
                                                            <textarea class="form-control<?=!empty($data['alamat']['error']) ? ' is-invalid' : ''?>" name="alamat" id="alamat" rows="5" placeholder="Masukan alamat" style="resize: vertical;"><?=!empty($data['alamat']['value']) ? $data['alamat']['value'] : ''?></textarea>
                                                            <?php if (!empty($data['alamat']['error'])): ?>
                                                                <div class="invalid-feedback"><?=$data['alamat']['error']?></div>
                                                            <?php endif?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.Address -->

                                                <div class="col-xs-12 col-md-6">
                                                    <div class="row">
                                                        <!-- City -->
                                                        <div class="col-xs-12 col-md-6">
                                                            <div class="form-group">
                                                                <label for="city">Kota</label>
                                                                <input type="text" class="form-control<?=!empty($data['city']['error']) ? ' is-invalid' : ''?>" name="city" id="city" placeholder="Masukkan kota"<?=!empty($data['city']['value']) ? ' value="' . $data['city']['value'] . '"': ''?>>
                                                                <?php if (!empty($data['city']['error'])): ?>
                                                                    <div class="invalid-feedback"><?=$data['city']['error']?></div>
                                                                <?php endif?>
                                                            </div>
                                                        </div>
                                                        <!-- /.City -->

                                                        <!-- Province -->
                                                        <div class="col-xs-12 col-md-6">
                                                            <div class="form-group">
                                                                <label for="province">Provinsi</label>
                                                                <input type="text" class="form-control<?=!empty($data['province']['error']) ? ' is-invalid' : ''?>" name="province" id="province" placeholder="Masukkan provinsi"<?=!empty($data['province']['value']) ? ' value="' . $data['province']['value'] . '"': ''?>>
                                                                <?php if (!empty($data['province']['error'])): ?>
                                                                    <div class="invalid-feedback"><?=$data['province']['error']?></div>
                                                                <?php endif?>
                                                            </div>
                                                        </div>
                                                        <!-- /.Province -->

                                                        <!-- Telephone -->
                                                        <div class="col-xs-12 col-md-12">
                                                            <div class="form-group">
                                                                <label for="telp">Nomor Telepon</label>
                                                                <input type="text" class="form-control<?=!empty($data['telp']['error']) ? ' is-invalid' : ''?>" name="telp" id="telp" placeholder="Masukkan nomor telepon "<?=!empty($data['telp']['value']) ? ' value="' . $data['telp']['value'] . '"': ''?>>
                                                                <?php if (!empty($data['telp']['error'])): ?>
                                                                    <div class="text-danger"><?=$data['telp']['error']?></div>
                                                                <?php endif?>
                                                            </div>
                                                        </div>
                                                        <!-- /.Telephone -->
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="row mb-5">
                                            <!-- Action Button -->
                                            <div class="col-xs-12 col-md-6 offset-md-6" id="profile-button-action"<?=(isset($data['mode-edit']) and $data['mode-edit'] == false) == true ? ' style="display: none;"' : ''?>>
                                                <button type="button" class="btn btn-danger col-md-5" id="profile-button-cancel" style="margin-right: 15px;"><i class="fa fa-angle-double-down"></i> Batal</button>
                                                <button type="submit" class="btn btn-success col-md-6"><i class="fa fa-save"></i> Simpan</button>
                                            </div>
                                            <div class="col-xs-12 col-md-6 offset-md-6" id="profile-button-request"<?=(isset($data['mode-edit']) and $data['mode-edit'] == true) == true ? ' style="display: none;"' : ''?>>
                                                <button type="button" class="btn btn-info btn-block" id="profile-button-change"><i class="fa fa-angle-double-down"></i> Ubah Profil</button>
                                            </div>
                                            <!-- /.Action Button -->
                                        </div>
                                    <?=form_close()?>
                                    <!-- /.My Profile Form -->
                                </div>
                            </div>
                            <div class="tab-pane fade<?=(isset($data['selected-tab']) and $data['selected-tab'] == 'reset-password') == true ? ' show active' : ''?>" id="reset-password" role="tabpanel">
                                <div class="container">
                                    <!-- Change Password Form -->
                                    <?=form_open(site_url('profile'), ['class' => 'form'], ['selected-tab' => 'reset-password'])?>
                                        <div class="row mt-5 mb-3">
                                            <!-- Password -->
                                            <div class="col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <label for="password">Password</label>
                                                    <input type="password" class="form-control<?=!empty($data['password']['error']) ? ' is-invalid' : ''?>" name="password" id="password" placeholder="Masukkan password"<?=!empty($data['password']['value']) ? ' value="' . $data['password']['value'] . '"': ''?>>
                                                    <?php if (!empty($data['password']['error'])): ?>
                                                        <div class="invalid-feedback"><?=$data['password']['error']?></div>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.Password -->

                                            <!-- Retype Password -->
                                            <div class="col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <label for="retype-password">Ulangi Password</label>
                                                    <input type="password" class="form-control<?=!empty($data['retype-password']['error']) ? ' is-invalid' : ''?>" name="retype-password" id="retype-password" placeholder="Masukkan ulang password"<?=!empty($data['retype-password']['value']) ? ' value="' . $data['retype-password']['value'] . '"': ''?>>
                                                    <?php if (!empty($data['retype-password']['error'])): ?>
                                                        <div class="invalid-feedback"><?=$data['retype-password']['error']?></div>
                                                    <?php endif?>
                                                </div>
                                            </div>
                                            <!-- /.Retype Password -->
                                        </div>
                                        <div class="row mb-3">
                                            <!-- Action Button -->
                                            <div class="col-xs-12 col-md-12">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                                            </div>
                                            <!-- /.Action Button -->
                                        </div>
                                    <?=form_close()?>
                                    <!-- /.Change Password Form -->
                                </div>
                            </div>
                        </div>
                        <!-- /.content -->
                    </div>
                </div>
                <!-- /.right side -->
            </div>
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
    <script src="<?=base_url('assets/plugins/jquery/jquery-ui.min.js')?>"></script>
    <script>
        $(document).ready(function () {
            $('#profile-button-change').click(function () {
                $('#profile-button-request').hide('drop', {direction: 'right'}, 'slow');
                $('#profile-button-action').show('drop', {direction: 'left'}, 'slow');
                $('#profile-form').removeAttr('disabled');
            });
            $('#profile-button-cancel').click(function () {
                $('#profile-button-request').show('drop', {direction: 'left'}, 'slow');
                $('#profile-button-action').hide('drop', {direction: 'right'}, 'slow');
                $('#profile-form').attr('disabled', 'disabled');
            });
        });
    </script>
</body>
</html>