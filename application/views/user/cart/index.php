<!DOCTYPE html>
<html lang="<?=$this->config->item('language')?>">
<head>
    <?php $this->load->view('layouts/user/style') ?>
</head>
<body>
    <article>
        <!-- header -->
        <?php $this->load->view('layouts/user/nav') ?>
        <!-- /.header -->

        <!-- section -->
        <section class="container">
            <!-- Page Title -->
            <h1 class="my-4">
                Keranjang Belanja <small>UD. Tutara Jaya</small>
            </h1>
            <!-- /.Page Title -->

            <?php if (strlen($this->session->flashdata('message')) > 0): // check if alert is exist or not ?>
                <div class="row">
                    <!-- alert -->
                    <div class="col-xs-12 col-md-12 mt-3">
                        <div class="alert alert-dismissible<?=strlen($this->session->flashdata('type')) > 0 ? ' ' . $this->session->flashdata('type') : ''?>" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <span><?=$this->session->flashdata('message')?></span>
                        </div>
                    </div>
                    <!-- /.alert -->
                </div>
            <?php endif?>

            <!-- Cart Detail -->
            <div class="row mb-5">
                <div class="col-xs-12 col-md-12 table-responsive">
                    <table class="table">
                        <!-- Cart Header -->
                        <thead class="thead-dark">
                            <tr>
                                <th style="width: 5%;">No</th>
                                <th style="width: 25%;">Nama Produk</th>
                                <th style="width: 15%;">Jumlah</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right">Subtotal</th>
                                <th style="width: 10%;">Opsi</th>
                            </tr>
                        </thead>
                        <!-- /.Cart Header -->

                        <!-- Cart Body -->
                        <tbody>
                            <?php
                            $i = 0;
                            $discounts = [];
                            ?>
                            <?php foreach ($this->cart->contents() as $cart): ?>
                                <?php $discounts[$i] = ($cart['price'] * $cart['discount'] * 0.01) * $cart['qty']; ?>
                                <tr>
                                    <td>
                                        <?=($i + 1)?>
                                    </td>
                                    <td>
                                        <?=$cart['name']?>
                                        <?php if (!empty($cart['picture'])): ?>
                                            <img class="img-fluid" src="<?=base_url($cart['picture'])?>" alt="<?=$cart['name']?>">
                                        <?php else: ?>
                                            <img class="img-fluid" src="<?=base_url('assets/img/product/noimage.png')?>" alt="<?=$cart['name']?>">
                                        <?php endif ?>
                                    </td>
                                    <td>
                                        <?=form_open(site_url('cart/' . $cart['rowid']), ['id' => 'update-cart-' . $cart['rowid']], ['id' => $cart['id']])?>
                                            <div class="input-group">
                                                <select class="form-control change-qty" name="qty" data-row-id="<?=$cart['rowid']?>">
                                                    <?php for ($j=0; $j < $cart['stock']; $j++) : ?>
                                                        <option value="<?=($j + 1)?>"<?=($j + 1) == $cart['qty'] ? ' selected="selected"' : ''?>><?=($j + 1)?></option>
                                                    <?php endfor?>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">pcs</span>
                                                </div>
                                            </div>
                                        <?=form_close()?>
                                    </td>
                                    <td class="text-right">
                                        <?=format_rupiah($cart['price'])?>
                                        <?php if (!empty($cart['discount'])): ?>
                                            <br>
                                            <small>- <?=format_rupiah($cart['price'] * $cart['discount'] * 0.01)?></small>
                                        <?php endif ?>
                                    </td>
                                    <td class="text-right">
                                        <?=format_rupiah($cart['subtotal'])?>
                                        <?php if (!empty($discounts[$i])): ?>
                                            <br>
                                            <small>- <?=format_rupiah($discounts[$i])?></small>
                                        <?php endif ?>
                                    </td>
                                    <td>
                                        <?=form_open(site_url('cart/' . $cart['rowid']), '', ['id' => $cart['id'], 'qty' => 0])?>
                                            <button type="submit" class="btn btn-danger btn-sm update-cart">
                                                <i class="fa fa-trash" aria-hidden="true"></i> Hapus
                                            </button>
                                        <?=form_close()?>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach?>
                        </tbody>
                        <!-- /.Cart Body -->

                        <?php if (!empty($this->cart->contents())): ?>
                            <!-- Cart Footer -->
                            <tbody>
                                <tr>
                                    <td colspan="4" class="text-right">Total Harga</td>
                                    <td class="text-right"><?=format_rupiah($this->cart->total())?></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">Total Potongan</td>
                                    <td class="text-right"><?=format_rupiah(array_sum($discounts))?></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th colspan="4" class="text-right">Total Bayar</th>
                                    <th class="text-right"><?=format_rupiah($this->cart->total() - array_sum($discounts))?></th>
                                    <th></th>
                                </tr>
                            </tbody>
                            <!-- /.Cart Footer -->
                        <?php endif ?>
                    </table>
                </div>
                <?php if (!empty($this->cart->contents())): ?>
                    <div class="col-xs-12 col-md-12 text-right mt-3">
                        <div class="btn-group">
                            <a href="<?=site_url()?>" class="btn btn-warning text-white">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i> Kembali ke Beranda
                            </a>
                            <a href="<?=site_url('checkout')?>" class="btn btn-success text-white">
                                Proses transaksi <i class="fa fa-chevron-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                <?php endif?>
            </div>
            <!-- /.Cart Detail -->
        </section>
        <!-- /.section -->

        <!-- footer -->
        <?php $this->load->view('layouts/user/footer') ?>
        <!-- /.footer -->
    </article>

    <?php $this->load->view('layouts/user/script') ?>
    <script>
        $(document).ready(function (argument) {
            $('table').DataTable({
                language: {
                    emptyTable: 'Keranjang belanja kosong'
                },
                lengthMenu: [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, 'All']
                ],
                columnDefs: [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });
            $('.change-qty').change(function () {
                var rowId = $(this).data('row-id');
                $('form#update-cart-' + rowId).submit();
            });
        });
    </script>
</body>
</html>