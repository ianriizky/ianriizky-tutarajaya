<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{

    public function edit_unique($str, $field)
    {
        sscanf($field, '%[^.].%[^.].%[^.].%[^.]', $table, $field, $column, $value);
        return isset($this->CI->db) ?
        ($this->CI->db->limit(1)->get_where($table, [
            $field          => $str,
            $column . ' !=' => $value,
        ])->num_rows() === 0) : false;
    }

}
