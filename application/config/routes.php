<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|    $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:    my-controller/index    -> my_controller/index
|        my-controller/my-method    -> my_controller/my_method
 */

/**
 * Default Controller
 */
$route['404_override'] = 'Error_custom/show_404';
$route['translate_uri_dashes'] = false;


/* =============================
 * ========= Home Area =========
 * =============================
 */

/**
 * Home Controller
 */
// Home
$route['default_controller'] = 'UserHome';
// Category
$route['category']['GET'] = 'UserHome';
$route['category/(:any)']['GET'] = 'UserHome/index/$1';
// Product
$route['product/(:num)']['GET'] = 'UserHome/show/$1';
// Cart
$route['cart']['GET'] = 'UserCart/index';
$route['cart']['POST'] = 'UserCart/store';
$route['cart/(:any)']['POST'] = 'UserCart/update/$1';
// Checkout
$route['checkout']['GET'] = 'UserCheckout/index';
$route['checkout']['POST'] = 'UserCheckout/store';
// Transaction
$route['transaction']['GET'] = 'UserTransaction/index';
$route['transaction/(:num)']['GET'] = 'UserTransaction/show/$1';
$route['transaction/(:num)/delete']['GET'] = 'UserTransaction/delete/$1';
$route['transaction/(:num)/destroy']['POST'] = 'UserTransaction/destroy/$1';
$route['transaction/(:any)/upload']['GET'] = 'UserTransaction/upload/$1';
$route['transaction/(:num)']['POST'] = 'UserTransaction/update/$1';
$route['transaction/(:num)/upload/destroy']['POST'] = 'UserTransaction/uploadDestroy/$1';
// Profile
$route['profile']['GET'] = 'UserProfile/show';
$route['profile']['POST'] = 'UserProfile/update';
// Add Dummy "z_user" data
$route[ADMIN . '/admin_lol']['GET'] = 'UserProfile/dummy';

/**
 * Authentication Controller
 */
// Login
$route['login']['GET'] = 'UserAuthentication/loginGet';
$route['login']['POST'] = 'UserAuthentication/loginPost';
// Reset Password
$route['password/reset']['GET'] = 'UserAuthentication/passwordResetGet';
$route['password/reset']['POST'] = 'UserAuthentication/passwordResetPost';
$route['password/reset/verify/(:any)']['GET'] = 'UserAuthentication/passwordResetVerifyGet/$1';
$route['password/reset/verify/(:any)']['POST'] = 'UserAuthentication/passwordResetVerifyPost/$1';
// Register
$route['register']['GET'] = 'UserAuthentication/registerGet';
$route['register']['POST'] = 'UserAuthentication/registerPost';
$route['register/verify/(:any)']['GET'] = 'UserAuthentication/registerVerifyGet/$1';
// Logout
$route['logout']['POST'] = 'UserAuthentication/logoutPost';
$route['logout/success']['GET'] = 'UserAuthentication/logoutSuccessGet';



/* ==============================
 * ========= Admin Area =========
 * ==============================
 */

/**
 * Admin Controller
 */
// Dashboard
$route[ADMIN]['GET'] = 'AdminDashboard';
// Profile
$route[ADMIN . '/profile']['GET'] = 'AdminProfile/show';
$route[ADMIN . '/profile']['POST'] = 'AdminProfile/update';

/**
 * Authentication_admin Controller
 */
// Login
$route[ADMIN . '/login']['GET'] = 'AdminAuthentication/loginGet';
$route[ADMIN . '/login']['POST'] = 'AdminAuthentication/loginPost';
// Reset Password (tidak ada karena tabel "z_user" tidak memiliki kolom "token")
// Logout
$route[ADMIN . '/logout']['POST'] = 'AdminAuthentication/logoutPost';
$route[ADMIN . '/logout/success']['GET'] = 'AdminAuthentication/logoutSuccessGet';



/* ===============================
 * ========= Master Data =========
 * ===============================
 */

/**
 * Category Controller
 */
$route[ADMIN . '/category']['GET'] = 'AdminCategory/index';
$route[ADMIN . '/category/datatables']['POST'] = 'AdminCategory/datatables';
$route[ADMIN . '/category/create']['GET'] = 'AdminCategory/create';
$route[ADMIN . '/category']['POST'] = 'AdminCategory/store';
$route[ADMIN . '/category/(:num)']['GET'] = 'AdminCategory/show/$1';
$route[ADMIN . '/category/(:num)/edit']['GET'] = 'AdminCategory/edit/$1';
$route[ADMIN . '/category/(:num)']['POST'] = 'AdminCategory/update/$1';
$route[ADMIN . '/category/(:num)/delete']['GET'] = 'AdminCategory/delete/$1';
$route[ADMIN . '/category/(:num)/destroy']['POST'] = 'AdminCategory/destroy/$1';

/**
 * Product Controller
 */
$route[ADMIN . '/product']['GET'] = 'AdminProduct/index';
$route[ADMIN . '/product/datatables']['POST'] = 'AdminProduct/datatables';
$route[ADMIN . '/product/create']['GET'] = 'AdminProduct/create';
$route[ADMIN . '/product']['POST'] = 'AdminProduct/store';
$route[ADMIN . '/product/(:num)']['GET'] = 'AdminProduct/show/$1';
$route[ADMIN . '/product/(:num)/edit']['GET'] = 'AdminProduct/edit/$1';
$route[ADMIN . '/product/(:num)']['POST'] = 'AdminProduct/update/$1';
$route[ADMIN . '/product/(:num)/delete']['GET'] = 'AdminProduct/delete/$1';
$route[ADMIN . '/product/(:num)/destroy']['POST'] = 'AdminProduct/destroy/$1';
// product_picture
$route[ADMIN . '/product/(:num)/picture']['GET'] = 'AdminProduct/pictureIndex/$1';
$route[ADMIN . '/product/picture/datatables']['POST'] = 'AdminProduct/pictureDatatables';
$route[ADMIN . '/product/(:num)/picture/create']['GET'] = 'AdminProduct/pictureCreate/$1';
$route[ADMIN . '/product/(:num)/picture']['POST'] = 'AdminProduct/pictureStore/$1';
$route[ADMIN . '/product/(:num)/picture/delete']['GET'] = 'AdminProduct/pictureDelete/$1';
$route[ADMIN . '/product/(:num)/picture/destroy']['POST'] = 'AdminProduct/pictureDestroy/$1';

/**
 * Slider Controller
 */
$route[ADMIN . '/slider']['GET'] = 'AdminSlider/index';
$route[ADMIN . '/slider/datatables']['POST'] = 'AdminSlider/datatables';
$route[ADMIN . '/slider/create']['GET'] = 'AdminSlider/create';
$route[ADMIN . '/slider']['POST'] = 'AdminSlider/store';
// slider edit dimatikan karena gak perlu bosque :)
// $route[ADMIN . '/slider/(:num)']['GET'] = 'AdminSlider/show/$1';
// $route[ADMIN . '/slider/(:num)/edit']['GET'] = 'AdminSlider/edit/$1';
// $route[ADMIN . '/slider/(:num)']['POST'] = 'AdminSlider/update/$1';
$route[ADMIN . '/slider/(:num)/delete']['GET'] = 'AdminSlider/delete/$1';
$route[ADMIN . '/slider/(:num)/destroy']['POST'] = 'AdminSlider/destroy/$1';
$route[ADMIN . '/slider/(:num)/status']['GET'] = 'AdminSlider/statusGet/$1';
$route[ADMIN . '/slider/(:num)/status']['POST'] = 'AdminSlider/statusPost/$1';

/**
 * Transaction Controller
 */
$route[ADMIN . '/transaction']['GET'] = 'AdminTransaction/index';
$route[ADMIN . '/transaction/datatables']['POST'] = 'AdminTransaction/datatables';
// $route[ADMIN . '/transaction/create']['GET'] = 'AdminTransaction/create';
// $route[ADMIN . '/transaction']['POST'] = 'AdminTransaction/store';
$route[ADMIN . '/transaction/(:num)']['GET'] = 'AdminTransaction/show/$1';
$route[ADMIN . '/transaction/(:num)/edit']['GET'] = 'AdminTransaction/edit/$1';
$route[ADMIN . '/transaction/(:num)']['POST'] = 'AdminTransaction/update/$1';
$route[ADMIN . '/transaction/(:num)/delete']['GET'] = 'AdminTransaction/delete/$1';
$route[ADMIN . '/transaction/(:num)/destroy']['POST'] = 'AdminTransaction/destroy/$1';
$route[ADMIN . '/transaction/(:num)/upload/destroy']['POST'] = 'AdminTransaction/uploadDestroy/$1';

/**
 * Report Controller
 */
$route[ADMIN . '/report']['GET'] = 'AdminReport/index';
$route[ADMIN . '/report/datatables']['POST'] = 'AdminReport/datatables';
$route[ADMIN . '/report/(:num)']['GET'] = 'AdminReport/show/$1';

/**
 * Member Controller
 */
$route[ADMIN . '/member']['GET'] = 'AdminMember/index';
$route[ADMIN . '/member/datatables']['POST'] = 'AdminMember/datatables';
$route[ADMIN . '/member/create']['GET'] = 'AdminMember/create';
$route[ADMIN . '/member']['POST'] = 'AdminMember/store';
$route[ADMIN . '/member/(:num)']['GET'] = 'AdminMember/show/$1';
$route[ADMIN . '/member/(:num)/edit']['GET'] = 'AdminMember/edit/$1';
$route[ADMIN . '/member/(:num)']['POST'] = 'AdminMember/update/$1';
$route[ADMIN . '/member/(:num)/delete']['GET'] = 'AdminMember/delete/$1';
$route[ADMIN . '/member/(:num)/destroy']['POST'] = 'AdminMember/destroy/$1';
$route[ADMIN . '/member/(:num)/status']['GET'] = 'AdminMember/statusGet/$1';
$route[ADMIN . '/member/(:num)/status']['POST'] = 'AdminMember/statusPost/$1';
$route[ADMIN . '/member/(:num)/password/reset']['GET'] = 'AdminMember/passwordResetGet/$1';
$route[ADMIN . '/member/(:num)/password/reset']['POST'] = 'AdminMember/passwordResetPost/$1';


/* =============================================
 * === Avoid direct access to the controller ===
 * ====== with the full controller name. ======
 * =============================================
 */
$route['(:any)'] = 'Error_custom/show_404';