<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|    https://codeigniter.com/user_guide/general/hooks.html
|
 */
// $hook['post_controller_constructor'] = function () {
//     // load all config_property
//     $CI        = &get_instance();
//     $configs   = $CI->M_setting->get_value_by_key('config');
//     if ($configs) {
//         $jsonDecodeConfigs = json_decode($configs, true);
//         $CI->config->set_item_array($jsonDecodeConfigs);
//     }

//     defined('USEAJAX') or define('USEAJAX', $CI->M_setting->get_value_by_key('use_ajax'));
//     exit(USEAJAX);
// };